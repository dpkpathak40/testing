## [Docker](https://www.docker.com/) 

### Prerequisites 
* Docker Engine v20.10
* Docker Compose v1.27

### Usage
You can use Docker for MySql:
```
cd /path/to/this-project/.docker
docker-compose up -d mysql8

# Login into container
docker-compose exec mysql8 bash
```
