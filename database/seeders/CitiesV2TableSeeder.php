<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class CitiesV2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = json_decode(File::get(database_path('raw/cities-v2.json')), true);

        foreach ($cities as $cityItem) {
            City::create(Arr::only($cityItem, [
                'name',
            ]));
        }
    }
}
