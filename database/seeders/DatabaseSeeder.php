<?php

namespace Database\Seeders;

use Database\Seeders\Testing\AdminsTableSeeder;
use Database\Seeders\Testing\BrokersTableSeeder;
use Database\Seeders\Testing\JobsTableSeeder;
use Database\Seeders\Testing\UsersTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding: Environment - '.config('app.env'));

        $this->call([
            ServicesTableSeeder::class,
            CitiesTableSeeder::class,
            CitiesV2TableSeeder::class,
            CitiesV3TableSeeder::class,
        ]);

        // Don't seed these tables when in production
        if (! app()->environment(['production', 'testing', 'staging'])) {
            $this->call([
                UsersTableSeeder::class,
                AdminsTableSeeder::class,
                BrokersTableSeeder::class,
                JobsTableSeeder::class,
            ]);
        }
    }
}
