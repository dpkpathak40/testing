<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function run()
    {
        $cities = json_decode(File::get(database_path('raw/cities.json')), true);

        foreach ($cities as $cityItem) {
            City::create(Arr::only($cityItem, [
                'name',
            ]));
        }
    }
}
