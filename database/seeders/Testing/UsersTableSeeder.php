<?php

namespace Database\Seeders\Testing;

use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = Service::all();

        $user = new User([
            'email' => env('TEST_REALTOR_EMAIL', 'austin.ankur@ithands.net'),
            'password' => bcrypt('password@123'),
        ]);
        $user->setEmailAsVerified();
        $user->setDocumentsStatusTo(\App\Models\UserDocument::STATUS_VERIFIED);
        $user->save();
        $user->profile()->save(\App\Models\UserProfile::factory()->make([
            'first_name' => 'TestRealtor',
        ]));
        $user->preference()->save(\App\Models\UserPreference::factory()->make([
            'email_notifications' => true,
        ]));
        $user->services()->saveMany($services->shuffle()->take(rand(3, 6)));
        $user->cities()->sync(Arr::random(range(1, 80), rand(5, 15)));

        // Create some random users
        User::factory(rand(15, 30))->create()->each(function (User $user) use ($services) {
            $user->profile()->save(\App\Models\UserProfile::factory()->make());
            $user->services()->saveMany($services->shuffle()->take(rand(2, 4)));

            if (rand(0, 1)) {
                $user->address()->save(\App\Models\UserAddress::factory()->make());
            }
            if (rand(0, 1)) {
                $user->preference()->save(\App\Models\UserPreference::factory()->make());
            }
        });
    }
}
