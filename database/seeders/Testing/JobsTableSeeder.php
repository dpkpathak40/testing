<?php

namespace Database\Seeders\Testing;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Job::factory(rand(15, 30))->create()->each(function ($job) {
            $job->cities()->sync(Arr::random(range(1, 80), rand(1, 5)));

            if ($job->service->requires_number_of_homes) {
                $job->number_of_homes = rand(1, 5);
                $job->save();
            }
        });
    }
}
