<?php

namespace Database\Seeders\Testing;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'jessica@jjsociallight.com',
            ],
            [
                'email' => 'oktohub@oktohub.com',
            ],
            [
                'email' => 'ryan@jjsociallight.com',
            ],
            [
                'email' => env('TEST_ADMIN_EMAIL', 'austin.ankur@ithands.net'),
            ],
        ];

        foreach ($users as $user) {
            Admin::create(array_merge($user, [
                'password' => bcrypt('password@123'),
            ]));
        }

        Admin::factory(rand(50, 100))->create();
    }
}
