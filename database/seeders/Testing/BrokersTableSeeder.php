<?php

namespace Database\Seeders\Testing;

use Illuminate\Database\Seeder;

class BrokersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Broker::factory(rand(10, 20))->create();
    }
}
