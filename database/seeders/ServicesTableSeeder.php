<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;
use Illuminate\Http\File;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = require database_path('raw/services.php');

        foreach ($services as $service) {
            $service['image'] = new File(public_path('assets/services/'.$service['image']));
            $service['active'] = true;
            Service::create($service);
        }
    }
}
