<?php

namespace Database\Factories;

use App\Models\UserProfile;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserProfile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => rand(0, 1) ? $this->faker->lastName : null,
            'bio' => $this->faker->paragraph(rand(3, 5)),
            'experience' => $this->faker->paragraph,
            'education' => $this->faker->paragraph(3),
        ];
    }
}
