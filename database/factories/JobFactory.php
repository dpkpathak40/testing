<?php

namespace Database\Factories;

use App\Models\Job;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Job::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'creator_user_id' => rand(1, 10),
            'service_id' => rand(1, 6),
            'due_date' => now()->addDays(rand(7, 30)),
            'chargeable_amount' => rand(50, 150),
            'transferable_amount' => rand(50, 100),
            'description' => $this->faker->paragraph(rand(3, 5)),
            'number_of_homes' => 0,
        ];
    }
}
