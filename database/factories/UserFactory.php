<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$5GSm8YpIuQqU7OpGREJWv.EY7OoofkVCnoYcx87Zv8nb9qqDVXGcG', // password@123
            'remember_token' => null,
            'email_verified_at' => $this->faker->boolean(33) ? now()->subHours(rand(1, 23)) : null,
        ];
    }
}
