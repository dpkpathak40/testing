<?php

namespace Database\Factories;

use App\Models\Broker;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrokerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Broker::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => rand(0, 1) ? $this->faker->lastName : null,
            'email' => $this->faker->unique()->safeEmail,
            'area_code' => $this->faker->randomNumber(3, true),
            'phone' => $this->faker->randomNumber(7, true),
            'stripe_user_id' => null,
        ];
    }
}
