<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->colorName,
            'description' => rand(0, 1) ? $this->faker->word : null,
            'amount' => rand(50, 500),
            'requires_number_of_homes' => rand(0, 1),
            'active' => $this->faker->boolean(80),
        ];
    }
}
