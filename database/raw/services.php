<?php

return [
    [
        'name' => 'House showing',
        'amount' => 90,
        'image' => 'house-showing.svg',
        'requires_number_of_homes' => true,
        'description' => 'The fee for the first home is $90. Each additional home in the same area is $25.00',
    ],
    [
        'name' => 'Open House',
        'amount' => 115,
        'image' => 'open-house.svg',
        'description' => 'up to 3 hours',
    ],
    [
        'name' => 'Inspection',
        'amount' => 125,
        'image' => 'inspection.svg',
        'description' => 'up to 3 hours',
    ],
    [
        'name' => 'Contractor meeting',
        'amount' => 90,
        'image' => 'contractor-meeting.svg',
        'description' => 'up to 2 hours',
    ],
    [
        'name' => 'Photographer',
        'amount' => 90,
        'image' => 'photographer.svg',
        'description' => 'up to 2 hours',
    ],
    [
        'name' => 'Customization option',
        'amount' => 90,
        'image' => 'customization-option.svg',
    ],
];
