<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->decimal('chargeable_incentive_amount', 10, 2)->nullable()->after('transferable_amount');
            $table->decimal('transferable_incentive_amount', 10, 2)->nullable()->after('chargeable_incentive_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('chargeable_incentive_amount');
            $table->dropColumn('transferable_incentive_amount');
        });
    }
}
