<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class AddColumnsToJobNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_notes', function (Blueprint $table) {
            $table->unsignedInteger('sender_id')->default(1)->after('job_id');
            $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('attachment')->after('note')->nullable();
            $table->renameColumn('note', 'message');
            $table->timestamp('read_at')->nullable()->after('attachment');
        });

        Schema::table('job_notes', function (Blueprint $table) {
            $table->longText('message')->nullable()->change();
        });

        Schema::rename('job_notes', 'job_chats');

        Artisan::call(\App\Console\Commands\Migration\NotesMigration::class, [
            '--force' => true,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_chats', function (Blueprint $table) {
            $table->dropColumn('sender_id');
            $table->dropColumn('attachment');
            $table->dropColumn('read_at');
        });
    }
}
