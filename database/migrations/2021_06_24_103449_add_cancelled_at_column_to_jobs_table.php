<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCancelledAtColumnToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dateTime('cancelled_at')->nullable()->after('status');
            $table->mediumText('cancellation_reason')->nullable()->after('status');

            $table->unsignedInteger('cancelled_by_admin_id')->nullable()->after('status');
            $table->foreign('cancelled_by_admin_id')
                ->references('id')
                ->on('admins')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            //
        });
    }
}
