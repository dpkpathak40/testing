<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('creator_user_id');
            $table->foreign('creator_user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('assignee_user_id')->nullable();
            $table->foreign('assignee_user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

            $table->date('due_date');
            $table->decimal('chargeable_amount', 10, 2);
            $table->decimal('transferable_amount', 10, 2);

            $table->text('description')->nullable();
            $table->tinyInteger('number_of_homes')->nullable()->unsigned();
            $table->string('status')->default(\App\Models\Job::STATUS_PENDING);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
