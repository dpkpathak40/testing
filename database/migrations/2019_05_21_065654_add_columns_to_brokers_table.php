<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToBrokersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brokers', function (Blueprint $table) {
            $table->string('area_code')->nullable()->after('email');
            $table->string('w9_document')->nullable()->after('stripe_user_id');
            $table->string('owner_name')->nullable()->after('stripe_user_id');
            $table->string('company_name')->nullable()->after('stripe_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brokers', function (Blueprint $table) {
            $table->dropColumn('area_code');
            $table->dropColumn('w9_document');
            $table->dropColumn('owner_name');
            $table->dropColumn('company_name');
        });
    }
}
