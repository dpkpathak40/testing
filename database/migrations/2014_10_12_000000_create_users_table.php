<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');

            $table->string('stripe_customer_id')
                ->nullable()
                ->unique()
                ->collation('utf8mb4_bin');

            $table->dateTime('email_verified_at')->nullable();
            $table->string('documents_status')->nullable();

            $table->unsignedInteger('broker_id')->nullable();
            $table->foreign('broker_id')->references('id')->on('brokers')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
