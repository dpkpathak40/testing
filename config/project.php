<?php

return [

    'media' => [
        'avatar' => [
            'max_size' => 10000, // KB
            'min_width' => 150, // px
            'min_height' => 150, // px
        ],
        'service' => [
            'max_size' => 500, // KB
            'min_width' => 150, // px
            'min_height' => 150, // px
        ],
        'user_document' => [
            'allowed_mimes' => 'jpeg,jpg,png,doc,docx,pdf,rtf,odt',
            'max_size' => 8000, //KB
        ],
        'broker_document' => [
            'allowed_mimes' => 'jpeg,jpg,png,doc,docx,pdf,rtf,odt',
            'max_size' => 8000, //KB
        ],
        'chat_document' => [
            'allowed_mimes' => 'jpeg,jpg,png,doc,docx,pdf,rtf,odt',
            'max_size' => 8000, //KB
        ],
    ],

    'job' => [
        'max_number_of_applicants' => 5,
        'max_number_of_homes' => 10,
        'max_number_of_documents' => 10,
    ],

    'service' => [
        'per_home_amount' => 25, // dollars
    ],

    'transferable_percentage' => 80, // %

    'incentive_percentage' => 4, // %

    'admin_email_address' => env('ADMIN_EMAIL_ADDRESS'),

];
