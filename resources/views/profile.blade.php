@extends('_layouts.app')
@section('pageTitle','Profile')

@section('content')
  <div class="container mid-section view-profile">

    <section class="card-white text-center">
      @if(auth('web')->user()->is($user))
        <a href="{{route('account.profile.edit')}}" class="btn-edit">
          <i class="fas fa-pen fa-lg"></i>
        </a>
      @endif
      <div class="img-round round-big">
        <img src="{{$user->profile->avatar_thumb_url}}" alt="avatar">
      </div>
      <h3 class="mt-5">{{$user->profile->full_name}}</h3>
      <p>Company Name: {{$user->broker->company_name}}</p>
      <div class="stars">
        @foreach(range(1,5) as $rating)
          @if($loop->iteration <= (int)$user->rating)
            <i class="fas fa-star text-warning"></i>
          @else
            <i class="fas fa-star text-muted"></i>
          @endif
        @endforeach
      </div>
      <div class="col-md-8 offset-md-2 mt-5">
        <p>{{$user->profile->bio}}</p>
      </div>
    </section>

    <section class="card-white">
      <h4>Accomplishments</h4>
      @foreach($accomplishments as $item)
        <div
          class="profile-item">{{$item->total}} {{$item->service->name}} {{\Illuminate\Support\Str::plural('Job',$item->total)}}
          done successfully.
        </div>
      @endforeach
    </section>

    <section class="card-white">
      <h4>Services</h4>
      <div class="profile-item">
        <h4>
          @foreach($user->services as $item)
            <span class="badge badge-pill badge-secondary">{{$item->name}}</span>
          @endforeach
        </h4>
      </div>
    </section>

    <section class="card-white">
      <h4>Work Cities</h4>
      <div class="profile-item">
        <h4>
          @foreach($user->cities as $item)
            <span class="badge badge-pill badge-secondary mb-3">{{$item->name}}</span>
          @endforeach
        </h4>
      </div>
    </section>

    <section class="card-white">
      <h4>Background</h4>
      <div class="profile-item">
        <strong>Education</strong>
        <p>{{$user->profile->education}}</p>
      </div>
      <div class="profile-item">
        <strong>Experience</strong>
        <p>{{$user->profile->experience}}</p>
      </div>
    </section>

    <section class="card-white contact-info">
      <h4>Contact Information</h4>
      <div class="profile-item">
        <strong>Area code</strong>
        <span>@if($user->address->area_code)(+1){{$user->address->area_code}}@endif</span>
      </div>
      <div class="profile-item">
        <strong>Phone</strong>
        <span>{{$user->address->primary_phone}}</span>
      </div>
      <div class="profile-item">
        <strong>Email</strong>
        <span>{{$user->email}}</span>
      </div>
      <div class="profile-item">
        <strong>City</strong>
        <span>{{$user->address->city}}</span>
      </div>
      <div class="profile-item">
        <strong>State</strong>
        <span>{{$user->address->state}}</span>
      </div>
      <div class="profile-item">
        <strong>Address</strong>
        <span>{{$user->address->street_address}}</span>
      </div>
    </section>

    @if(auth('web')->user()->is($user))
      <section class="card-white contact-info">
        <h4>Broker Information</h4>
        <div class="profile-item">
          <strong>Name</strong>
          <span>{{$user->broker->full_name}}</span>
        </div>
        <div class="profile-item">
          <strong>Phone</strong>
          <span>{{$user->broker->phone}}</span>
        </div>
        <div class="profile-item">
          <strong>Email</strong>
          <span>{{$user->broker->email}}</span>
        </div>
      </section>
    @endif

  </div>
@endsection
