@extends('_layouts.app')
@section('pageTitle','Log in')

@section('content')
  @include('_partials.logo')

  <form action="{{ route('login') }}" method="POST">
    @csrf
    <div class="form-wrap">
      @include('alert::bootstrap')

      <h1>Log In</h1>
      <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control @isInvalid('email')" id="email" name="email"
               placeholder="Enter your email" required autofocus value="{{ old('email') }}">
        @validationMessage('email')
      </div>

      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control @isInvalid('password')" id="password" name="password"
               placeholder="Enter your password" required>
        @validationMessage('password')
      </div>

      <div class="form-group text-center">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="remember"
                 name="remember" {{ old('remember') ? 'checked' : '' }}>
          <label class="custom-control-label" for="remember">Remember me</label>
        </div>
      </div>

      <div class="form-group text-center">
        <button type="submit" class="btn-purple btn-large mt-3">Login</button>
      </div>

      <div class="form-footer text-center mt-5">
        <div class="mb-5">
          <a href="{{ route('password.request') }}" class="float-left">Forgot Password?</a>
          <a href="{{ route('verification.resend') }}" class="float-right">Resend Verification?</a>
        </div>
        <div>Don’t have an account? <a href="{{ route('register') }}">Sign Up Free</a></div>
      </div>
    </div>
  </form>
@endsection
