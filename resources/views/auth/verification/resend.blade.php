@extends('_layouts.app')
@section('pageTitle','Resend Confirmation')

@section('content')
  @include('_partials.logo')

  <form action="{{ route('verification.resend') }}" method="POST">
    @csrf
    <div class="form-wrap">
      @include('alert::bootstrap')

      <h1>Resend verification email</h1>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control @isInvalid('email')" id="email" name="email"
               placeholder="Enter your email" value="{{ old('email') }}" required autofocus>
        @validationMessage('email')
      </div>

      <div class="form-group text-center">
        <button type="submit" class="btn-purple btn-large">Resend Verification</button>
      </div>

      <div class="form-footer text-center">
        <div>Already have an verified account? <a href="{{ route('login') }}">Login</a></div>
      </div>
    </div>
  </form>

@endsection
