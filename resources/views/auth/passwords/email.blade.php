@extends('_layouts.app')
@section('pageTitle','Recover Password')

@section('content')
  @include('_partials.logo')

  <form action="{{ route('password.email') }}" method="POST">
    @csrf
    <div class="form-wrap">
      @include('alert::bootstrap')

      @if (session('status'))
        <div class="alert alert-success alert-dismissible show" role="alert">
          {{ session('status') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <h1>Recover your lost password</h1>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control @isInvalid('email')" id="email" name="email"
               placeholder="Enter your registered email" value="{{ old('email') }}" required autofocus>
        @validationMessage('email')
      </div>

      <div class="form-group text-center">
        <button type="submit" class="btn-purple btn-large">Submit</button>
      </div>

      <div class="form-footer text-center mt-5">
        <a href="{{ route('login') }}">Back to Login</a>
      </div>
    </div>
  </form>
@endsection
