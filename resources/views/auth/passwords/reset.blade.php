@extends('_layouts.app')
@section('pageTitle','Reset Password')

@section('content')
  @include('_partials.logo')

  <form action="{{ route('password.request') }}" method="POST">
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="form-wrap">
      @include('alert::bootstrap')

      <h1>Reset Password</h1>
      <div class="form-group">
        <label for="email">Email Address</label>
        <input type="email" class="form-control @isInvalid('email')" id="email" name="email"
               placeholder="Enter your email" value="{{ $email ?? old('email') }}" required autofocus>
        @validationMessage('email')
      </div>

      <div class="form-group">
        <label for="password">New Password</label>
        <input type="password" class="form-control @isInvalid('password')" id="password" name="password"
               placeholder="Enter your password" required>
        @validationMessage('password')
      </div>

      <div class="form-group">
        <label for="password_confirmation">Confirm new password</label>
        <input type="password" class="form-control" id="password_confirmation"
               name="password_confirmation"
               placeholder="Re-Enter your password" required>
      </div>

      <div class="form-group text-center">
        <button type="submit" class="btn-purple btn-large">Reset Password</button>
      </div>
      <div class="form-footer text-center mt-5">
        <a href="{{ route('login') }}">Back to Login</a>
      </div>
    </div>
  </form>
@endsection
