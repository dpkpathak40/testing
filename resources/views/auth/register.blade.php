@extends('_layouts.app')
@section('pageTitle','Sign up')

@section('content')
  @include('_partials.logo')

  <form action="{{ route('register') }}" method="POST">
    @csrf
    <div class="form-wrap">
      @include('alert::bootstrap')

      <h1>Sign up</h1>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control @isInvalid('first_name')" name="first_name"
                   placeholder="Enter your first name" value="{{ old('first_name') }}" required>
            @validationMessage('first_name')
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control @isInvalid('last_name')" name="last_name"
                   placeholder="Enter your last name" value="{{ old('last_name') }}">
            @validationMessage('last_name')
          </div>
        </div>
      </div>

      <div class="form-group">
        <label>Email Address</label>
        <input type="email" class="form-control @isInvalid('email')" name="email"
               placeholder="Enter your email" value="{{ old('email') }}" required>
        @validationMessage('email')
      </div>

      <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control @isInvalid('password')" name="password"
               placeholder="Enter your password" required>
        @validationMessage('password')
      </div>

      <div class="form-group">
        <label>Confirm Password</label>
        <input type="password" class="form-control"
               name="password_confirmation"
               placeholder="Re-Enter your password" required>
      </div>

      <div class="custom-control custom-checkbox">
        <input type="checkbox" id="terms-and-condition" class="custom-control-input"
               name="terms_and_condition" value="1" required>
        <label class="custom-control-label" for="terms-and-condition">I Agree with Terms of Service and Privacy
          Policy</label>
        @validationMessage('terms_and_condition')
      </div>

      <div class="form-group text-center">
        @honeypot
        <button type="submit" id="register-submit" class="btn-purple btn-large">Sign up</button>
      </div>

      <div class="form-footer text-center mt-5">
        <div class="policy-text">
          By clicking on "Sign up" you agree to {{config('app.name')}}'s <a
            href="http://oktohub.com/terms-and-conditions/" target="_blank">Terms of Service</a> and <a
            href="http://oktohub.com/privacy-policy/" target="_blank">Privacy Policy</a>.
        </div>
        <div>Already have an account? <a href="{{ route('login') }}">Login</a></div>
        <div><a href="{{ route('verification.resend') }}">Resend verification email?</a></div>
      </div>
    </div>
  </form>
@endsection
