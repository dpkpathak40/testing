@extends('_layouts.app')
@section('pageTitle','ToDo')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Received Jobs
      @endcomponent

      @include('receivedJobs._tabs')
      <received-jobs-to-do-card :jobs='@json($toDoJobs->items())'></received-jobs-to-do-card>
      <div class="pagination-center">
        {{ $toDoJobs->links() }}
      </div>
    </div>
  </section>
  <job-done-modal></job-done-modal>
@endsection
