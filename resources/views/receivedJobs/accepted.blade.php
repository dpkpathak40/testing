@extends('_layouts.app')
@section('pageTitle','Accepted')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Received Jobs
      @endcomponent

      @include('receivedJobs._tabs')
      <received-jobs-accepted-card :jobs='@json($acceptedJobs->items())'></received-jobs-accepted-card>
      <div class="pagination-center">
        {{ $acceptedJobs->links() }}
      </div>
    </div>
  </section>
@endsection
