<div class="tabs-received-jobs received-jobs">
  <a href="{{route('receivedJobs.received')}}" class="{{active_route('receivedJobs.received')}}">
    {{$counts->received_jobs}} <span>Received</span>
  </a>
  <a href="{{route('receivedJobs.accepted')}}" class="{{active_route('receivedJobs.accepted')}}">
    {{$counts->accepted_jobs}} <span>Accepted</span>
  </a>
  <a href="{{route('receivedJobs.toDo')}}" class="{{active_route('receivedJobs.toDo')}}">
    {{$counts->to_do_jobs}} <span>To Do</span>
  </a>
  <a href="{{route('receivedJobs.completed')}}" class="{{active_route('receivedJobs.completed')}}">
    {{$counts->completed_jobs}} <span>Completed</span>
  </a>
</div>
