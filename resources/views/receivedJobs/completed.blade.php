@extends('_layouts.app')
@section('pageTitle','Completed')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Received Jobs
      @endcomponent

      @include('receivedJobs._tabs')
      <received-jobs-completed-card :jobs='@json($completedJobs->items())'></received-jobs-completed-card>
      <div class="pagination-center">
        {{ $completedJobs->links() }}
      </div>
    </div>
  </section>
@endsection
