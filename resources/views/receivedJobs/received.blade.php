@extends('_layouts.app')
@section('pageTitle','Received')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Received Jobs
      @endcomponent

      @include('receivedJobs._tabs')
      <received-jobs-received-card :jobs='@json($receivedJobs->items())'></received-jobs-received-card>

      <div class="pagination-center">
        {{ $receivedJobs->links() }}
      </div>
    </div>
  </section>
  <job-accept-modal></job-accept-modal>
  <job-ignore-modal></job-ignore-modal>
@endsection
