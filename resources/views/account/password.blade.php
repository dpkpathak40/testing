@extends('_layouts.app')
@section('pageTitle','Password')

@section('content')
  <div class="container">
    @component('components.breadcrumb')
      Password
    @endcomponent
  </div>

  <div class="container mid-section view-profile pt-0">
    @include('alert::bootstrap')
    <form method="POST" action="{{route('account.password.update')}}">
      @csrf
      <div class="card-white wrap-form mt-0">
        <h4>Update your password</h4>
        <div class="form-group">
          <label>Current password</label>
          <input type="password" name="current_password"
                 class="form-control @isInvalid('current_password')" placeholder="Current password" required autofocus>
          @if ($errors->has('current_password'))
            <div class="invalid-feedback">
              {{ $errors->first('current_password') }}
            </div>
          @else
            <small class="form-text text-muted">You must provide your current password in order to change it.
            </small>
          @endif
        </div>

        <div class="form-group">
          <label>New password</label>
          <input type="password" name="password"
                 class="form-control @isInvalid('password')" placeholder="New password" required>
          @validationMessage('password')
        </div>

        <div class="form-group">
          <label>Confirm new password</label>
          <input type="password" name="password_confirmation"
                 class="form-control" placeholder="Confirm new password" required>
        </div>
        <div class="form-group text-right">
          <button type="submit" class="btn btn-purple">
            <i class="fas fa-fw fa-pencil-alt"></i> Update
          </button>
        </div>
      </div>
    </form>
  </div>
@endsection
