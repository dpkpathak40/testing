@extends('_layouts.app')
@section('pageTitle','Profile')

@section('content')
  <div class="container">
    @component('components.breadcrumb')
      Profile
    @endcomponent
  </div>

  <div class="container mid-section view-profile pt-0">
    <edit-profile :user='@json($user)' :services='@json($services)' :cities='@json($cities)'></edit-profile>
  </div>
@endsection
