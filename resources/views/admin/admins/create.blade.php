@extends('admin._layouts.admin')
@section('pageTitle','Create broker')

@section('content')

  @component('admin.components.breadcrumb',[
       'links' => [
         'Admins' => route('admin.admins.index')
       ]
     ])
    Create admin
  @endcomponent

  @include('alert::bootstrap')

  <form method="POST" action="{{route('admin.admins.store')}}">
    {{csrf_field()}}

    <div class="card mb-3">
      <div class="card-body">
        <h5 class="card-title">Create new admin user</h5>

        <div class="form-group required">
          <label for="email">E-Mail address</label>
          <input id="email" type="email" class="form-control @isInvalid('email')" name="email" placeholder="E-Mail"
                 value="{{old('email')}}"
                 required autofocus>
          @validationMessage('email')
        </div>
        <p class="card-text font-weight-light">User will receive an password reset email.</p>
      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-check-circle"></i> Create admin
        </button>
      </div>
    </div>
  </form>

@endsection
