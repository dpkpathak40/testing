<form onsubmit="return confirm('Are you sure?')" action="{{route('admin.admins.actions.passwordResetEmail',$admin)}}"
      method="POST">
  @csrf
  <div class="card my-4">
    <div class="card-body">
      <h5 class="card-title">Send password reset e-mail</h5>
      <p class="card-text font-weight-light">
        An email with password reset instructions will be send to this user e-mail address.
      </p>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-outline-primary">
        <i class="fas fa-envelope"></i> Send password reset e-mail
      </button>
    </div>
  </div>
</form>
