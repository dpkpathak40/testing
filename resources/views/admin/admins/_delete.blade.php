<form onsubmit="return confirm('Are you sure to delete this admin user?')"
      action="{{route('admin.admins.destroy',$admin)}}"
      method="POST">
  @csrf
  @method('delete')
  <div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title text-danger">Delete admin</h5>

      <p class="card-text font-weight-light">
        This operation can't be undone.
      </p>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-danger">
        <i class="fas fa-trash"></i> Delete admin
      </button>
    </div>
  </div>
</form>
