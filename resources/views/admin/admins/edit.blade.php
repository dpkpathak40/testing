@extends('admin._layouts.admin')
@section('pageTitle','Edit Admin')

@section('content')

  @component('admin.components.breadcrumb',[
      'links' => [
        'Admins' => route('admin.admins.index')
      ]
    ])
    Edit admin
  @endcomponent

  @include('alert::bootstrap')

  <form method="POST" action="{{route('admin.admins.update',$admin)}}">
    @csrf
    @method('PUT')

    <section class="card mb-3">
      <div class="card-body">
        <h5 class="card-title">Edit admin</h5>

        <div class="form-group required">
          <label for="email">E-Mail address</label>
          <input id="email" type="email" class="form-control @isInvalid('email')" name="email" placeholder="E-Mail"
                 value="{{old('email',$admin->email)}}"
                 required autofocus>
          @validationMessage('email')
        </div>
        <p class="card-text font-weight-light">User will receive an password reset email on new email address.</p>
      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-pencil-alt"></i> Update admin
        </button>
      </div>
    </section>
  </form>
  @include('admin.admins._passwordReset')
  @include('admin.admins._updateBlockStatus')
  @include('admin.admins._delete')
@endsection
