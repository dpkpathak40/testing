@extends('admin._layouts.admin')
@section('pageTitle','Admins')

@section('content')

  <section class="row d-flex">
    <div class="col">
      @component('admin.components.breadcrumb')
        Admins
      @endcomponent
    </div>
    <div class="col pt-1 text-right">
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin.admins.create')}}">
            <i class="fas fa-plus-circle"></i> Add new admin
          </a>
        </li>
      </ul>
    </div>
  </section>

  @include('alert::bootstrap')

  <section class="card mb-3">
    <div class="card-body">
      <form id="search-form" class="form-inline" method="GET" action="{{route('admin.admins.index')}}">
        <select class="form-control mb-2 mb-sm-0 mr-sm-2" name="per_page">
          <option disabled="">Per page</option>
          @foreach([10,30,50] as $perPage)
            <option value="{{$perPage}}"
                    @if(request('per_page') == $perPage) selected @endif>{{$perPage}}</option>
          @endforeach
        </select>

        <input type="text" class="form-control mb-2 mb-sm-0 mr-sm-2" placeholder="Search" name="search"
               value="{{request('search')}}" autofocus>

        <button type="submit" class="btn btn-primary mb-0 mb-sm-0 mr-sm-2">
          <i class="fa fa-search fa-fw"></i>Search
        </button>
      </form>
    </div>
  </section>

  <section class="table-responsive mb-3">
    <table class="table table-striped  table-bordered table-hover">
      <thead>
      <tr>
        <th>Email</th>
        <th>Created at</th>
        <th>Account Status</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      @forelse($admins as $admin)
        <tr>
          <td>{{$admin->email}}</td>
          <td>@date($admin->created_at)</td>
          <td class="align-middle h5">
            @if($admin->is_blocked)
              <span class="badge badge-danger">Blocked</span>
            @else
              <span class="badge badge-success">Active</span>
            @endif
          </td>
          <td class="text-center">
            <a href="{{route('admin.admins.edit',$admin)}}" class="btn btn-sm btn-outline-info mb-0">
              <i class="fa fa-edit"></i> Edit</a>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="6" class="text-center">No records found</td>
        </tr>
      @endforelse
      </tbody>
    </table>
  </section>

  @if($admins->total() > 0)
    <div class="row">
      <div class="col-md-4 mb-sm-0 mb-3 text-center text-sm-left">
        <h5 class="mt-sm-2 mt-0 mb-0">Found {{$admins->total()}} entries</h5>
      </div>
      <div class="col-md-8 d-flex">
        <div class="mx-auto ml-sm-auto mr-sm-0 table-responsive-sm">
          {{$admins->appends(request()->query())->links()}}
        </div>
      </div>
    </div>
  @endif
@endsection
