@extends('admin._layouts.admin')
@section('pageTitle','Login')

@section('content')
  <div class="row justify-content-center">
    <section class="col-sm-5">
      @include('admin._partials.logo')

      @include('alert::bootstrap')

      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Log in to your account</h5>
          <form method="POST" action="{{ route('admin.login') }}">
            @csrf

            <div class="form-group">
              <label for="email">E-mail address</label>
              <input id="email" type="email" class="form-control @isInvalid('email')"
                     name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

              @validationMessage('email')
            </div>

            <div class="form-group">
              <label for="password">Password</label>
              <input id="password" type="password"
                     class="form-control @isInvalid('password')"
                     name="password" required placeholder="Password">

              @validationMessage('password')
            </div>

            <div class="form-group">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" name="remember"
                       {{ old('remember') ? 'checked' : '' }} class="custom-control-input" id="input-remember">
                <label class="custom-control-label" for="input-remember">Remember me</label>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">
                <i class="fas fa-sign-in-alt"></i> Log in
              </button>
            </div>
          </form>
        </div>
      </div>
      <div class="text-center">
        <a href="{{ route('admin.password.request') }}">
          Forgot your password?
        </a>
      </div>
    </section>
  </div>
@endsection
