@extends('admin._layouts.admin')
@section('pageTitle','Reset password')

@section('content')

  <div class="row justify-content-center">
    <div class="col-md-5">

      @include('admin._partials.logo')

      @include('alert::bootstrap')

      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Reset your password</h5>
          <h6 class="card-subtitle text-muted">You need to provide your registered e-mail address.</h6>

          <form class="mt-2" method="POST" action="{{ route('admin.password.request') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
              <label for="email"> E-mail address</label>

              <input id="email" type="email" class="form-control @isInvalid('email')"
                     name="email" value="{{ $email ?? old('email') }}" placeholder="E-mail" required autofocus>
              @validationMessage('email')
            </div>

            <div class="form-group">
              <label for="password">New password</label>

              <input id="password" type="password"
                     class="form-control @isInvalid('password')" name="password"
                     placeholder="New password" required>

              @validationMessage('password')
            </div>

            <div class="form-group">
              <label for="password-confirm">Confirm new password</label>

              <input id="password-confirm" type="password"
                     class="form-control"
                     name="password_confirmation" placeholder="Confirm new password" required>
            </div>

            <div class="form-group mb-0">
              <button type="submit" class="btn btn-primary btn-block">
                <i class="fas fa-check"></i> Reset password
              </button>
            </div>
          </form>
        </div>
      </div>
      <div class="text-center">
        <a href="{{ route('admin.login') }}">
          Back to Log in
        </a>
      </div>
    </div>
  </div>

@endsection
