<form onsubmit="return confirm('Are you sure to delete this broker?')"
      action="{{route('admin.brokers.destroy',$broker)}}"
      method="POST">
  @csrf
  @method('delete')
  <div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title text-danger">Delete broker</h5>

      @if($errors->delete->has('broker'))
        <div class="alert alert-danger alert-dismissible show" role="alert">
          <i class="fas fa-exclamation-circle"></i> {{ $errors->delete->first('broker') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <p class="card-text font-weight-light">
        You can only delete brokers those are not attached with any realtor.<br>
        This operation can't be undone.
      </p>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-danger">
        <i class="fas fa-trash"></i> Delete broker
      </button>
    </div>
  </div>
</form>
