@extends('admin._layouts.admin')
@section('pageTitle','Create broker')

@section('content')

  @component('admin.components.breadcrumb',[
       'links' => [
         'brokers' => route('admin.brokers.index')
       ]
     ])
    Create broker
  @endcomponent

  @include('alert::bootstrap')

  <form method="POST" action="{{route('admin.brokers.store')}}" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="card mb-3">
      <div class="card-body">
        <h5 class="card-title">Create new broker</h5>

        <div class="form-group required">
          <label for="email">E-Mail address</label>
          <input id="email" type="email" class="form-control @isInvalid('email')" name="email" placeholder="E-Mail"
                 value="{{old('email')}}"
                 required autofocus>
          @validationMessage('email')
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group required">
              <label for="first_name">First name</label>
              <input id="first_name" type="text" class="form-control @isInvalid('first_name')" name="first_name"
                     placeholder="First name" value="{{old('first_name')}}"
                     required>
              @validationMessage('first_name')
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="last_name">Last name</label>
              <input id="last_name" type="text" class="form-control @isInvalid('last_name')" name="last_name"
                     placeholder="Last name" value="{{old('last_name')}}">
              @validationMessage('last_name')
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <div class="form-group required">
              <label for="area-code">Area Code</label>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">+1</div>
                </div>
                <input id="area-code" type="tel" name="area_code" class="form-control @isInvalid('area_code')"
                       placeholder="Area Code" value="{{old('area_code')}}" required>
                @validationMessage('area_code')
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <div class="form-group required">
              <label for="phone">Primary phone</label>
              <input id="phone" type="tel" name="phone" class="form-control @isInvalid('phone')"
                     placeholder="Primary phone" value="{{old('phone')}}" required>
              @validationMessage('phone')
            </div>
          </div>
        </div>

        <div class="form-group required">
          <label for="w9-document">Broker's W9 Document</label>
          <div class="custom-file">
            <input type="file" class="custom-file-input form-control @isInvalid('w9_document')"
                   id="input-file-w9-document" name="w9_document" required>
            <label class="custom-file-label" for="input-file-w9-document">Choose file</label>
            @validationMessage('w9_document')
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="owner_name">Owner name</label>
              <input id="first_name" type="text" class="form-control @isInvalid('owner_name')" name="owner_name"
                     placeholder="Owner name" value="{{old('owner_name')}}"
              >
              @validationMessage('owner_name')
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="company_name">Company name</label>
              <input id="company_name" type="text" class="form-control @isInvalid('company_name')" name="company_name"
                     placeholder="Company name" value="{{old('company_name')}}">
              @validationMessage('company_name')
            </div>
          </div>
        </div>

      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-check-circle"></i> Create broker
        </button>
      </div>
    </div>
  </form>

@endsection
