<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Transactions Report</title>
  <style>
    body {
      background: #FFFFFF;
      color: #000000;
      font-family: "Arial", "Verdana", "Roboto", sans-serif;
    }

    h1 {
      font-size: 30px !important;
    }

    h2 {
      font-size: 20px !important;
    }

    h5 {
      font-size: 15px !important;
    }

    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 1px solid #222;
    }

    table {
      width: 100%;
    }

    th {
      height: 50px;
    }

    table th,
    table td {
      text-align: center;
      padding: 0.75rem;
      font-size: 12px !important;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-muted {
      color: #666666;
    }

    .font-weight-normal {
      font-weight: normal;
    }
  </style>
</head>
<body>
<main class="text-center">
  <section>
    <header>
      <img src="{{asset('assets/logo-large.png')}}" alt="{{config('app.name')}}" height="50">
      <h1>Transactions Report</h1>
      <h5 class="text-muted font-weight-normal">
        Generated for: <b>{{ $broker->email }}</b> on {{ now()->toDateString() }}
      </h5>
    </header>
    <table>
      <thead>
      <tr>
        <th>Job ID</th>
        <th>Realtor Name</th>
        <th>Realtor Email</th>
        <th>Service</th>
        <th>Status</th>
        <th>Amount</th>
        <th>Created At</th>
        <th>Due Date</th>
        <th>Transaction ID</th>
      </tr>
      </thead>
      <tbody>
      @foreach($users as $user)
        @foreach($user->assignedJobs as $job)
          <tr>
            <td>{{ $job->getKey() }}</td>
            <td>{{ $user->profile->full_name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $job->service->name }}</td>
            <th>{{ ucfirst($job->status) }}</th>
            @if(!empty($job->transfer))
              <td>{{ money($job->transfer->total_amount) }}</td>
            @else
              <td> -</td>
            @endif
            <td>{{ $job->created_at->format('M d Y, h:i a') }}</td>
            <td>{{ $job->due_date->format('M d Y, h:i a') }}</td>
            @if(!empty($job->transfer))
              <td>{{ $job->transfer->transaction_id }}</td>
            @else
              <td> -</td>
            @endif
          </tr>
        @endforeach
      @endforeach
      </tbody>
    </table>
  </section>
  <footer>
    <div class="text-right">
      <h5>Total Amount: {{ money($totalAmount) }}</h5>
    </div>
  </footer>
</main>
</body>
</html>

