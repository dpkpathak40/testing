<form onsubmit="return confirm('Are you sure to send invite?')"
      action="{{route('admin.brokers.actions.stripeInvite',$broker)}}"
      method="POST">
  @csrf
  @method('post')
  <div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title">Stripe connect invite</h5>
      <p class="card-text font-weight-light">
        Broker will receive an email with a link to stripe connect.
      </p>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-outline-primary">
        <i class="fas fa-envelope"></i> Send invite
      </button>
    </div>
  </div>
</form>
