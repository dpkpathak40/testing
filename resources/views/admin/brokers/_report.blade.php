<form method="POST" action="{{ route('admin.brokers.report',$broker) }}">
  @csrf

  <div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title">Report Generation</h5>

      <div class="row">
        <div class="col-6">
          <div class="form-group required">
            <label for="start-date">Start Date</label>
            <div class="form-group">
              <input type="text" id="start-date" class="form-control @isInvalid('start_date')" name="start_date"
                     value="{{old('start_date')}}"
                     required
                     autofocus/>
              @validationMessage('start_date')
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="form-group required">
            <label for="end-date">End Date</label>
            <div class="form-group">
              <input type="text" id="end-date" class="form-control @isInvalid('end_date')" name="end_date"
                     value="{{old('end_date')}}"
                     required/>
              @validationMessage('end_date')
            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn  btn-outline-primary">
        <i class="fas fa-file-pdf"></i> Send Report
      </button>
    </div>
  </div>
</form>
