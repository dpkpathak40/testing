@extends('admin._layouts.admin')
@section('pageTitle','Dashboard')

@section('content')

  @component('admin.components.breadcrumb')
    Dashboard
  @endcomponent

  <section class="row">
    <div class="col-md-4 mb-3">
      <div class="card shadow-sm">
        <div class="card-body row ">
          <div class="col-9 text-left">
            <h2>{{$counts->users}}</h2>
          </div>
          <div class="col-3 text-muted text-right">
            <i class="fas fa-2x fa-users"></i>
          </div>
        </div>
        <div class="card-footer text-muted bg-white">
          Total users
        </div>
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <div class="card shadow-sm">
        <div class="card-body row ">
          <div class="col-9 text-left">
            <h2>{{$counts->jobs}}</h2>
          </div>
          <div class="col-3 text-muted text-right">
            <i class="fas fa-2x fa-cubes"></i>
          </div>
        </div>
        <div class="card-footer text-muted bg-white">
          Total jobs posted
        </div>
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <div class="card shadow-sm">
        <div class="card-body row ">
          <div class="col-9 text-left">
            <h2>{{money($counts->charged)}}</h2>
          </div>
          <div class="col-3 text-muted text-right">
            <i class="fas fa-2x fa-credit-card"></i>
          </div>
        </div>
        <div class="card-footer text-muted bg-white">
          Total charged
        </div>
      </div>
    </div>
  </section>

@endsection
