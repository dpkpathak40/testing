@extends('admin._layouts.admin')
@section('pageTitle','Services')

@section('content')

  <section class="row d-flex">
    <div class="col">
      @component('admin.components.breadcrumb')
        Services
      @endcomponent
    </div>
    <div class="col pt-1 text-right">
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin.services.create')}}">
            <i class="fas fa-plus-circle"></i> Add new service
          </a>
        </li>
      </ul>
    </div>
  </section>

  @include('alert::bootstrap')

  <section class="card mb-3">
    <div class="card-body">
      <form id="search-form" class="form-inline" method="GET" action="{{route('admin.services.index')}}">
        <select class="form-control mb-2 mb-sm-0 mr-sm-2" name="per_page">
          <option disabled="">Per page</option>
          @foreach([10,30,50] as $perPage)
            <option value="{{$perPage}}"
                    @if(request('per_page') == $perPage) selected @endif>{{$perPage}}</option>
          @endforeach
        </select>

        <input type="text" class="form-control mb-2 mb-sm-0 mr-sm-2" placeholder="Search" name="search"
               value="{{request('search')}}" autofocus>

        <button type="submit" class="btn btn-primary mb-0 mb-sm-0 mr-sm-2">
          <i class="fa fa-search fa-fw"></i>Search
        </button>
      </form>
    </div>
  </section>

  <section class="table-responsive mb-3">
    <table class="table table-striped  table-bordered table-hover">
      <thead>
      <tr>
        <th>Name</th>
        <th>Amount</th>
        <th>Status</th>
        <th>Created at</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      @forelse($services as $service)
        <tr>
          <td>{{$service->name}}</td>
          <td>{{money($service->amount)}}</td>
          <td class="align-middle h5">
            @if($service->active)
              <span class="badge badge-success">Active</span>
            @else
              <span class="badge badge-danger">In-active</span>
            @endif
          </td>
          <td>@date($service->created_at)</td>
          <td class="text-center">
            <a href="{{route('admin.services.edit',$service)}}" class="btn btn-sm btn-outline-info mb-0">
              <i class="fa fa-edit"></i> Edit</a>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="6" class="text-center">No records found</td>
        </tr>
      @endforelse
      </tbody>
    </table>
  </section>

  @if($services->total() > 0)
    <div class="row">
      <div class="col-md-4 mb-sm-0 mb-3 text-center text-sm-left">
        <h5 class="mt-sm-2 mt-0 mb-0">Found {{$services->total()}} entries</h5>
      </div>
      <div class="col-md-8 d-flex">
        <div class="mx-auto ml-sm-auto mr-sm-0 table-responsive-sm">
          {{$services->appends(request()->query())->links()}}
        </div>
      </div>
    </div>
  @endif
@endsection
