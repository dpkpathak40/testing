@extends('admin._layouts.admin')
@section('pageTitle','Create service')

@section('content')

  @component('admin.components.breadcrumb',[
       'links' => [
         'services' => route('admin.services.index')
       ]
     ])
    Create service
  @endcomponent

  @include('alert::bootstrap')

  <form method="POST" action="{{route('admin.services.store')}}" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="card mb-3">
      <div class="card-body">
        <h5 class="card-title">Create new service</h5>

        <div class="form-row">
          <div class="col-md-6">
            <div class="form-group required">
              <label>Name</label>
              <input type="text" class="form-control @isInvalid('name')" name="name" placeholder="Name"
                     value="{{old('name')}}"
                     required autofocus>
              @validationMessage('name')
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Description</label>
              <input type="text" class="form-control @isInvalid('description')" name="description"
                     placeholder="Description"
                     value="{{old('description')}}">
              @validationMessage('description')
            </div>
          </div>
        </div>

        <div class="form-group required">
          <label>Amount</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
            </div>
            <input type="number" min="1" class="form-control @isInvalid('amount')" name="amount"
                   placeholder="Amount" value="{{old('amount')}}" required>
          </div>
          @validationMessage('amount')
        </div>

        <div class="form-group">
          <label>Image</label>
          <div class="custom-file">
            <input type="file" name="image" class="custom-file-input @isInvalid('image')" id="service-image"
                   accept="image/*">
            <label class="custom-file-label" for="service-image">Choose file</label>
            @validationMessage('image')
          </div>
        </div>

        <div class="form-row">
          <div class="col-md-6">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="input-requires_number_of_homes"
                     name="requires_number_of_homes"
                     @if(old('requires_number_of_homes')) checked @endif value="1"
              >
              <label class="custom-control-label" for="input-requires_number_of_homes">Requires number of homes?</label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="input-active" name="active"
                     @if(old('active',true)) checked @endif value="1"
              >
              <label class="custom-control-label" for="input-active">Is Active?</label>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-check-circle"></i> Create service
        </button>
      </div>
    </div>
  </form>

@endsection
