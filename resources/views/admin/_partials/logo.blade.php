<div class="mt-5 mb-4 text-center">
  <a href="{{url('/')}}">
    <img src="{{ url('assets/logo-large.png') }}" alt="{{config('app.name')}}" width="60%">
  </a>
</div>
