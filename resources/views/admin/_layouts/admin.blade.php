<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="strict-origin-when-cross-origin" name="referrer">
  <meta name="robots" content="noindex, nofollow">

  <title> @yield('pageTitle', 'Dashboard') | {{config('app.name')}}</title>
  <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

  @include('admin._layouts.partials.styles')
  @php
    $appConfig = [
      'csrfToken' => csrf_token(),
      'env' => config('app.env', 'production'),
      'user'=> ['id' => auth()->id()],
    ];
  @endphp
  <script>
    window.appConfig = @json($appConfig)
  </script>
</head>
<body class="{{'env-'.config('app.env', 'production')}} bg-light @yield('bodyClass')">
<div id="admin-app" class="app">
  @auth('admin')
    @include('admin._layouts.partials.navbar')
    <div id="wrapper">
      @include('admin._layouts.partials.sidebar')
      <main class="container-fluid">
        @yield('content')
      </main>
    </div>
  @else
    <main class="container">
      @yield('content')
    </main>
  @endauth
</div>
@include('admin._layouts.partials.scripts')
</body>
</html>
