@stack('before_vendor_scripts')
<script defer src="{{ mix('js/manifest.js', 'dist') }}"></script>
<script defer src="{{ mix('js/vendor.js', 'dist') }}"></script>
@stack('after_vendor_scripts')
@stack('before_app_scripts')
<script defer async src="{{ mix('js/admin.js', 'dist') }}"></script>
@stack('after_app_scripts')
