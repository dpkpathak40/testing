<ul class="sidebar navbar-nav">
  <li class="nav-item {{active_route('admin.dashboard')}}">
    <a class="nav-link" href="{{route('admin.dashboard')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <li class="nav-item {{active_route('admin.users.*')}}">
    <a class="nav-link" href="{{route('admin.users.index')}}">
      <i class="fas fa-fw fa-users"></i>
      <span>Realtors</span></a>
  </li>
  <li class="nav-item {{active_route('admin.brokers.*')}}">
    <a class="nav-link" href="{{route('admin.brokers.index')}}">
      <i class="fas fa-fw fa-user-tie"></i>
      <span>Brokers</span></a>
  </li>
  <li class="nav-item {{active_route('admin.jobs.*')}}">
    <a class="nav-link" href="{{route('admin.jobs.index')}}">
      <i class="fas fa-fw fa-tasks"></i>
      <span>Jobs</span></a>
  </li>
  <li class="nav-item {{active_route('admin.admins.*')}}">
    <a class="nav-link" href="{{route('admin.admins.index')}}">
      <i class="fas fa-fw fa-user"></i>
      <span>Admins</span></a>
  </li>
  <li class="nav-item {{active_route('admin.services.*')}}">
    <a class="nav-link" href="{{route('admin.services.index')}}">
      <i class="fas fa-fw fa-layer-group"></i>
      <span>Services</span></a>
  </li>
</ul>
