<nav class="navbar navbar-expand navbar-light bg-white shadow-sm border-bottom static-top">

  <a class="navbar-brand mr-5" href="{{route('admin.dashboard')}}">
    <img src="{{asset('assets/logo-large.png')}}" class="d-inline-block align-top" height="30"
         alt="{{config('app.name')}}">
  </a>

  <button class="btn btn-link btn-sm order-1 text-black-50 order-sm-0" id="sidebar-toggle" href="#">
    <i class="fas fa-bars"></i>
  </button>

  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="user-dropdown" role="button" data-toggle="dropdown"
         aria-haspopup="true" aria-expanded="false">
        <span class="d-none d-md-inline-block">{{auth('admin')->user()->email}}</span>
        <i class="fas fa-user-circle fa-fw"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="user-dropdown">
        <a class="dropdown-item" href="{{route('admin.account.password.edit')}}"> <i class="fas fa-user-cog"></i>
          Profile</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#"
           onclick="event.preventDefault();document.querySelector('#logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> Log out
        </a>
      </div>
    </li>
  </ul>

  <form hidden id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
    @csrf
  </form>

</nav>
