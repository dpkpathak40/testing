@extends('admin._layouts.admin')
@section('pageTitle','Edit Realtor')

@section('content')

  @component('admin.components.breadcrumb',[
        'links' => [
          'realtors' => route('admin.users.index')
        ]
      ])
    Edit
  @endcomponent

  @include('alert::bootstrap')

  <div class="row">
    <aside class="col-md-3">
      <div class="card">
        <div class="card-body text-center">
          @if($user->profile->avatar)
            <a href="{{$user->profile->avatar_url}}" target="_blank">
              <img class="img-thumbnail rounded" src="{{$user->profile->avatar_thumb_url}}" alt="avatar" width="300">
              <span class="sr-only">View large image</span>
            </a>
          @else
            <i class="fas fa-8x fa-user-circle text-muted"></i>
          @endif
          <p class="h5 my-3 text-truncate">{{optional($user->profile)->full_name}}</p>
          <p class="h5 my-3 text-truncate">{{$user->email}}</p>
          <p>
            Broker: {{$user->broker->full_name}}
          </p>
          <p>
            Company Name: {{$user->broker->company_name}}
          </p>
          <p class="small text-muted mb-0">
            Documents Status: <span class="text-capitalize">{{$user->documents_status}}</span>
          </p>
        </div>
      </div>
    </aside>
    <section class="col-md-9 mt-sm-0 mt-lg-0 mt-4">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Submitted documents</h5>
          <div class="table-responsive">
            <table class="table mb-0">
              <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Download</th>
              </tr>
              </thead>
              <tbody>
              @forelse($user->documents as $item)
                <tr>
                  <th scope="row">{{$loop->iteration}}</th>
                  <td>{{$item->display_name}}</td>
                  <td><a href="{{route('admin.users.documents.show',[$user, $item])}}" target="_blank"><i
                        class="fas fa-download"></i> Download</a></td>
                </tr>
              @empty
                <tr>
                  <td colspan="3" class="text-center">No documents uploaded yet.</td>
                </tr>
              @endforelse
              </tbody>
            </table>
          </div>
        </div>

      </div>

      <form method="POST" action="{{route('admin.users.update',$user)}}">
        @csrf
        @method('PUT')

        <div class="card my-3">
          <div class="card-body">

            <h5 class="card-title">Update documents status</h5>
            <div class="form-group">
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="radio-reject" name="documents_status" class="custom-control-input" required
                       value="{{\App\Models\UserDocument::STATUS_REJECTED}}">
                <label class="custom-control-label" for="radio-reject">Mark documents as Rejected</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="radio-accept" name="documents_status" class="custom-control-input" required
                       value="{{\App\Models\UserDocument::STATUS_VERIFIED}}">
                <label class="custom-control-label" for="radio-accept">Mark documents as Accepted</label>
              </div>
              @validationMessage('documents_status')
            </div>
            <div class="form-group">
              <label>Reject reason</label>
              <textarea class="form-control @isInvalid('reject_reason')" name="reject_reason" rows="2"></textarea>
              @validationMessage('reject_reason')
            </div>
            <p class="text-muted mb-0">
              User will receive an email about the update.
            </p>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary">
              <i class="fas fa-pencil-alt"></i> Update status
            </button>
          </div>
        </div>
      </form>
    </section>
  </div>

@endsection
