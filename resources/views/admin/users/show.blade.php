@extends('admin._layouts.admin')
@section('pageTitle','View Realtor')

@section('content')

  @component('admin.components.breadcrumb',[
        'links' => [
          'realtors' => route('admin.users.index')
        ]
      ])
    Detail
  @endcomponent

  @include('alert::bootstrap')

  <div class="row mb-3">
    <aside class="col-md-3">
      <div class="card">
        <div class="card-body text-center">
          @if($user->profile->avatar)
            <a href="{{$user->profile->avatar_url}}" target="_blank">
              <img class="img-thumbnail rounded" src="{{$user->profile->avatar_thumb_url}}" alt="avatar" width="300">
              <span class="sr-only">View large image</span>
            </a>
          @else
            <i class="fas fa-8x fa-user-circle text-muted"></i>
          @endif
          <div class="stars">
            @foreach(range(1,5) as $rating)
              @if($loop->iteration <= (int)$user->rating)
                <i class="fas fa-star text-warning"></i>
              @else
                <i class="fas fa-star text-muted"></i>
              @endif
            @endforeach
          </div>
          <p class="h5 my-3 text-truncate">{{optional($user->profile)->full_name}}</p>
          <p class="my-3 text-truncate">{{$user->email}}</p>
          <p>
            Broker: {{$user->broker->full_name}}
          </p>
          <p>
            Company Name: {{$user->broker->company_name}}
          </p>
          <p class="small text-muted mb-0">
            Documents Status: <span class="text-capitalize">{{$user->documents_status}}</span>
          </p>
        </div>
      </div>
    </aside>
    <section class="col-md-9 mt-sm-0 mt-lg-0 mt-4">
      <div class="card">
        <div class="card-body">
          <div class="card-detail border-bottom mb-3">
            <h5 class="card-title">Accomplishments</h5>
            @forelse($accomplishments as $item)
              <div class="profile-item">
                {{$item->total}} {{$item->service->name}}
                {{\Illuminate\Support\Str::plural('Job',$item->total)}} done successfully.
              </div>
            @empty
              NA
            @endforelse
          </div>

          <div class="card-detail border-bottom mb-3">
            <h5 class="card-title">Background</h5>
            <div class="profile-item">
              <strong>Education</strong>
              <p>{{$user->profile->education}}</p>
            </div>

            <div class="profile-item">
              <strong>Experience</strong>
              <p>{{$user->profile->experience}}</p>
            </div>
          </div>

          <div class="card-detail border-bottom mb-3">
            <h5 class="card-title">Services</h5>
            <div class="profile-item">
              <h4>
                @foreach($user->services as $item)
                  <span class="badge badge-pill badge-secondary font-weight-normal">{{$item->name}}</span>
                @endforeach
              </h4>
            </div>
          </div>

          <div class="card-detail border-bottom mb-3">
            <h5 class="card-title">Work Cities</h5>
            <div class="profile-item">
              <h4>
                @foreach($user->cities as $item)
                  <span class="badge badge-pill badge-secondary font-weight-normal">{{$item->name}}</span>
                @endforeach
              </h4>
            </div>
          </div>

          <div class="card-detail mb-3">
            <h5 class="card-title">Contact Information</h5>
            <table class="table table-bordered">
              <tbody>
              <tr>
                <td><strong>Phone</strong></td>
                <td>{{$user->address->full_phone_number}}</td>
              </tr>
              <tr>
                <td><strong>Email</strong></td>
                <td>{{$user->email}}</td>
              </tr>
              <tr>
                <td><strong>City</strong></td>
                <td>{{$user->address->city}}</td>
              </tr>
              <tr>
                <td><strong>State</strong></td>
                <td>{{$user->address->state}}</td>
              </tr>
              <tr>
                <td><strong>Address</strong></td>
                <td>{{$user->address->street_address}}</td>
              </tr>
              <tr>
                <td><strong>Zip code</strong></td>
                <td>{{$user->address->zip_code}}</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection
