@extends('admin._layouts.admin')
@section('pageTitle','Realtors')

@section('content')

  @component('admin.components.breadcrumb')
    Realtors
  @endcomponent

  @include('alert::bootstrap')

  <section class="card mb-3">
    <div class="card-body">
      <form id="search-form" class="form-inline" method="GET" action="{{route('admin.users.index')}}">
        <select class="form-control mb-2 mb-sm-0 mr-sm-2" name="per_page">
          <option disabled>Per page</option>
          @foreach([10,30,50] as $perPage)
            <option value="{{$perPage}}"
                    @if(request('per_page') == $perPage) selected @endif>{{$perPage}}</option>
          @endforeach
        </select>

        <select class="form-control mb-2 mb-sm-0 mr-sm-2" name="email_status">
          <option disabled>Email status</option>
          <option value="">Any</option>
          @foreach(['confirmed'=>'Confirmed', 'not_confirmed'=>'Not confirmed'] as $value => $label)
            <option value="{{$value}}"
                    @if(request('email_status') === $value) selected @endif>{{$label}}</option>
          @endforeach
        </select>

        <select class="form-control mb-2 mb-sm-0 mr-sm-2 text-capitalize" name="documents_status">
          <option disabled>Documents status</option>
          <option value="">Any</option>
          @foreach(['verified' ,'pending', 'rejected'] as $status)
            <option value="{{$status}}"
                    @if(request('documents_status') === $status) selected @endif>{{$status}}</option>
          @endforeach
        </select>

        <select class="form-control mb-2 mb-sm-0 mr-sm-2 text-capitalize" name="city_id">
          <option disabled>Cities</option>
          <option value="">All</option>
          @foreach($cities as $city)
            <option value="{{$city->id}}"
                    @if(request('city_id') == $city->id) selected @endif>{{$city->name}}</option>
          @endforeach
        </select>

        <input type="text" class="form-control mb-2 mb-sm-0 mr-sm-2" placeholder="Search" name="search"
               value="{{request('search')}}" autofocus>

        <button type="submit" class="btn btn-primary mb-0 mb-sm-0 mr-sm-2">
          <i class="fa fa-search fa-fw"></i>Search
        </button>
      </form>
    </div>
  </section>

  <section class="table-responsive mb-3">
    <table class="table table-striped  table-bordered table-hover">
      <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Broker</th>
        <th>Documents status</th>
        <th>Joined at</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      @forelse($users as $user)
        <tr>
          <td>
            <a href="{{route('admin.users.show',$user)}}" class="mb-0">
              {{$user->profile->full_name}}
            </a>
          </td>
          <td>
            {{$user->email}}
            @if($user->has_verified_email)
              <span class="text-success">
                <i class="fas fa-check-circle"></i>
              </span>
            @else
              <span class="text-warning">
                <i class="fas fa-exclamation-triangle"></i>
              </span>
            @endif
          </td>
          <td>
            {{optional($user->broker)->full_name}}
          </td>
          <td class="align-middle h5">
            @switch($user->documents_status)
              @case(\App\Models\UserDocument::STATUS_PENDING)
              <span class="badge badge-warning">Pending review</span>
              @break
              @case(\App\Models\UserDocument::STATUS_VERIFIED)
              <span class="badge badge-success">Verified</span>
              @break
              @case(\App\Models\UserDocument::STATUS_REJECTED)
              <span class="badge badge-danger">Rejected</span>
              @break
              @default
              <span class="badge badge-secondary">Not uploaded</span>
            @endswitch
          </td>
          <td>@date($user->created_at)</td>
          <td class="text-center">
            @if(!$user->documentsStatusIs(null))
              <a href="{{route('admin.users.edit',$user)}}" class="btn btn-sm btn-outline-info mb-0">
                <i class="fas fa-file-alt"></i> Documents</a>
            @endif
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="8" class="text-center">No records found</td>
        </tr>
      @endforelse
      </tbody>
    </table>
  </section>

  @if($users->total() > 0)
    <div class="row">
      <div class="col-md-4 mb-sm-0 mb-3 text-center text-sm-left">
        <h5 class="mt-sm-2 mt-0 mb-0">Found {{$users->total()}} entries</h5>
      </div>
      <div class="col-md-8 d-flex">
        <div class="mx-auto ml-sm-auto mr-sm-0 table-responsive-sm">
          {{$users->appends(request()->query())->links()}}
        </div>
      </div>
    </div>
  @endif
@endsection
