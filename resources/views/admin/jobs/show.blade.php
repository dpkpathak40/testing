@extends('admin._layouts.admin')
@section('pageTitle','View Job Details')

@section('content')

  @component('admin.components.breadcrumb',[
        'links' => [
          'jobs' => route('admin.jobs.index')
        ]
      ])
    Details
  @endcomponent

  @include('alert::bootstrap')

  <div class="row mb-3">
    <aside class="col-md-3">
      <div class="card mb-3">
        <div class="card-body text-center">
          @if($job->creator->profile->avatar)
            <a href="{{$job->creator->profile->avatar_thumb_url}}" target="_blank">
              <img class="img-thumbnail rounded" src="{{$job->creator->profile->avatar_thumb_url}}" alt="avatar"
                   width="300">
              <span class="sr-only">View large image</span>
            </a>
          @else
            <i class="fas fa-8x fa-user-circle text-muted"></i>
          @endif
          <p class="h5 my-3 text-truncate"><strong>Creator:</strong> {{$job->creator->profile->full_name}}</p>
          <p class="my-3 text-truncate">{{$job->creator->email}}</p>
        </div>
      </div>

      <div class="card mb-3">
        @if($job->assignee)
          <div class="card-body text-center">
            @if($job->assignee->profile->avatar)
              <a href="{{$job->assignee->profile->avatar_thumb_url}}" target="_blank">
                <img class="img-thumbnail rounded" src="{{$job->assignee->profile->avatar_thumb_url}}" alt="avatar"
                     width="300">
                <span class="sr-only">View large image</span>
              </a>
            @else
              <i class="fas fa-8x fa-user-circle text-muted"></i>
            @endif
            <p class="h5 my-3 text-truncate"><strong>Assignee:</strong> {{$job->assignee->profile->full_name}}</p>
            <p class="my-3 text-truncate">{{$job->assignee->email}}</p>
          </div>
        @else
          <div class="card-body no-record p-5">
            <h3 class="text-muted">No realtor assigned yet.</h3>
          </div>
        @endif
      </div>
    </aside>

    <section class="col-md-9 mt-sm-0 mt-lg-0 mt-4">
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="card-title">Job Details</h5>
          <table class="table table-bordered">
            <tbody>
            <tr>
              <td><strong>Service Name</strong></td>
              <td>{{$job->service->name}}</td>
            </tr>
            <tr>
              <td><strong>City</strong></td>
              <td>
                @foreach($job->cities as $item)
                  <span class="badge badge-pill badge-secondary font-weight-normal">{{$item->name}}</span>
                @endforeach
              </td>
            </tr>
            <tr>
              <td><strong>Job Description</strong></td>
              <td>@if($job->description){{$job->description}}@else - @endif</td>
            </tr>
            <tr>
              <td><strong>Chargeable Amount</strong></td>
              <td>${{$job->chargeable_amount + $job->chargeable_incentive_amount}}</td>
            </tr>
            <tr>
              <td><strong>Transferable Amount</strong></td>
              <td>${{$job->transferable_amount + $job->transferable_incentive_amount}}</td>
            </tr>
            <tr>
              <td><strong>Incentive Amount</strong></td>
              <td>@if($job->transferable_incentive_amount)${{$job->transferable_incentive_amount}}@else $0 @endif</td>
            </tr>
            <tr>
              <td><strong>Posted at</strong></td>
              <td>@date($job->created_at)</td>
            </tr>
            <tr>
              <td><strong>Due Date</strong></td>
              <td>@date($job->due_date)</td>
            </tr>
            <tr>
              <td><strong>Status</strong></td>
              <td>
                @switch($job->status)
                  @case(\App\Models\Job::STATUS_COMPLETED)
                  <span class="badge badge-success">Completed</span>
                  @break
                  @case(\App\Models\Job::STATUS_DONE)
                  <span class="badge badge-warning">Done</span>
                  @break
                  @case(\App\Models\Job::STATUS_PENDING)
                  <span class="badge badge-secondary">Pending</span>
                  @break
                  @case(\App\Models\Job::STATUS_CANCELLED)
                  <span class="badge badge-danger">Cancelled</span>
                  @break
                @endswitch
              </td>
            </tr>
            @if($job->cancelled_at)
              <tr>
                <td><b>Cancelled at</b></td>
                <td>@date($job->cancelled_at)</td>
              </tr>
              <tr>
                <td><b>Cancellation reason</b></td>
                <td>{{$job->cancellation_reason}}</td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>

      @if(!$job->assignee_user_id)
        <div class="card mb-3">
          <div class="card-body">
            <h5 class="card-title">Offer Accepted by - {{$job->applicants->count()}}</h5>
            @forelse($job->applicants as $applicant)
              <div class="d-flex mb-3">
                <div>
                  <img src="{{$applicant->profile->avatar_thumb_url}}" class="rounded-circle" alt="avatar" width="100"
                       height="100">
                </div>
                <div class="w-100 pl-3 my-auto">
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <h5>{{ $applicant->profile->full_name }}</h5>
                      <div class="stars mb-2">
                        @foreach(range(1,5) as $rating)
                          @if($loop->iteration <= (int)$applicant->rating)
                            <i class="fas fa-star text-warning"></i>
                          @else
                            <i class="fas fa-star text-muted"></i>
                          @endif
                        @endforeach
                      </div>
                      <h6><a href="{{route('admin.users.show',$applicant)}}" target="_blank">View Profile</a></h6>
                    </div>
                  </div>
                </div>
              </div>
            @empty
              <div class="text-center text-muted">No applicant yet.</div>
            @endforelse
          </div>
        </div>
      @endif

      @can('cancel', $job)
        @include('admin.jobs._cancelForm')
      @endcan
    </section>
  </div>

@endsection
