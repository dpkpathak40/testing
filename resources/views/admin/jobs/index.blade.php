@extends('admin._layouts.admin')
@section('pageTitle','Jobs')

@section('content')

  @component('admin.components.breadcrumb')
    Jobs
  @endcomponent

  @include('alert::bootstrap')

  <section class="card mb-3">
    <div class="card-body">
      <form id="search-form" class="form-inline" method="GET" action="{{route('admin.jobs.index')}}">
        <select class="form-control mb-2 mb-sm-0 mr-sm-2" name="per_page">
          <option disabled>Per page</option>
          @foreach([10,30,50] as $perPage)
            <option value="{{$perPage}}"
                    @if(request('per_page') == $perPage) selected @endif>{{$perPage}}</option>
          @endforeach
        </select>

        <select class="form-control mb-2 mb-sm-0 mr-sm-2 text-capitalize" name="job_status">
          <option disabled>Job status</option>
          <option value="">Any</option>
          @foreach(['completed' ,'pending', 'done', 'cancelled'] as $status)
            <option value="{{$status}}"
                    @if(request('job_status') === $status) selected @endif>{{$status}}</option>
          @endforeach
        </select>

        <select class="form-control mb-2 mb-sm-0 mr-sm-2 text-capitalize" name="city_id">
          <option disabled>Cities</option>
          <option value="">All</option>
          @foreach($cities as $city)
            <option value="{{$city->id}}"
                    @if(request('city_id') == $city->id) selected @endif>{{$city->name}}</option>
          @endforeach
        </select>

        <input type="text" class="form-control mb-2 mb-sm-0 mr-sm-2" placeholder="Search" name="search"
               value="{{request('search')}}" autofocus>

        <button type="submit" class="btn btn-primary mb-0 mb-sm-0 mr-sm-2">
          <i class="fa fa-search fa-fw"></i>Search
        </button>
      </form>
    </div>
  </section>

  <section class="table-responsive mb-3">
    <table class="table table-striped  table-bordered table-hover">
      <thead>
      <tr>
        <th>Service Name</th>
        <th>Creator Name</th>
        <th>Assignee Name</th>
        <th>Chargeable Amount</th>
        <th>Transferable Amount</th>
        <th>Due Date</th>
        <th>Posted at</th>
        <th>Status</th>
      </tr>
      </thead>
      <tbody>
      @forelse($jobs as $job)
        <tr>
          <td>
            <a href="{{route('admin.jobs.show',$job)}}">
              {{$job->service->name}}
            </a>
          </td>
          <td data-id="{{$job->creator->id}}">
            <a href="{{route('admin.users.show',$job->creator)}}">
              {{$job->creator->profile->full_name}}
            </a>
          </td>
          <td>
            @if($job->assignee)
              <a href="{{route('admin.users.show',$job->assignee)}}">
                {{$job->assignee->profile->full_name}}
              </a>
            @endif
          </td>
          <td>${{$job->chargeable_amount + $job->chargeable_incentive_amount}}</td>
          <td>${{$job->transferable_amount + $job->transferable_incentive_amount}}</td>
          <td>@date($job->due_date)</td>
          <td>@date($job->created_at)</td>
          <td class="align-middle h5">
            @switch($job->status)
              @case(\App\Models\Job::STATUS_COMPLETED)
              <span class="badge badge-success">Completed</span>
              @break
              @case(\App\Models\Job::STATUS_DONE)
              <span class="badge badge-warning">Done</span>
              @break
              @case(\App\Models\Job::STATUS_PENDING)
              <span class="badge badge-secondary">Pending</span>
              @break
              @case(\App\Models\Job::STATUS_CANCELLED)
              <span class="badge badge-danger">Cancelled</span>
              @break
            @endswitch
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="8" class="text-center">No records found</td>
        </tr>
      @endforelse
      </tbody>
    </table>
  </section>

  @if($jobs->total() > 0)
    <div class="row">
      <div class="col-md-4 mb-sm-0 mb-3 text-center text-sm-left">
        <h5 class="mt-sm-2 mt-0 mb-0">Found {{$jobs->total()}} entries</h5>
      </div>
      <div class="col-md-8 d-flex">
        <div class="mx-auto ml-sm-auto mr-sm-0 table-responsive-sm">
          {{$jobs->appends(request()->query())->links()}}
        </div>
      </div>
    </div>
  @endif
@endsection
