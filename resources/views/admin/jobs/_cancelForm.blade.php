<form method="post" action="{{route('admin.jobs.cancel', $job)}}" onsubmit="return confirm('Are you sure to cancel?')">
  @csrf
  @method('put')
  <div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title">Cancel job</h5>
      <div class="form-group required mb-0">
        <label>Cancellation reason</label>
        <textarea class="form-control @isInvalid('cancellation_reason')"
                  name="cancellation_reason"
                  rows="2">{{ old('cancellation_reason', $job->cancellation_reason) }}</textarea>
        @validationMessage('cancellation_reason')
      </div>
    </div>
    <div class="card-footer text-right">
      <div class="d-inline-block text-muted mr-3">
        Realtor will receive a refund.
      </div>
      <button class="btn btn-danger" type="submit">
        <i class="fas fa-fw fa-times-circle"></i> Cancel the job
      </button>
    </div>
  </div>
</form>

