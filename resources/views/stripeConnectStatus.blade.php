@extends('_layouts.app')
@section('pageTitle','Stripe Connect')

@section('content')
  @include('_partials.logo')

  <div class="form-wrap">
    @include('alert::bootstrap')

    <div class="text-center">
      @if (session('status'))
        <i class="fas fa-8x fa-check-circle text-success my-5"></i>
        <h3 class="h1">Stripe connect was successful.</h3>
      @elseif(session('error'))
        <i class="fas fa-8x fa-times-circle text-danger my-5"></i>
        <h3 class="h1">{{session('error')}}</h3>
        <p>Try again or contact administrator if problem persist.</p>
      @endif
    </div>
  </div>

@endsection
