@extends('_layouts.app')
@section('pageTitle','View Job')

@section('content')
  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Job detail
      @endcomponent

      @include('alert::bootstrap')

      <job-detail :job='@json($job)'
                  :incentive-percentage="{{config('project.incentive_percentage')}}"
                  :can-mark-job-as-completed="{{Auth::user()->can('markAsCompleted',$job) ? 'true' : 'false'}}"
                  :can-delete-job="{{Auth::user()->can('delete',$job) ? 'true' : 'false'}}"
      ></job-detail>
      @if(is_null($job->assignee_user_id))
        <job-applicants :applicants='@json($applicants)' :job-id="{{$job->id}}"></job-applicants>
      @endif
    </div>
  </section>
  @can('markAsCompleted',$job)
    <job-complete-modal></job-complete-modal>
  @endcan
  @can('delete',$job)
    <job-delete-modal></job-delete-modal>
  @endcan
@endsection
