@extends('_layouts.app')
@section('pageTitle','Assign Job')

@section('content')
  <div class="container mid-section">
    <h3 class="text-center">Assign {{$job->display_name}} Job</h3>
    <h1 class="text-center">Make Payment</h1>
    <div class="col-sm-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
      @include('alert::bootstrap')

      <div class="card-white card-form mt-3 pb-0">
        <job-assign :job='@json($job)' :assignee='@json($user)'></job-assign>
      </div>
    </div>
  </div>
@endsection
@push('before_app_scripts')
  <script src="https://js.stripe.com/v3/"></script>
@endpush
