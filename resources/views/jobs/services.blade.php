@extends('_layouts.app')
@section('pageTitle','Choose service')

@section('content')
  <div class="container mid-section">
    <h3 class="text-center">Create New Job Offer</h3>
    <h1 class="text-center">Choose a Service Type</h1>
    <div class="flex-wrap">

      @include('alert::bootstrap')

      @foreach($services as $service)
        <div class="job-card text-center">
          <div class="img-round">
            <img src="{{$service->image_url}}">
          </div>
          <h3>{{$service->name}}</h3>
          <div class="price">{{money($service->amount)}}</div>
          <a href="{{route('jobs.create',$service)}}" class="btn btn-green text-light">Select</a>
        </div>
      @endforeach

    </div>
  </div>
@endsection
