@extends('_layouts.app')
@section('pageTitle','Create job')

@section('content')
  <div class="container mid-section">
    <h3 class="text-center">Create New Job Offer</h3>
    <h1 class="text-center">Add Job details</h1>
    <div class="col-sm-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
      @include('alert::bootstrap')

      <div class="card-white card-form mt-3 pb-0">
        <job-create :service='@json($service)'
                    :cities='@json($cities)'
                    :max-number-of-homes='{{config('project.job.max_number_of_homes')}}'
                    :incentive-percentage='{{config('project.incentive_percentage')}}'
        ></job-create>
      </div>
    </div>
  </div>
@endsection
