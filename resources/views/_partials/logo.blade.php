<div class="logo-outer text-center">
  <img src="{{ url('assets/logo-large.png') }}" alt="{{config('app.name')}}">
</div>
