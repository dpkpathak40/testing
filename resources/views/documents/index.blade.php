@extends('_layouts.app')
@section('pageTitle','Documents verification')

@section('content')
  <div class="container">
    <h1 class="text-center font-weight-light my-5">Documents Verification</h1>
    <div class="col-sm-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
      @include('alert::bootstrap')

      <div class="card-white card-form">

        @switch(auth()->user()->documents_status)
          @case(\App\Models\UserDocument::STATUS_PENDING)
          <div class="text-center">
            <i class="fas fa-5x fa-exclamation-triangle text-warning my-3"></i>
            <h5 class="text-muted text-center">Your documents verification is pending for review.<br>
              You will get an email when upon verification.</h5>
          </div>
          @break
          @case(\App\Models\UserDocument::STATUS_VERIFIED)
          <div class="text-center">
            <i class="fas fa-5x fa-check-circle text-success my-3"></i>
            <h5 class="text-muted text-center">Your documents has been verified.<br>
              You can continue using the application.</h5>
          </div>
          @break
          @case(\App\Models\UserDocument::STATUS_REJECTED)
          <div class="text-center">
            <i class="fas fa-5x fa-times-circle text-danger my-3"></i>
            <h5 class="text-muted text-center">Your documents has been rejected.<br>
              You need to upload documents again.</h5>
          </div>
          @break
          @default
          <div class="text-center">
            <i class="fas fa-5x fa-info-circle text-muted my-3"></i>
            <h5 class="text-muted text-center">Your documents verification is
              pending.<br>
              Please fill information below to verify your account.</h5>
          </div>
        @endswitch

        @can('create',\App\Models\UserDocument::class)
          @include('documents._form')
        @endif
      </div>
    </div>
  </div>
@endsection
