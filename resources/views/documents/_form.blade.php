<form action="{{route('documents.store')}}" method="POST" class="wrap-form"
      enctype="multipart/form-data" novalidate>
  @csrf
  <div class="form-group">
    <label>Select your broker</label>
    <select class="form-control @isInvalid('broker')" name="broker">
      <option value="">Select...</option>
      @foreach($brokers as $broker)
        <option
          value="{{$broker->id}}" {{old("broker",auth()->user()->broker->id) == $broker->id ? 'selected' : ''}}>
          {{$broker->full_name}} - {{$broker->email}}</option>
      @endforeach
    </select>
    @validationMessage('broker')
  </div>

  @foreach($documentTypes as $slug => $label)
    <div class="form-group">
      <label>{{$label}}</label>
      <div class="custom-file">
        <input type="file" class="custom-file-input form-control @isInvalid($slug)"
               id="input-file-{{$loop->iteration}}" name="{{$slug}}" required>
        <label class="custom-file-label" for="input-file-{{$loop->iteration}}">Choose file</label>
        @validationMessage($slug)
      </div>
    </div>
  @endforeach

  <div class="text-center">
    <button type="submit" class="btn btn-green">Submit</button>
  </div>
</form>
