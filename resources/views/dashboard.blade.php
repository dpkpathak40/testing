@extends('_layouts.app')
@section('pageTitle','Dashboard')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Dashboard
      @endcomponent

      <section class="row">
        <div class="col-sm-6">
          <div class="sent-jobs">
            <h3>
              Sent Jobs
              <span class="dots-large"></span>
              <span class="dots-small"></span>
              <span class="dots-ex-small"></span>
            </h3>
            <div class="row text-center">
              <div class="col">
                <div class="job-value">{{$counts->sent_jobs}}</div>
                <a href="{{route('sentJobs.sent')}}" class="btn btn-bordered">Sent</a>
              </div>
              <div class="col">
                <div class="job-value">{{$counts->assigned_jobs}}</div>
                <a href="{{route('sentJobs.assigned')}}" class="btn btn-bordered">Assigned</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="received-jobs">
            <h3>
              Received Jobs
              <span class="dots-large"></span>
              <span class="dots-small"></span>
              <span class="dots-ex-small"></span>
            </h3>
            <div class="row text-center">
              <div class="col">
                <div class="job-value">{{$counts->received_jobs}}</div>
                <a href="{{route('receivedJobs.received')}}" class="btn btn-bordered">Received</a>
              </div>
              <div class="col">
                <div class="job-value">{{$counts->accepted_jobs}}</div>
                <a href="{{route('receivedJobs.accepted')}}" class="btn btn-bordered">Accepted</a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <received-jobs-card :jobs='@json($receivedJobs)'></received-jobs-card>
      <sent-jobs-card :jobs='@json($sentJobs)'></sent-jobs-card>
      <upcoming-jobs-card :jobs='@json($upcomingJobs)'></upcoming-jobs-card>
    </div>
  </section>
  <job-accept-modal></job-accept-modal>
  <job-ignore-modal></job-ignore-modal>
  <job-done-modal></job-done-modal>
  <job-delete-modal></job-delete-modal>
@endsection

