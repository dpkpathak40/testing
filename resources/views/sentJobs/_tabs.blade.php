<div class="tabs-sent-jobs sent-jobs">
  <a href="{{route('sentJobs.sent')}}" class="{{active_route('sentJobs.sent')}}">
    {{$counts->sent_jobs}} <span>Sent</span>
  </a>
  <a href="{{route('sentJobs.assigned')}}" class="{{active_route('sentJobs.assigned')}}">
    {{$counts->assigned_jobs}} <span>Assigned</span>
  </a>
  <a href="{{route('sentJobs.forReview')}}" class="{{active_route('sentJobs.forReview')}}">
    {{$counts->to_review_jobs}} <span>For Review</span>
  </a>
  <a href="{{route('sentJobs.completed')}}" class="{{active_route('sentJobs.completed')}}">
    {{$counts->completed_jobs}} <span>Completed</span>
  </a>
</div>
