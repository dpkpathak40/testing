@extends('_layouts.app')
@section('pageTitle','For Review')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Sent Jobs
      @endcomponent

      @include('sentJobs._tabs')
      <sent-jobs-for-review-card :jobs='@json($forReviewJobs->items())'></sent-jobs-for-review-card>
      <div class="pagination-center">
        {{ $forReviewJobs->links() }}
      </div>
    </div>
  </section>
@endsection
