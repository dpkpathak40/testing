@extends('_layouts.app')
@section('pageTitle','Completed')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Sent Jobs
      @endcomponent

      @include('sentJobs._tabs')
      <sent-jobs-completed-card :jobs='@json($completedJobs->items())'></sent-jobs-completed-card>
      <div class="pagination-center">
        {{ $completedJobs->links() }}
      </div>
    </div>
  </section>
@endsection
