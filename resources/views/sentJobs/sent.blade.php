@extends('_layouts.app')
@section('pageTitle','Sent')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Sent Jobs
      @endcomponent

      @include('sentJobs._tabs')
      <sent-jobs-sent-card :jobs='@json($sentJobs->items())'></sent-jobs-sent-card>
      <div class="pagination-center">
        {{ $sentJobs->links() }}
      </div>
    </div>
  </section>
@endsection
