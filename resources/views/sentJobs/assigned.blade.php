@extends('_layouts.app')
@section('pageTitle','Assigned')

@section('content')

  <section class="dashboard-in">
    <div class="container">
      @component('components.breadcrumb')
        Sent Jobs
      @endcomponent

      @include('sentJobs._tabs')
      <sent-jobs-assigned-card :jobs='@json($assignedJobs->items())'></sent-jobs-assigned-card>
      <div class="pagination-center">
        {{ $assignedJobs->links() }}
      </div>
    </div>
  </section>
@endsection
