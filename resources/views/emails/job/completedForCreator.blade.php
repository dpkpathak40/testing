@component('mail::message')
  Hi {{$user->profile->first_name}},

  Thank you for using OktoHub.
  We hope we have made your life just a little bit easier. Hope to see you again soon.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
