@component('mail::message')
  Hi {{$user->profile->first_name}},

  Congrats on a {{$job->display_name}} job well done!
  OktoHub appreciates your hard work.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
