@component('mail::message')
  Hi {{$user->profile->first_name}},

  We're all wrapped up.
  Please confirm that the {{$job->display_name}} task was completed to your satisfaction and rate the agent accordingly.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
