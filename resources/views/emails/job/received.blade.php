@component('mail::message')
  Hi {{$user->profile->first_name}},

  Ding, Ding, we have a {{$job->display_name}} job for you!
  Please click on the link to review the details.

  <p>
    @if($job->transferable_incentive_amount)
      There is an extra incentive for this job available too.
    @endif
  </p>

  @component('mail::button', ['url' => $actionUrl])
    View
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
