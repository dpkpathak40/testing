@component('mail::message')
  Hi {{$user->profile->first_name}},

  Good call, you've requested a {{$job->display_name}} task through OktoHub.
  We will take it from here.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
