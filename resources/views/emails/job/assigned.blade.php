@component('mail::message')
  Hi {{$user->profile->first_name}},

  You ready to roll? You've been selected to complete the {{$job->display_name}} task.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
