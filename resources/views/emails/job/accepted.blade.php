@component('mail::message')
  Hi {{$user->profile->first_name}},

  You're off the hook. Your {{$job->display_name}} job has been accepted.

  @component('mail::button', ['url' => $actionUrl])
    View
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
