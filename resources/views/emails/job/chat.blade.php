@component('mail::message')
  Hi {{$user->profile->first_name}},

  You have a new chat notification from {{$sender->profile->full_name}} on {{$job->display_name}} which has a due
  date on {{$job->formatted_due_date}}.

  @component('mail::button', ['url' => $actionUrl])
    View
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
