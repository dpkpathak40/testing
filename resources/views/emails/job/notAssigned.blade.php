@component('mail::message')
  Hi {{$user->profile->first_name}},

  Thank you for accepting the {{$job->display_name}} task.
  The job was assigned to another agent, but don’t fret, there will be others coming your way.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
