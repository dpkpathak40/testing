@component('mail::message')
  Hi {{$user->profile->first_name}},

  Incentive amount has been updated for the {{$job->display_name}} job.
  Please click on the link to review the details.

  @component('mail::button', ['url' => $actionUrl])
    View
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
