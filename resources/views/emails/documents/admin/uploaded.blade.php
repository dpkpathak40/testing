@component('mail::message')
  Hi, Admin
  <br>
  <p>
    The realtor "{{$realtor->profile->full_name}}" has uploaded documents for verification.
  </p>

  @component('mail::button', ['url' => $actionUrl])
    Review
  @endcomponent

  <p>
    Above link will directly take you to review screen.
  </p>

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
