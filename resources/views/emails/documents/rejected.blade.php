@component('mail::message')
  Hi {{$user->profile->first_name}},
  <br>
  <p>
    Unfortunately, your document(s) could not be verified. Turn that frown upside down because you can just go to your
    account and resubmit the document. Please see message below for clarification.
  </p>

  @component('mail::panel')
    Reason: {{$reason}}
  @endcomponent

  <p>
    You need to upload your documents again by logging in to application.
  </p>

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
