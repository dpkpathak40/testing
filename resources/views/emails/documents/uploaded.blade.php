@component('mail::message')
  Hi {{$user->profile->first_name}},
  <br>
  <p>
    So far, you're good at following directions. We're impressed. Your documents have been successfully uploaded.
  </p><br>

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
