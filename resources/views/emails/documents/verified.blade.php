@component('mail::message')
  Hi {{$user->profile->first_name}},
  <br>
  <p>
    Whoopee! Your documents have been verified. Please complete your account including that sweet bio pic. Glamour shots
    are encouraged.
  </p>

  @component('mail::button', ['url' => route('account.profile.edit'), 'color' => 'green'])
    Complete your Profile
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
