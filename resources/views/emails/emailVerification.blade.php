@component('mail::message')
  Hi {{$user->profile->first_name}},

  You need to verify your email address before you start using this application.

  @component('mail::button', ['url' => $actionUrl])
    Verify Email
  @endcomponent

  This link will expire in {{$linkExpireInHours}} {{\Illuminate\Support\Str::plural('hour', $linkExpireInHours)}}.

  Regards,<br>
  {{ config('app.name') }}

  @component('mail::subcopy')
    @lang(
        "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
        'into your web browser: [:actionURL](:actionURL)',
        [
            'actionText' => 'Verify Email',
            'actionURL' => $actionUrl
        ]
    )
  @endcomponent
@endcomponent
