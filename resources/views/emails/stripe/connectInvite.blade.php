@component('mail::message')
  Hi, {{$broker->first_name}}

  You have been invited to connect with Stripe.

  @component('mail::button', ['url' => $actionUrl])
    Connect with Stripe
  @endcomponent

  Regards,<br>
  {{ config('app.name') }}

  @component('mail::subcopy')
    @lang(
        "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
        'into your web browser: [:actionURL](:actionURL)',
        [
            'actionText' => 'Connect with Stripe',
            'actionURL' => $actionUrl
        ]
    )
  @endcomponent
@endcomponent
