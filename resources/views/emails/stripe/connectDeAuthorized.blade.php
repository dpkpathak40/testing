@component('mail::message')
  Hi, Admin

  The broker "{{$broker->full_name}}" with email "{{$broker->email}}" has been disconnected from Stripe.

  @component('mail::button', ['url' => $actionUrl])
    Invite Again
  @endcomponent

  <p>
    @if($realtorsCount)
      {{$realtorsCount}} {{\Illuminate\Support\Str::plural('realtor', $realtorsCount)}} those are connected to this
      Broker will not be able to complete their Jobs.
    @else
      There are no realtor connected with this Broker.
    @endif
  </p>

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
