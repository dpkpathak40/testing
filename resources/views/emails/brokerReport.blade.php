@component('mail::message')
  Hi {{$broker->first_name}},

  Attached is the pdf with details of all the transaction for the jobs.

  Regards,<br>
  {{ config('app.name') }}
@endcomponent
