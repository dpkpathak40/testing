@php
  $appConfig = [
    'csrfToken' => csrf_token(),
    'env' => config('app.env'),
    'stripePublicKey' => config('services.stripe.key'),
    'user'=> auth()->check() ? [
          'id' => auth()->id(),
          'email'=> auth()->user()->email
        ] : [],
  ];
@endphp
<script>
  window.appConfig = @json($appConfig)
</script>
@stack('after_app_config_script')
