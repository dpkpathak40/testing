<nav class="navbar navbar-expand-md main-nav">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <div class="navbar-nav ml-auto">
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
      </ul>
    </div>
  </div>
  <a class="btn-green green-small" href="{{ route('register') }}">Sign Up</a>
</nav>
