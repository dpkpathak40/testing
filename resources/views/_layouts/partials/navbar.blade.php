<nav class="navbar navbar-expand-md main-nav fixed-top">
  <a href="{{url('/')}}" class="navbar-brand">
    <img src="{{asset('assets/logo-large.png')}}" alt="{{config('app.name')}}">
  </a>

  <form class="form-inline" action="/">
    <span class="icon-search"><i class="fas fa-search"></i></span>
    <input class="form-control" type="text" placeholder="Search for Realtors by City, Services and their Name...">
  </form>

  <a class="btn btn-purple green-small ml-auto" href="{{route('jobs.services')}}">
    Add a Job Offer
  </a>

  <button class="icon-search btn-search">
    <i class="fas fa-search"></i>
    <span class="sr-only">Search</span>
  </button>

  @include('_layouts.partials.notifications')

  <div class="dropdown profile-menu">
    <button class="btn-profile" data-toggle="dropdown">
      <img src="{{auth('web')->user()->profile->avatar_thumb_url}}" alt="avatar">
      <i class="fas fa-user-alt"></i>
    </button>

    <ul class="dropdown-menu">
      <li>
        <button class="btn-close-menu"><i class="fas fa-times"></i></button>
        <a href="{{url('/')}}" class="navbar-brand">
          <img src="{{asset('assets/logo-large.png')}}" alt="{{config('app.name')}}">
        </a>
      </li>
      <li class="profile-icon-wrap">
        <span class="profile-icon-big">
          <img src="{{auth('web')->user()->profile->avatar_thumb_url}}" alt="avatar">
        </span>
        <span>{{auth('web')->user()->profile->full_name ?? 'Realtor'}}
              <div>{{auth('web')->user()->email}}</div>
              <div><a href="{{route('account.profile.edit')}}" class="btn btn-green">Edit Profile</a></div>
            </span>
      </li>
      <li><a href="{{route('dashboard')}}">Dashboard</a></li>
      <li><a href="{{route('sentJobs.sent')}}">Sent Jobs</a></li>
      <li><a href="{{route('receivedJobs.received')}}">Received Jobs</a></li>
      <li><a href="{{route('account.password.edit')}}">Change Password</a></li>
      <li class="dropdown-divider"></li>
      <li><a href="#" onclick="event.preventDefault();document.querySelector('#logout-form').submit();">Log Out</a></li>
    </ul>
  </div>
  <form hidden id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
</nav>
