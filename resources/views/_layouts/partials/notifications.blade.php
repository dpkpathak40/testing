<div class="dropdown wrap-notification">
  <a role="button" data-toggle="dropdown" href="javascript:void(0)">
    <notifications-indicator :count="{{auth('web')->user()->unreadNotifications()->count()}}"></notifications-indicator>
    <i class="fas fa-bell"></i>
  </a>
  <notifications-drop-down></notifications-drop-down>
</div>
