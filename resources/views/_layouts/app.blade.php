<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="strict-origin-when-cross-origin" name="referrer">

  <title> @yield('pageTitle', 'Home') | {{config('app.name')}}</title>
  <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

  @include('_layouts.partials.styles')
  @include('_layouts.partials.appConfig')

</head>
<body class="{{'env-'.config('app.env', 'production')}} {{auth('web')->guest() ? 'guest-user':''}} @yield('bodyClass')">
<main id="app" class="app">
  @auth('web')
    @include('_layouts.partials.navbar')
  @else
    @include('_partials.wave')
    @include('_layouts.partials.navbarGuest')
  @endauth

  <div class="{{auth('web')->guest() ? 'container':''}}">
    @yield('content')
  </div>
</main>
@routes
@include('_layouts.partials.scripts')
</body>
</html>
