if (process.env.MIX_SENTRY_JS_ENABLED === 'true') {
  require('@/js/sentry');
}

import jQuery from 'jquery/dist/jquery.slim';
import Popper from 'popper.js';
// Selective bootstrap.js build
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/alert';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/modal';

import Vue from 'vue';
import axios from 'axios';
import Form, {FieldErrorComponent, IsInvalidDirective} from 'laravel-form-validation';
import Ziggy from 'ziggy-js';
import VueTimeago from 'vue-timeago'
import VueToast from 'vue-toast-notification';
import InfiniteLoading from 'vue-infinite-loading';

window.jQuery = jQuery;

window.Popper = Popper;

// Event bus for components can talk with each other
const EventBus = new Vue();
Vue.prototype.$bus = EventBus;

axios.defaults.withCredentials = true;
axios.defaults.headers.common['X-CSRF-TOKEN'] = window.appConfig.csrfToken;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios = axios;
require('./axios.js');
// Lets not use axios directly
Vue.prototype.$http = axios;

Form.$defaults.axios = axios;

// route() helper is available everywhere
window.route = Ziggy;
// Exposing route() helper method to vue templates
Vue.mixin({
  methods: {
    route: Ziggy
  }
});

Vue.use(VueTimeago, {
  locale: 'en'
});

Vue.use(VueToast);

Vue.use(InfiniteLoading, {
  props: {
    spinner: 'circles',
  },
  slots: {
    noMore: 'No more chats to show',
    noResults: 'No more chats to show',
  },
});

Vue.directive('error', IsInvalidDirective);

Vue.component('FormError', FieldErrorComponent);

