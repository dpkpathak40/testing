import Vue from 'vue';

// Intercept all responses
window.axios.interceptors.response.use(
  (response) => response,

  (error) => {
    console.error(error);

    if (error.response) {

      switch (error.response.status) {
        case 400:
          Vue.$toast.error(error.response.data?.message || 'Bad request.');
          break;
        case 401:
          Vue.$toast.error(error.response.data?.message || 'Unauthorized access.', {
            duration: 10000
          });
          break;
        case 403:
        case 423:
          Vue.$toast.error(error.response.data?.message || 'Forbidden access.', {
            duration: 10000
          });
          break;
        case 404:
          Vue.$toast.error('Requested resource not found.');
          break;
        case 419:
          Vue.$toast.error('Session expired. Please login again.');
          break;
        case 422:
          // individual component should handle it
          break;
        case 429:
          Vue.$toast.warning('Too many requests. Try again later.');
          break;
        case 500:
          Vue.$toast.error('Internal server error.');
          break;
        case 503:
          Vue.$toast.error('App is down for maintenance.');
          break;
        default:
          Vue.$toast.error('Unknown error occurred.');
      }
    }

    return Promise.reject(error);
  }
);
