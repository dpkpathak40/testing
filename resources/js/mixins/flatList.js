export default {
  methods: {
    flatList(items, property = 'name') {
      return items.map(item => item[property]).join(', ')
    }
  }
}
