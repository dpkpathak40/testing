import Errors from 'laravel-form-validation/dist/esm/Errors.js';

export default {
  data() {
    return {
      errors: new Errors(),
    }
  },
  methods: {
    recordErrors(error) {
      if (error && error.response && error.response.data.errors) {
        this.errors.record(error.response.data.errors);
      }
    }
  }
}
