jQuery(function ($) {

  $('form').find('.custom-file-input').on('change', function (event) {
    let fileName = $(this).val().replace(/\\/g, '/').replace(/.*\//, '');
    $(this).parent('.custom-file').find('.custom-file-label').text(fileName);
  });

});
