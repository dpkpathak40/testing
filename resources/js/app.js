import '../sass/vendor.scss';
import '../sass/app.scss';

import Vue from 'vue';
import './bootstrap';
import './fileInput'

import EditProfile from './components/account/EditProfile.vue';

import ReceivedJobsCard from './components/dashboard/Received.vue';
import SentJobsCard from './components/dashboard/Sent.vue';
import UpcomingJobsCard from './components/dashboard/Upcoming.vue';

import JobCreate from './components/jobs/create';
import JobDetail from './components/jobs/show/detail';
import JobApplicants from './components/jobs/show/applicants';
import JobAcceptModal from './components/jobs/acceptModal';
import JobIgnoreModal from './components/jobs/ignoreModal';
import JobAssign from './components/jobs/assign';
import JobDoneModal from './components/jobs/doneModal';
import JobCompleteModal from './components/jobs/completeModal';
import JobDeleteModal from './components/jobs/deleteModal';
import JobChatModal from './components/jobs/chatModal';

import SentJobsSentCard from './components/sentJobs/Sent.vue';
import SentJobsAssignedCard from './components/sentJobs/Assigned.vue';
import SentJobsForReviewCard from './components/sentJobs/ForReview.vue';
import SentJobsCompletedCard from './components/sentJobs/Completed.vue';

import ReceivedJobsReceivedCard from './components/receivedJobs/Received.vue';
import ReceivedJobsAcceptedCard from './components/receivedJobs/Accepted.vue';
import ReceivedJobsToDoCard from './components/receivedJobs/ToDo.vue';
import ReceivedJobsCompletedCard from './components/receivedJobs/Completed.vue';

import NotificationsDropDown from './components/notification/dropDown.vue';
import NotificationsIndicator from './components/notification/indicator.vue';

const app = new Vue({
  components: {
    EditProfile,

    ReceivedJobsCard,
    SentJobsCard,
    UpcomingJobsCard,

    JobCreate,
    JobDetail,
    JobApplicants,
    JobAcceptModal,
    JobIgnoreModal,
    JobAssign,
    JobDoneModal,
    JobCompleteModal,
    JobDeleteModal,
    JobChatModal,

    SentJobsSentCard,
    SentJobsAssignedCard,
    SentJobsForReviewCard,
    SentJobsCompletedCard,

    ReceivedJobsReceivedCard,
    ReceivedJobsAcceptedCard,
    ReceivedJobsToDoCard,
    ReceivedJobsCompletedCard,

    NotificationsDropDown,
    NotificationsIndicator,
  }
});

// There might be some pages where app container is not available
if (document.querySelector('#app')) {
  app.$mount('#app')
} else {
  console.warn('Could not load vue app')
}
