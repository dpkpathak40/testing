import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import {Vue as VueIntegration} from '@sentry/integrations';

const safeOptions = Object.assign({}, {
  user: {},
  env: 'production'
}, window.appConfig || {});

// https://docs.sentry.io/platforms/javascript/guides/vue/
Sentry.init({
  dsn: process.env.MIX_SENTRY_DSN,
  environment: safeOptions.env,
  sendDefaultPii: true,
  integrations: [new VueIntegration({Vue, attachProps: true, logErrors: true})],
});

Sentry.setUser(safeOptions.user);


