import jQuery from 'jquery/dist/jquery.slim';
import Popper from 'popper.js';
// Selective bootstrap.js build
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/alert';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/collapse';
import '../../sass/admin/vendor.scss'

window.jQuery = jQuery;

window.Popper = Popper;

require('@/js/fileInput');

