(function ($) {
  'use strict';

  // Toggle the side navigation
  $('#sidebar-toggle').on('click',function (event) {
    event.preventDefault();
    $('body').toggleClass('sidebar-toggled');
    $('.sidebar').toggleClass('toggled');
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (event) {
    if ($(window).width() > 768) {
      let e0 = event.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      event.preventDefault();
    }
  });

})(jQuery);
