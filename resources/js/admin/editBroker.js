import flatpickr from 'flatpickr';

const startFP = flatpickr('#start-date', {
  onChange: (selectedDates, dateStr, instance) => {
    const nextWeek = flatpickr.parseDate(dateStr, 'Y-m-d').fp_incr(6);
    let currentDate = new Date();
    currentDate = flatpickr.parseDate(currentDate, 'Y-m-d');

    endFP.set('minDate', dateStr);
    if (nextWeek >= currentDate) {
      endFP.set('maxDate', currentDate)
    } else {
      endFP.set('maxDate', nextWeek)
    }
  },
  altInput: true,
  altFormat: 'F j, Y',
  maxDate: 'today'
});

const endFP = flatpickr('#end-date', {
  onChange: (selectedDates, dateStr, instance) => {
    startFP.set('maxDate', dateStr)
  },
  altInput: true,
  altFormat: 'F j, Y',
  maxDate: 'today'
});
