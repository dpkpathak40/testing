# OktoHub (Laravel App)

### Prerequisites

* php v8.0, [see](https://laravel.com/docs/installation) Laravel specific requirements
* Apache v2.4.33 with ```mod_rewrite```
* MySql v8.0
* [Composer](https://getcomposer.org) v2.1
* [node-js](https://github.com/creationix/nvm) >=16.3 and [yarn](https://yarnpkg.com/en/) >=1.22

### Quick setup

* Clone this repo, checkout to ```dev``` branch
* Install dependencies

```bash
composer install
yarn install
```

* Write permissions on ```storage``` and ```bootstrap/cache``` folders
* Create a config file (copy from ```.env.example```), and update environment variables in ```.env``` file

```bash
cp .env.example .env
php artisan key:generate
```

* Migrate and Seed database

```bash
php artisan migrate
php artisan db:seed
```

* Create the symbolic link for local file uploads

```bash
php artisan storage:link
```

* Point your web server to **public** folder of this project
* Additionally, you can run these commands on production server

```bash
php artisan optimize
```

### Laravel Horizon (optional)

* The application is utilizing the Laravel [Horizon](https://laravel.com/docs/horizon) to manage queues.
* Install and configure [redis-server](https://redis.io) v6.x and [supervisord](http://supervisord.org) on the server.
* :warning: Run these commands after each deployment

```bash
php artisan horizon:terminate
php artisan horizon:publish
php artisan queue:restart
```

### Asset building

* This project is using a custom webpack solution [Laravel-Bundler](https://github.com/ankurk91/laravel-bundler)
* See available commands in [package.json](./package.json) file

### Credentials

* Default user credentials can be found in `database/seeds`
* In production, you may want to create very first admin user using the command -

```bash
php artisan create:admin
```

### Integrating Stripe Webhooks

[Configure](https://dashboard.stripe.com/account/webhooks) Webhooks to

* receive events from your account
    - Webhook URL would be `${APP_URL}/webhooks/stripe`
    - Choose only registered event types to send
    - Put Signing secret in `.env` file
* receive events from Connect applications
    - Webhook URL would be `${APP_URL}/webhooks/stripe/connect`
    - Choose only registered event types to send
    - Put Signing secret in `.env` file

### 3rd party services used

* E-mail service
* [Stripe](https://stripe.com) payment gateway
* [Bandwidth SMS](https://www.bandwidth.com/messaging/sms-api)
* [Active Campaign](https://www.activecampaign.com/) CRM
* [Sentry](https://docs.sentry.io/platforms/php/laravel/) error reporting service (optional)
