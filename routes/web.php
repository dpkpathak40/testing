<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], base_path('routes/guest.php'));

// Realtor dashboard
Route::get('dashboard', 'DashboardController')->middleware(['auth', 'documents.verified'])
    ->name('dashboard');

// Realtor documents
Route::group(['prefix' => 'documents', 'as' => 'documents.', 'middleware' => ['auth']], function () {
    Route::get('/', 'DocumentController@create')->name('create');
    Route::post('store', 'DocumentController@store')->name('store')
        ->middleware('can:create,'.\App\Models\UserDocument::class);
    Route::get('{userDocument}', 'DocumentController@show')->name('show')
        ->middleware('can:view,userDocument');
});

// User account routes
Route::group(['prefix' => 'account', 'as' => 'account.', 'middleware' => ['auth']], function () {

    // My profile
    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/', 'Account\ProfileController@edit')->name('edit');

        Route::post('profile', 'Account\ProfileController@update')->name('updateProfile');
    });

    // Change password
    Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
        Route::get('/', 'Account\PasswordController@edit')->name('edit');
        Route::post('update', 'Account\PasswordController@update')->name('update');
    });
});

// User profile
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'middleware' => ['auth']], function () {
    Route::get('{user}', 'ProfileController@show')->name('show');
});

// Job
Route::group(['prefix' => 'jobs', 'as' => 'jobs.', 'middleware' => ['auth', 'documents.verified']], function () {
    Route::get('{job}/detail', 'Job\ShowController@show')->name('show')
        ->middleware('can:view,job');
    Route::get('services', 'Job\CreateController@services')->name('services');
    Route::get('create/{service}', 'Job\CreateController@create')->name('create')
        ->middleware('can:create,'.App\Models\Job::class.',service');
    Route::post('create/{service}', 'Job\CreateController@store')->name('store')
        ->middleware('can:create,'.App\Models\Job::class.',service');
    Route::post('amount/{service}', 'Job\CreateController@getAmount')->name('amount')
        ->middleware('can:create,'.App\Models\Job::class.',service');
    Route::post('{job}/update', 'Job\CreateController@updateIncentive')->name('incentive.update');

    // Job assignment
    Route::group(['prefix' => '{job}/payments', 'as' => 'payments.'], function () {
        Route::get('assign/{user}', 'Job\AssignController@showAssignForm')
            ->name('assign')->middleware('can:assign,job,user');
        Route::post('assign/{user}', 'Job\AssignController@assign')
            ->middleware('can:assign,job,user');
    });

    // Job note
    Route::group(['prefix' => '{job}/chat', 'as' => 'chat.'], function () {
        Route::post('/', 'Job\ChatController@update')->name('update')
            ->middleware('can:update,'.\App\Models\JobChat::class.',job');
        Route::get('/', 'Job\ChatController@index')->name('index')
            ->middleware('can:view,'.\App\Models\JobChat::class.',job');
        Route::get('/{jobChat}/attachment', 'Job\ChatController@downloadAttachment')
            ->name('attachment')->middleware('can:view,'.\App\Models\JobChat::class.',job');
    });

    // Various actions on a Job
    Route::group(['prefix' => 'actions', 'as' => 'actions.'], function () {
        Route::post('{job}/accept', 'Job\ActionController@accept')
            ->name('accept')->middleware('can:accept,job');
        Route::delete('{job}', 'Job\ActionController@delete')
            ->name('destroy')->middleware('can:delete,job');
        Route::post('{job}/ignore', 'Job\ActionController@ignore')
            ->name('ignore');
        Route::post('{job}/done', 'Job\ActionController@markAsDone')
            ->name('done')->middleware('can:markAsDone,job');
        Route::post('{job}/complete', 'Job\ActionController@markAsCompleted')
            ->name('complete')->middleware('can:markAsCompleted,job');
    });
});

// Sent jobs
Route::group(['prefix' => 'sent-jobs', 'as' => 'sentJobs.', 'middleware' => ['auth', 'documents.verified']], function () {
    Route::get('sent', 'SentJobController@sent')->name('sent');
    Route::get('assigned', 'SentJobController@assigned')->name('assigned');
    Route::get('for-review', 'SentJobController@forReview')->name('forReview');
    Route::get('completed', 'SentJobController@completed')->name('completed');
});

// Received jobs
Route::group(['prefix' => 'received-jobs', 'as' => 'receivedJobs.', 'middleware' => ['auth', 'documents.verified']], function () {
    Route::get('received', 'ReceivedJobController@received')->name('received');
    Route::get('accepted', 'ReceivedJobController@accepted')->name('accepted');
    Route::get('to-do', 'ReceivedJobController@toDo')->name('toDo');
    Route::get('completed', 'ReceivedJobController@completed')->name('completed');
});

// Database Notifications
Route::group(['prefix' => 'notifications', 'as' => 'notifications.', 'middleware' => ['auth']], function () {
    Route::get('/', 'NotificationController@index')->name('index');
    Route::delete('delete-all', 'NotificationController@deleteAll')->name('deleteAll');

    Route::post('unread-all', 'NotificationController@markAllAsUnread')->name('markAllAsUnread');
    Route::post('{notification}/unread', 'NotificationController@markAsUnread')->name('markAsUnread');

    Route::post('read-all', 'NotificationController@markAllAsRead')->name('markAllAsRead');
    Route::post('{notification}/read', 'NotificationController@markAsRead')->name('markAsRead');
});
