<?php

use Illuminate\Support\Facades\Route;

/*
|---------------------------------------------------------------
| Guest web routes
|---------------------------------------------------------------
|
| Routes that are to be consumed by guest users.
|
| Note: you need to manually add `guest` middleware on routes
| Note: NOT all routes have `guest` middleware applied, for example `logout`
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {

    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Password Reset
    Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
        Route::get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('request');
        Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('email');
        Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset');
        Route::post('reset', 'Auth\ResetPasswordController@reset');
    });
});

Route::redirect('/', 'login', 302);

// Login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->middleware('honeypot');

// Password Reset
Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
    Route::get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('request');
    Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('email');
    Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset');
    Route::post('reset', 'Auth\ResetPasswordController@reset');
});

// Email verification
Route::group(['prefix' => 'verification', 'as' => 'verification.', 'middleware' => ['guest', 'throttle:6,1']], function () {
    Route::get('verify/{user}', 'Auth\VerificationController@verify')->name('verify');
    Route::get('resend', 'Auth\VerificationController@showResendForm')->name('resend');
    Route::post('resend', 'Auth\VerificationController@resend');
});

// Stripe connect
Route::group(['prefix' => 'stripe', 'as' => 'stripe.', 'middleware' => ['guest', 'throttle:6,1']], function () {
    Route::get('/', 'StripeConnectController@status')->name('status');
    Route::get('callback', 'StripeConnectController@callback')->name('callback');
});

// Stripe Webhooks
Route::group(['prefix' => 'webhooks/stripe', 'as' => 'webhooks.stripe.'], function () {
    Route::stripeWebhooks('{configKey}')->name('connect')->where('configKey', 'connect');
    Route::stripeWebhooks('/')->name('main');
});

// Bandwidth Webhooks
Route::group(['prefix' => 'webhooks/bandwidth', 'as' => 'webhooks.bandwidth.'], function () {
    Route::post('message', 'BandwidthWebhookController')->name('callback');
});
