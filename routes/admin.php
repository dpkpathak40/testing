<?php

use Illuminate\Support\Facades\Route;

/*
|---------------------------------------------------------------
| Admin web routes
|---------------------------------------------------------------
|
| Define routes that starts with /admin/ here
| All routes are already protected with 'auth:admin' middleware
| Don't prefix your routes with `/admin/`
| Don't prefix controllers with `Admin\`
|
*/

Route::get('/', 'DashboardController')->name('dashboard');

Route::get('users/{user}/documents/{document}', 'UserController@downloadDocument')
    ->name('users.documents.show');

Route::resource('users', 'UserController')
    ->only(['index', 'edit', 'update', 'show']);

Route::post('brokers/{broker}/actions/stripe', 'Broker\BrokerActionController@sendStripeInvite')
    ->name('brokers.actions.stripeInvite');

Route::post('brokers/{broker}/report', 'Broker\BrokerController@report')
    ->name('brokers.report');

Route::resource('brokers', 'Broker\BrokerController')
    ->only(['index', 'edit', 'update', 'destroy', 'create', 'store']);

Route::get('brokers/{broker}/download-w9', 'Broker\BrokerController@downloadW9Document')
    ->name('brokers.downloadW9Document');

// User account routes ...
Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
    Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
        Route::get('/', 'Account\PasswordController@edit')->name('edit');
        Route::post('update', 'Account\PasswordController@update')->name('update');
    });

    Route::group(['prefix' => 'actions', 'as' => 'actions.'], function () {
        Route::post('password-reset-email', 'Account\ActionController@sendPasswordResetEmail')
            ->name('passwordResetEmail');
    });
});

// Admin manages other admins
Route::group(['prefix' => 'admins', 'as' => 'admins.', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('create', 'AdminController@create')->name('create');
    Route::post('/', 'AdminController@store')->name('store');
    Route::get('{admin}/edit', 'AdminController@edit')->name('edit');
    Route::put('{admin}', 'AdminController@update')->name('update')
        ->middleware('can:update,admin');
    Route::delete('{admin}', 'AdminController@destroy')->name('destroy')
        ->middleware('can:delete,admin');

    Route::group(['prefix' => 'actions', 'as' => 'actions.'], function () {
        Route::post('{admin}/password-reset-email', 'ActionController@sendPasswordResetEmail')
            ->name('passwordResetEmail');
        Route::patch('{admin}/toggle-block', 'ActionController@toggleBlockedStatus')
            ->name('toggleBlock')->middleware('can:update,admin');
    });
});

Route::resource('services', 'ServiceController')
    ->only(['index', 'edit', 'update', 'destroy', 'create', 'store']);

Route::put('jobs/{job}/cancel', 'Job\ActionController@cancel')
    ->middleware('can:cancel,job')
    ->name('jobs.cancel');

Route::resource('jobs', 'JobController')
    ->only(['index', 'show']);
