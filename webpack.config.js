'use strict';

module.exports = require('laravel-bundler')({
    entry: {
      app: './resources/js/app.js',
      admin: './resources/js/admin/app.js',
    },
  },
  require('laravel-bundler/src/recipes/vue-2.js'),
  require('laravel-bundler/src/recipes/alias'),
  require('laravel-bundler/src/recipes/jquery'),
);
