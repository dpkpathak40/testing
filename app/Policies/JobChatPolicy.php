<?php

namespace App\Policies;

use App\Models\Job;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobChatPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update note the given job.
     *
     * @param  \App\Models\User  $authUser
     * @param  \App\Models\Job  $job
     *
     * @return mixed
     */
    public function update(User $authUser, Job $job)
    {
        return $job->creator->is($authUser) || $job->assignee->is($authUser);
    }

    /**
     * Determine if given user is assigned on the job.
     *
     * @param  User  $user
     * @param  Job  $job
     *
     * @return mixed
     */
    protected function userIsAssignee(User $user, Job $job)
    {
        return $job->assignee && $job->assignee->is($user);
    }

    /**
     * Determine whether the user can view note the given job.
     *
     * @param  \App\Models\User  $authUser
     * @param  \App\Models\Job  $job
     *
     * @return mixed
     */
    public function view(User $authUser, Job $job)
    {
        return $job->creator->is($authUser) ||
            $this->userIsAssignee($authUser, $job);
    }
}
