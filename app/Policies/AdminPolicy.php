<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the logged-in admin can update the admin.
     *
     * @param  \App\Models\Admin  $authUser
     * @param  \App\Models\Admin  $admin
     *
     * @return mixed
     */
    public function update(Admin $authUser, Admin $admin)
    {
        return $authUser->isNot($admin);
    }

    /**
     * Determine whether the logged-in admin can delete the admin.
     *
     * @param  \App\Models\Admin  $authUser
     * @param  \App\Models\Admin  $admin
     *
     * @return mixed
     */
    public function delete(Admin $authUser, Admin $admin)
    {
        return $authUser->isNot($admin);
    }
}
