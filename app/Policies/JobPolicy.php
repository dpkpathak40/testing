<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Job;
use App\Models\Service;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create new jobs.
     *
     * @param User $authUser
     * @param Service $service
     *
     * @return bool
     */
    public function create(User $authUser, Service $service)
    {
        return $service->active;
    }

    /**
     * Determine if the given user can done job.
     *
     * @param \App\Models\User $authUser
     * @param \App\Models\Job $job
     *
     * @return bool
     */
    public function markAsCompleted(User $authUser, Job $job)
    {
        return $this->userIsOwner($authUser, $job) &&
            $job->statusIs(Job::STATUS_DONE);
    }

    /**
     * Determine if given user owns the job.
     *
     * @param User $user
     * @param Job $job
     *
     * @return mixed
     */
    protected function userIsOwner(User $user, Job $job)
    {
        return $job->creator->is($user);
    }

    public function markAsDone(User $authUser, Job $job)
    {
        return $job->assignee->is($authUser);
    }

    public function accept(User $authUser, Job $job)
    {
        if ($job->applicants()->count() >= config('project.job.max_number_of_applicants')) {
            return $this->deny('Max number of applicants reached on this Job.');
        }

        if ($job->expired) {
            return $this->deny('The requested Job has been expired.');
        }

        return ! $this->userIsOwner($authUser, $job) &&
            $job->statusIs(Job::STATUS_PENDING);
    }

    /**
     * Determine if the given user can view job.
     *
     * @param \App\Models\User $authUser
     * @param \App\Models\Job $job
     *
     * @return bool
     */
    public function view(User $authUser, Job $job)
    {
        return $this->userIsOwner($authUser, $job);
    }

    public function assign(User $authUser, Job $job, User $assignee)
    {
        if ($job->expired) {
            return $this->deny('The requested Job has been expired.');
        }

        if ($job->assignee_user_id) {
            return $this->deny('The Job has been assigned to someone already.');
        }

        $assigneeAcceptedTheJob = $job->applicants()
            ->whereExists(function ($query) use ($assignee) {
                $query->where('user_id', $assignee->getKey());
            })->count();

        return $assigneeAcceptedTheJob &&
            $this->userIsOwner($authUser, $job);
    }

    /**
     * Determine if the given user can delete job.
     *
     * @param \App\Models\User $authUser
     * @param \App\Models\Job $job
     *
     * @return bool
     */
    public function delete(User $authUser, Job $job)
    {
        return $this->userIsOwner($authUser, $job) &&
            is_null($job->assignee_user_id);
    }

    public function cancel(Admin $authUser, Job $job)
    {
        return $job->assignee_user_id &&
            ! $job->statusIs(Job::STATUS_COMPLETED) &&
            ! $job->cancelled_at;
    }
}
