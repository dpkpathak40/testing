<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserDocument;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserDocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user document.
     *
     * @param  \App\Models\User  $authUser
     * @param  \App\Models\UserDocument  $userDocument
     *
     * @return mixed
     */
    public function view(User $authUser, UserDocument $userDocument)
    {
        return $userDocument->user->is($authUser);
    }

    /**
     * Determine whether the user can create user documents.
     *
     * @param  \App\Models\User  $authUser
     *
     * @return mixed
     */
    public function create(User $authUser)
    {
        return ! $authUser->documentsStatusIs(UserDocument::STATUS_VERIFIED);
    }
}
