<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;
use Stripe\Stripe;

class StripeServiceProvider extends ServiceProvider
{
    public function register()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
        Stripe::setClientId(config('services.stripe.key'));
        Stripe::setMaxNetworkRetries(2);
        Stripe::setAppInfo(config('app.name'), config('app.env'), config('app.url'));

        $this->app->singleton(\Stripe\StripeClient::class, static function (Container $app) {
            return new \Stripe\StripeClient([
                'api_key' => config('services.stripe.secret'),
                'client_id' => config('services.stripe.key'),
            ]);
        });

        /*
         * \Illuminate\Log\Logger is already
         * Compatible with \Stripe\Util\LoggerInterface
         */
        Stripe::setLogger($this->app['log']);
    }
}
