<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->dateDirective();
        $this->isInvalidDirective();
        $this->validationMessageDirective();
    }

    private function dateDirective()
    {
        Blade::directive('date', function ($expression) {
            $segments = explode(',', $expression);
            $carbon = trim($segments[0]);
            $format = trim(\Illuminate\Support\Arr::get($segments, 1), false) ? 'j M Y' : 'j M Y, g:ia';

            return $this->wrapInTime($carbon, "<?php echo ($carbon)->format('$format'); ?>");
        });
    }

    private function wrapInTime($carbon, $content)
    {
        return "<time datetime=\"<?php echo ($carbon)->toIso8601String() ?>\" 
title=\"<?php echo ($carbon)->format('j M Y, g:i:s a, T')?>\">".$content.'</time>';
    }

    private function isInvalidDirective()
    {
        Blade::directive('isInvalid', function ($expression) {
            $segments = explode(',', $expression);
            $input = trim($segments[0]);
            $errorBag = trim(\Illuminate\Support\Arr::get($segments, 1, 'default'));

            return '<?php echo $errors->'.$errorBag.'->has('.$input.') ? "is-invalid" : ""; ?>';
        });
    }

    private function validationMessageDirective()
    {
        Blade::directive('validationMessage', function ($expression) {
            $segments = explode(',', $expression);
            $input = trim($segments[0]);
            $errorBag = trim(\Illuminate\Support\Arr::get($segments, 1, 'default'));

            return '<?php if ($errors->'.$errorBag.'->has('.$input.')) { ?>
                      <div class="invalid-feedback d-block">
                        <?php echo e($errors->'.$errorBag.'->first('.$input.')); ?>
                      </div>
                  <?php } ?>';
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
