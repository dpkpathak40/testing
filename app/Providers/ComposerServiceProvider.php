<?php

namespace App\Providers;

use App\ViewComposers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('sentJobs._tabs', ViewComposers\SentJobTabsComposer::class);
        View::composer('receivedJobs._tabs', ViewComposers\ReceivedJobTabsComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
