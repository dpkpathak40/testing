<?php

namespace App\Providers;

use App\Models;
use App\Policies;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Models\Job::class => Policies\JobPolicy::class,
        Models\Admin::class => Policies\AdminPolicy::class,
        Models\UserDocument::class => Policies\UserDocumentPolicy::class,
        Models\JobChat::class => Policies\JobChatPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
