<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Laravel\Horizon\Horizon;
use Laravel\Horizon\HorizonApplicationServiceProvider;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    public function boot()
    {
        Horizon::auth(fn ($request) => Auth::guard('admin')->check());

        Horizon::night();
    }
}
