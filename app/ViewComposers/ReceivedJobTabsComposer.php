<?php

namespace App\ViewComposers;

use App\Models\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ReceivedJobTabsComposer
{
    public function compose(View $view)
    {
        $user = Auth::user();
        $counts = new \stdClass();

        $counts->received_jobs = Job::receivedBy($user)->count();
        $counts->accepted_jobs = Job::acceptedBy($user)->count();

        $counts->to_do_jobs = Job::toDoFor($user)->count();
        $counts->completed_jobs = Job::completedFor($user)->count();

        $view->with(compact('counts'));
    }
}
