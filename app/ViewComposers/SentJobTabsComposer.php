<?php

namespace App\ViewComposers;

use App\Models\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class SentJobTabsComposer
{
    public function compose(View $view)
    {
        $user = Auth::user();
        $counts = new \stdClass();

        $counts->sent_jobs = Job::sentBy($user)->count();
        $counts->assigned_jobs = Job::assignedBy($user)->count();

        $counts->to_review_jobs = Job::toReviewBy($user)->count();
        $counts->completed_jobs = Job::completedBy($user)->count();

        $view->with(compact('counts'));
    }
}
