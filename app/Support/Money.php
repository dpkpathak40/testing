<?php

namespace App\Support;

class Money
{
    /**
     * Convert amount to cents.
     *
     * @source https://stackoverflow.com/questions/21735650/php-converting-dollars-to-cents
     * @param  int|float|string  $dollars
     *
     * @return string
     */
    public static function dollarsToCents($dollars)
    {
        return bcmul($dollars, 100);
    }

    /**
     * Convert amount to dollars.
     *
     * @source https://stackoverflow.com/questions/30713928/php-convert-cents-to-dollars/30714110
     * @param  int|float|string  $cents
     *
     * @return string
     */
    public static function centsToDollars($cents)
    {
        return bcdiv($cents, 100, 2);
    }

    /**
     * Convert a number into readable currency format.
     *
     * @param  int|float|string  $number
     * @param  string  $sign
     *
     * @return string
     */
    public static function format($number, $sign = '$')
    {
        return $sign.number_format((float) $number, 2, '.', ',');
    }
}
