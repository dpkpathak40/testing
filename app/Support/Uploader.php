<?php

namespace App\Support;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Uploader
{
    public static function dirPath(string $directory): string
    {
        $today = Date::today();

        return implode(DIRECTORY_SEPARATOR, [$directory, $today->year, $today->month]);
    }

    public static function generateName(UploadedFile | string $file): string
    {
        $baseName = Str::random(40);

        if ($file instanceof UploadedFile) {
            $extension = $file->guessClientExtension();
        } else {
            $extension = File::extension($file);
        }

        return $baseName.'.'.$extension;
    }
}
