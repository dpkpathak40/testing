<?php

namespace App\Console\Commands\Traits;

use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

trait SetsTestNow
{
    protected function setTestNow()
    {
        if ($testNow = $this->option('today')) {
            $message = "Setting today date to `{$testNow}` for testing.";
            $this->warn($message);
            Log::debug($message);

            Date::setTestNow($testNow);
            $this->info(sprintf('Datetime is freezed at: `%s`.', Date::now()));
        }
    }
}
