<?php

namespace App\Console\Commands\Migration;

use App\Models\Broker;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class PhoneNumberMigration extends Command
{
    use ConfirmableTrait;

    protected $signature = 'phone:migration
            {--force : Force the operation to run when in production}';

    protected $description = 'Migrate phone number in area code format.';

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        $this->line('Phone number migration starting...');

        $brokers = Broker::query()
            ->whereNull('area_code')
            ->latest('id');

        $this->line('Updating brokers');

        $brokers->each(function (Broker $broker) {
            $broker->fill([
                'phone' => $this->getPhoneNumber($broker->phone),
                'area_code' => $this->getAreaCode($broker->phone),
            ]);
            $broker->save();
        });

        $this->line('Updating users');

        $users = User::query()
            ->with('address')
            ->whereHas('address', function ($query) {
                $query->whereNull('area_code');
            })
            ->latest('id');

        $users->each(function (User $user) {
            /**
             * @var UserAddress $address
             */
            $address = $user->address;
            $address->fill([
                'primary_phone' => $this->getPhoneNumber($address->primary_phone),
                'area_code' => $this->getAreaCode($address->primary_phone),
            ]);
            $address->save();
        });

        $this->info('Phone number migration finished.');

        return Command::SUCCESS;
    }

    protected function getAreaCode($phone)
    {
        $withoutPlusOne = ltrim($phone, '+1');
        $code = substr($withoutPlusOne, 0, 3);

        return empty($code) ? null : $code;
    }

    protected function getPhoneNumber($phone)
    {
        $withoutPlusOne = ltrim($phone, '+1');
        $phone = substr($withoutPlusOne, 3);

        return empty($phone) ? null : $phone;
    }
}
