<?php

namespace App\Console\Commands\Migration;

use App\Models\JobChat;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\Date;

class NotesMigration extends Command
{
    use ConfirmableTrait;

    protected $signature = 'notes:migration
            {--force : Force the operation to run when in production}';

    protected $description = 'Migration of realtors notes.';

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        $this->line('Notes migration starting...');

        $jobNotes = JobChat::query()
            ->with(['job.creator'])
            ->where('sender_id', '=', 1)
            ->latest('id');

        $jobNotes->each(function (JobChat $note) {
            $note->fill([
                'sender_id' => $note->job->creator->getKey(),
                'read_at' => Date::now(),
            ]);
            $note->save();
        });

        $this->info('Notes migration finished.');

        return 0;
    }
}
