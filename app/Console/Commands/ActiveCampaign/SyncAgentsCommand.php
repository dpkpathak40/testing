<?php

namespace App\Console\Commands\ActiveCampaign;

use App\Jobs\ActiveCampaign\SyncAgentJob;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\Log;

class SyncAgentsCommand extends Command
{
    use ConfirmableTrait;

    protected $signature = 'sync:agents
            {--force : Force the operation to run when in production}';

    protected $description = 'Sync agents to active campaign crm.';

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        $this->line('sync:agents command starting...');
        Log::info('sync:agents command starting...');

        $agents = User::query()
            ->whereNull('active_campaign_contact_id')
            ->latest('id');

        $agents->each(function (User $user) {
            Log::debug("Processing id - {$user->getKey()}");
            $this->line("Processing id - {$user->getKey()}");

            // try/catch is helpful when queue connection is set to `sync`
            // this will let the loop continue despite errors
            try {
                dispatch(new SyncAgentJob($user));
            } catch (\Throwable $exception) {
                $this->error("Failed to sync id - {$user->getKey()}");
                report($exception);
            }
        });

        Log::info('sync:agents command finished!');
        $this->info('sync:agents command finished!');

        return Command::SUCCESS;
    }
}
