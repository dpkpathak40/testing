<?php

namespace App\Console\Commands\ActiveCampaign;

use App\Jobs\ActiveCampaign\SyncBrokerJob;
use App\Models\Broker;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\Log;

class SyncBrokersCommand extends Command
{
    use ConfirmableTrait;

    protected $signature = 'sync:brokers
            {--force : Force the operation to run when in production}';
    protected $description = 'Sync brokers to active campaign crm.';

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        $this->line('sync:broker command starting...');
        Log::info('sync:broker command starting...');

        $brokers = Broker::query()
            ->whereNull('active_campaign_contact_id')
            ->latest('id');

        $brokers->each(function (Broker $broker) {
            Log::debug("Processing id - {$broker->getKey()}");
            $this->line("Processing id - {$broker->getKey()}");

            // try/catch is helpful when queue connection is set to `sync`
            // this will let the loop continue despite errors
            try {
                dispatch(new SyncBrokerJob($broker));
            } catch (\Throwable $exception) {
                $this->error("Failed to sync id - {$broker->getKey()}");
                report($exception);
            }
        });

        Log::info('sync:broker command finished!');
        $this->info('sync:broker command finished!');

        return Command::SUCCESS;
    }
}
