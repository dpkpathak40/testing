<?php

namespace App\Console\Commands\Cron;

use App\Console\Commands\Traits\SetsTestNow;
use App\Mail\Admin\BrokerTransactionsReportMail;
use App\Models\Broker;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BrokerReportCommand extends Command
{
    use ConfirmableTrait, SetsTestNow;

    protected $signature = 'broker:report
            {--today= : Set today date time for testing purpose.}
            {--force : Force the operation to run when in production}';

    protected $description = 'Generate broker report for past week transactions';

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        $this->setTestNow();

        $this->line('Generating reports for brokers ...');
        Log::debug('Generating reports for brokers ...');

        $startDate = Date::now()->subWeek()->startOfDay();
        $endDate = Date::yesterday()->endOfDay();

        $brokers = $this->getApplicableBrokers($startDate, $endDate);

        $brokers->each(function (Broker $broker) use ($startDate, $endDate) {
            $this->line('Processing broker - '.$broker->getKey());
            Log::debug('Processing broker - '.$broker->getKey());

            // try/catch helps to keep loop running when queue is in sync mode
            try {
                Mail::to($broker)->send(new BrokerTransactionsReportMail($broker, $startDate, $endDate));
            } catch (\Throwable $exception) {
                report($exception);
                $this->alert('Error generating report for broker - '.$broker->getKey());
                $this->error($exception->getMessage());
            }
        });

        $this->info('Reports generation finished!.');
        Log::info('Reports generation finished!.');

        return Command::SUCCESS;
    }

    public function getApplicableBrokers(Carbon $startDate, Carbon $endDate)
    {
        return Broker::query()
            ->whereHas('users.assignedJobs', function ($query) use ($startDate, $endDate) {
                /* @var Builder $query */
                $query->where('status', Job::STATUS_COMPLETED)
                    ->where('updated_at', '>=', $startDate)
                    ->where('updated_at', '<=', $endDate);
            })
            ->whereNotNull('stripe_user_id')
            ->latest();
    }
}
