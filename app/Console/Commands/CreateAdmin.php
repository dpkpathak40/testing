<?php

namespace App\Console\Commands;

use App\Models\Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CreateAdmin extends Command
{
    protected $signature = 'create:admin';

    protected $description = 'Create a new admin user';

    public function handle()
    {
        $inputs = [
            'email' => $this->ask('Enter email address'),
            'password' => $this->secret('Enter password (min:8)'),
        ];

        $validator = Validator::make(
            $inputs,
            [
                'email' => 'bail|required|string|email:strict|unique:'.with(new Admin())->getTable(),
                'password' => 'required|string|min:8',
            ]
        );

        if ($validator->fails()) {
            $this->alert('Validation failed!');
            $this->error($validator->messages()->first());

            return 1;
        }

        DB::beginTransaction();

        try {
            $this->line('Creating new user ...');
            $user = new Admin([
                'email' => $inputs['email'],
                'password' => bcrypt($inputs['password']),
            ]);
            $user->save();
        } catch (\Throwable $e) {
            DB::rollBack();
            report($e);

            $this->alert('Error saving user!');
            $this->error($e->getMessage());

            return Command::FAILURE;
        }

        DB::commit();

        $this->info('User created successfully.');
        $this->line('<comment>ID:</comment> '.$user->getKey());
        $this->line('<comment>Email:</comment> '.$user->email);

        return Command::SUCCESS;
    }
}
