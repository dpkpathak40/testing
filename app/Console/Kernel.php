<?php

namespace App\Console;

use App\Console\Commands\Cron\BrokerReportCommand;
use Carbon\Carbon;
use Illuminate\Auth\Console\ClearResetsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Queue\Console\PruneFailedJobsCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(BrokerReportCommand::class, ['--force'])
            ->weeklyOn(Carbon::SUNDAY)
            ->timezone('US/Pacific')
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command(ClearResetsCommand::class)
            ->daily()
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command(ClearResetsCommand::class, ['admins'])
            ->dailyAt('01:00')
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command(PruneFailedJobsCommand::class, ['--hours' => Carbon::HOURS_PER_DAY * 30])
            ->dailyAt('01:30')
            ->withoutOverlapping()
            ->onOneServer();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
