<?php

use Illuminate\Support\Facades\Route;

/**
 * Compare given route with current route and return output if they match.
 *
 * @param string|array $patterns
 * @param string $output
 *
 * @return null|string
 */
function active_route($patterns, $output = 'active')
{
    return (Route::is($patterns)) ? $output : null;
}

/**
 * Convert a number into readable currency format.
 *
 * @param int|float $number
 * @param string $sign
 *
 * @return string
 */
function money($number, $sign = '$')
{
    return \App\Support\Money::format($number, $sign);
}
