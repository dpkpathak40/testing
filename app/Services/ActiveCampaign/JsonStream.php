<?php

namespace App\Services\ActiveCampaign;

use GuzzleHttp\Psr7\StreamDecoratorTrait;
use Psr\Http\Message\StreamInterface;

class JsonStream implements StreamInterface
{
    use StreamDecoratorTrait;

    /**
     * @return mixed|\stdClass
     */
    public function json()
    {
        $contents = (string) $this->getContents();
        // json_decode throws syntax error on empty string on php v7
        if ($contents === '') {
            return new \stdClass();
        }

        return \GuzzleHttp\Utils::jsonDecode($contents);
    }
}
