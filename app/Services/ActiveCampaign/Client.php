<?php

namespace App\Services\ActiveCampaign;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\ResponseInterface as Response;

class Client
{
    /**
     * CRM API Key.
     * @var string
     */
    protected $apiKey;

    /**
     * Base URL to webservice.
     * @var string
     */
    protected $apiUrl;

    public ?\GuzzleHttp\Client $client = null;

    protected array $headers = [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
    ];

    protected bool $breakOnErrors = true;

    public function __construct()
    {
        $this->apiKey = config('services.active_campaign.api_key');
        $this->apiUrl = config('services.active_campaign.api_url');
    }

    /**
     * Create fresh client
     * Optional method in chaining.
     *
     * Note: Client is immutable, so we will replace existing client
     */
    protected function createClient()
    {
        // http://docs.guzzlephp.org/en/stable
        $this->client = new GuzzleClient([
            'handler' => $this->getMiddlewareStack(),
            // Base URI follows RFC 3986
            'base_uri' => rtrim($this->apiUrl, '/').'/api/3/',
            'headers' => $this->headers,

            'connect_timeout' => 10.0, // seconds,
            'timeout' => 30.0, // seconds

            'http_errors' => $this->breakOnErrors, // throw exception on non 20x response codes
            'verify' => true, // SSL verification
            'allow_redirects' => false,
            'debug' => false, // don't leave this true in production
        ]);
    }

    protected function getClient(): GuzzleClient
    {
        if (is_null($this->client)) {
            $this->createClient();
        }

        return $this->client;
    }

    protected function getMiddlewareStack(): HandlerStack
    {
        $stack = HandlerStack::create();
        $stack->push(Middleware::mapResponse(function (Response $response) {
            $json = new JsonStream($response->getBody());

            return $response->withBody($json);
        }));

        return $stack;
    }

    public function withoutHttpErrors(): static
    {
        $this->breakOnErrors = false;

        return $this;
    }

    public function withHeaders(array $headers): static
    {
        $this->headers = array_merge_recursive(
            $this->headers,
            $headers
        );

        return $this;
    }

    public function withTokenAuth(): static
    {
        return $this->withHeaders([
            'Api-Token' => $this->apiKey,
        ]);
    }

    public function get($url, array $queryParams = [])
    {
        return $this->sendRequest('GET', $url, $queryParams);
    }

    public function post($url, array $body = [])
    {
        return $this->sendRequest('POST', $url, $body);
    }

    public function put($url, array $body = [])
    {
        return $this->sendRequest('PUT', $url, $body);
    }

    public function patch($url, array $body = [])
    {
        return $this->sendRequest('PATCH', $url, $body);
    }

    public function delete($url, array $body = [])
    {
        return $this->sendRequest('DELETE', $url, $body);
    }

    /**
     * Proxy method to guzzle request() method.
     *
     * @param string $method
     * @param string $url
     * @param array $payload
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRequest(string $method, string $url, array $payload = [])
    {
        $url = ltrim($url, '/');

        return $this->getClient()->request(
            $method,
            $url,
            array_merge_recursive(
                ['query' => $this->parseQueryParams($url)],
                $this->buildOptionsForRequest($method, $payload)
            )
        );
    }

    protected function buildOptionsForRequest(string $method, array $payload): array
    {
        $options = [];

        switch (strtolower($method)) {
            case 'get':
                $options['query'] = $payload;
                break;
            default:
                $options['json'] = $payload;
        }

        return $options;
    }

    protected function parseQueryParams(string $url): array
    {
        $query = [];
        parse_str(parse_url($url, PHP_URL_QUERY), $query);

        return $query;
    }

    public function __call($method, $args)
    {
        if (count($args) < 1) {
            throw new \InvalidArgumentException('Magic request methods require a URI and optional options array');
        }

        if (method_exists($this, $method)) {
            $this->{$method}(...$args);
        }

        // Hand over unknown methods calls to guzzle
        $this->client->{$method}(...$args);
    }
}
