<?php

namespace App\Models;

use App\Support\Uploader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Service extends Model
{
    use HasFactory;

    const STORAGE_DIR = 'services';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'amount' => 'float',
        'requires_number_of_homes' => 'boolean',
        'active' => 'boolean',
    ];

    protected $appends = [
        'image_url',
    ];

    protected static function boot()
    {
        parent::boot();

        // Delete attached file from storage
        static::deleted(function ($resource) {
            Storage::disk('public')->delete($resource->image);
        });
    }

    /**
     * Pull only active records.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_service')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Upload the given file.
     *
     * @param  \Symfony\Component\HttpFoundation\File\File|null  $file
     *
     * @return void
     */
    public function setImageAttribute(?\Symfony\Component\HttpFoundation\File\File $file)
    {
        // Delete old images if any
        // todo find a better way to delete them
        if (! is_null($this->image)) {
            Storage::disk('public')->delete($this->image);
        }

        if (is_null($file)) {
            $this->attributes['image'] = null;

            return;
        }

        $this->attributes['image'] = Storage::disk('public')->putFileAs(
            Uploader::dirPath(self::STORAGE_DIR),
            $file,
            Uploader::generateName($file)
        );
    }

    public function getImageUrlAttribute()
    {
        if (is_null($this->image)) {
            return asset('assets/okto-placeholder.png');
        }

        return Storage::disk('public')->url($this->image);
    }

    /**
     * Calculate amount based on number of homes.
     *
     * @param  int  $numberOfHomes
     *
     * @return float|int|mixed
     */
    public function chargeableAmount(int $numberOfHomes)
    {
        if ($this->requires_number_of_homes && $numberOfHomes > 1) {
            $firstHomeAmount = $this->amount;
            $extraHomes = $numberOfHomes - 1;

            return $firstHomeAmount + ($extraHomes * config('project.service.per_home_amount'));
        }

        return $this->amount;
    }
}
