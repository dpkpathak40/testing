<?php

namespace App\Models;

use App\Notifications\Admin\ResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    use HasFactory;

    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'blocked_at' => 'datetime',
    ];

    public function setBlockedStatusTo($value)
    {
        $this->forceFill(['blocked_at' => $value]);
    }

    public function getIsBlockedAttribute($value)
    {
        return ! is_null($this->blocked_at);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
