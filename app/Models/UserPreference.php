<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{
    use Traits\BelongsToUser;
    use HasFactory;

    protected $fillable = ['user_id', 'email_notifications', 'sms_notifications'];

    protected $casts = [
        'email_notifications' => 'boolean',
        'sms_notifications' => 'boolean',
    ];

    protected $attributes = [
        'email_notifications' => 1,
        'sms_notifications' => 1,
    ];
}
