<?php

namespace App\Models;

use App\Support\Uploader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Broker extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    const STORAGE_DIR = 'broker_documents';

    protected static function boot()
    {
        parent::boot();

        // Delete attached file from storage
        static::deleted(function ($resource) {
            Storage::delete($resource->w9_document);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getFullNameAttribute()
    {
        return trim($this->first_name.' '.$this->last_name);
    }

    public function getHasStripeIdAttribute()
    {
        return ! is_null($this->stripe_user_id);
    }

    public function getFullPhoneNumberAttribute()
    {
        return '+1'.$this->area_code.$this->phone;
    }

    public function setW9DocumentAttribute(\Illuminate\Http\UploadedFile $file = null)
    {
        if (is_null($file)) {
            $this->attributes['w9_document'] = null;

            return;
        }
        // Delete old images if any
        // todo find a better way to delete them
        if (! is_null($this->w9_document)) {
            Storage::delete($this->w9_document);
        }

        $this->attributes['w9_document'] = Storage::putFile(Uploader::dirPath(self::STORAGE_DIR), $file, 'private');
    }

    public function downloadW9Document($headers = [])
    {
        abort_unless(Storage::exists($this->w9_document), 404);

        $fileParts = explode('.', $this->w9_document);
        $documentName = 'w9-document'.'.'.last($fileParts);

        return Storage::download($this->w9_document, $documentName, $headers);
    }
}
