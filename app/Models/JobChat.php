<?php

namespace App\Models;

use App\Support\Uploader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class JobChat extends Model
{
    use Traits\BelongsToJob;
    use HasFactory;

    const STORAGE_DIR = 'chat_documents';

    protected $fillable = ['job_id', 'sender_id', 'message', 'attachment', 'read_at'];

    protected $hidden = ['attachment'];

    protected $appends = ['has_attachment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    /**
     * @param  \Illuminate\Http\UploadedFile|null  $uploadedFile
     */
    public function setAttachmentAttribute(?\Illuminate\Http\UploadedFile $uploadedFile)
    {
        if (! is_null($uploadedFile)) {
            $this->attributes['attachment'] = Storage::putFile(
                Uploader::dirPath(self::STORAGE_DIR),
                $uploadedFile,
                'private'
            );
        }
    }

    public function getHasAttachmentAttribute()
    {
        return ! is_null($this->attachment);
    }

    public function downloadAttachmentFile($headers = [])
    {
        abort_unless(Storage::exists($this->attachment), 404);

        $fileParts = explode('.', $this->attachment);
        $documentName = 'attachment'.'.'.last($fileParts);

        return Storage::download($this->attachment, $documentName, $headers);
    }
}
