<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use Traits\BelongsToUser;
    use HasFactory;

    protected $fillable = ['user_id', 'city', 'state', 'street_address', 'zip_code', 'area_code', 'primary_phone'];

    public function getFullPhoneNumberAttribute()
    {
        if (! is_null($this->area_code) && ! is_null($this->primary_phone)) {
            return trim('+1'.$this->area_code.$this->primary_phone);
        }

        return null;
    }
}
