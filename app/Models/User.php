<?php

namespace App\Models;

use App\Models\Traits\UserScopes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, UserScopes;
    use HasFactory;

    protected $fillable = [
        'email',
        'password',
        'stripe_customer_id',
        'active_campaign_contact_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'stripe_customer_id',
        'active_campaign_contact_id',
    ];

    protected $with = [
        'profile',
    ];

    protected $casts = [
        'rating' => 'integer',
        'email_verified_at' => 'datetime',
    ];

    public function getHasVerifiedEmailAttribute()
    {
        return ! is_null($this->email_verified_at);
    }

    public function getHasStripeIdAttribute()
    {
        return ! is_null($this->stripe_customer_id);
    }

    /**
     * Match document verification status with given value.
     *
     * @return bool
     */
    public function documentsStatusIs($status)
    {
        return $this->documents_status === $status;
    }

    public function setEmailAsVerified()
    {
        $this->forceFill(['email_verified_at' => $this->freshTimestamp()]);
    }

    /**
     * Set document verification status.
     *
     * @param  mixed  $value
     */
    public function setDocumentsStatusTo($value = null)
    {
        $this->forceFill(['documents_status' => $value]);
    }

    /**
     * Route notifications for the bandwidth channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     *
     * @return string|bool
     */
    public function routeNotificationForBandwidth($notification)
    {
        if ($this->preference->sms_notifications) {
            return $this->address->full_phone_number;
        }

        return false;
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     *
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        if ($this->preference->email_notifications) {
            return $this->email;
        }

        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class)->withDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preference()
    {
        return $this->hasOne(UserPreference::class)->withDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(UserDocument::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'user_service')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cities()
    {
        return $this->belongsToMany(City::class, 'user_city')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function broker()
    {
        return $this->belongsTo(Broker::class)->withDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address()
    {
        return $this->hasOne(UserAddress::class)->withDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assessments()
    {
        return $this->hasMany(UserJobAssessment::class);
    }

    /**
     * Get user rating.
     * Notice: the required relation should be eager loaded to prevent n+1.
     *
     * @return mixed
     */
    public function getRatingAttribute()
    {
        return round($this->assessments->avg('rating'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdJobs()
    {
        return $this->hasMany(Job::class, 'creator_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function appliedJobs()
    {
        return $this->belongsToMany(Job::class, 'job_applicant', 'user_id', 'job_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function omittedJobs()
    {
        return $this->belongsToMany(Job::class, 'user_omitted_job', 'user_id', 'job_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignedJobs()
    {
        return $this->hasMany(Job::class, 'assignee_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chat()
    {
        return $this->hasMany(JobChat::class, 'user_id');
    }
}
