<?php

namespace App\Models;

use App\Models\Traits\CreatedByUser;
use App\Models\Traits\JobScopes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes, CreatedByUser, JobScopes;
    use HasFactory;

    const STATUS_DONE = 'done';
    const STATUS_COMPLETED = 'completed';
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELLED = 'cancelled';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'number_of_homes' => 'integer',
        'chargeable_amount' => 'float',
        'transferable_amount' => 'float',
        'chargeable_incentive_amount' => 'float',
        'transferable_incentive_amount' => 'float',
        'due_date' => 'datetime',
        'cancelled_at' => 'datetime',
    ];

    protected $appends = [
        'display_name',
        'formatted_due_date',
    ];

    protected $with = [
        'service',
    ];

    public function setStatusTo($value = null)
    {
        $this->forceFill(['status' => $value]);
    }

    public function statusIs($status)
    {
        return $this->status === $status;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cities()
    {
        return $this->belongsToMany(City::class, 'job_city')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applicants()
    {
        return $this->belongsToMany(User::class, 'job_applicant', 'job_id', 'user_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function omitters()
    {
        return $this->belongsToMany(User::class, 'user_omitted_job', 'job_id', 'user_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function charge()
    {
        return $this->hasOne(Charge::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transfer()
    {
        return $this->hasOne(Transfer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chat()
    {
        return $this->hasMany(JobChat::class, 'job_id');
    }

    /**
     * Store date in mysql format.
     *
     * @param  string  $date
     */
    public function setDueDateAttribute($date)
    {
        $this->attributes['due_date'] = Carbon::parse($date)->toDateString();
    }

    public function getFormattedDueDateAttribute()
    {
        return $this->due_date->format('l, j M Y');
    }

    public function getExpiredAttribute()
    {
        return $this->due_date->clone()->endOfDay()->addDays(1)->isPast();
    }

    public function getDisplayNameAttribute(): string
    {
        $name = $this->service->name;

        if ($this->number_of_homes > 1) {
            return $name.' ('.$this->number_of_homes.')';
        }

        return $name;
    }
}
