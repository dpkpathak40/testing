<?php

namespace App\Models;

use App\Support\Uploader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserProfile extends Model
{
    use Traits\BelongsToUser;
    use HasFactory;

    const STORAGE_DIR = 'user_avatars';

    const THUMB_PREFIX = 'thumb-';

    protected $fillable = ['user_id', 'first_name', 'last_name', 'avatar', 'bio', 'experience', 'education'];

    protected $appends = [
        'full_name', 'avatar_url', 'avatar_thumb_url',
    ];

    public function getFullNameAttribute()
    {
        if ($this->exists) {
            return trim($this->first_name.' '.$this->last_name);
        }

        return null;
    }

    /**
     * Upload the given file.
     *
     * @param  \Illuminate\Http\UploadedFile|null  $file
     *
     * @return void
     */
    public function setAvatarAttribute(?\Illuminate\Http\UploadedFile $file)
    {
        // Delete old images if any
        // todo find a better way to delete them
        if (! is_null($this->avatar)) {
            Storage::disk('public')->delete([
                $this->avatar,
                $this->getAvatarThumbFilePath($this->avatar),
            ]);
        }

        if (is_null($file)) {
            $this->attributes['avatar'] = null;

            return;
        }

        $image = Image::make($file->getRealPath());

        // Resize image
        $image->resize(null, 300, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $fileName = Uploader::generateName($file);
        $dirPath = Uploader::dirPath(self::STORAGE_DIR);

        // Upload thumbnail
        Storage::disk('public')->put($dirPath.'/'.self::THUMB_PREFIX.$fileName, $image->stream());

        // Upload original image
        $this->attributes['avatar'] = Storage::disk('public')->putFileAs($dirPath, $file, $fileName);
    }

    /**
     * Prefix thumb- to file name.
     *
     * @param  string  $filePath
     *
     * @return string
     */
    protected function getAvatarThumbFilePath($filePath)
    {
        return dirname($filePath).'/'.self::THUMB_PREFIX.basename($filePath);
    }

    public function getAvatarUrlAttribute()
    {
        if (! is_null($this->avatar)) {
            return Storage::disk('public')->url($this->avatar);
        }

        return $this->avatar;
    }

    /**
     * Get avatar thumbnail image URL.
     *
     * @return string
     */
    public function getAvatarThumbUrlAttribute()
    {
        if (is_null($this->avatar)) {
            return asset('assets/okto-placeholder.png');
        }

        return Storage::disk('public')->url($this->getAvatarThumbFilePath($this->avatar));
    }
}
