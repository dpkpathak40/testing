<?php

namespace App\Models;

use App\Support\Uploader;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserDocument extends Model
{
    use Traits\BelongsToUser;
    use HasFactory;

    const STORAGE_DIR = 'user_documents';
    const STATUS_PENDING = 'pending';
    const STATUS_REJECTED = 'rejected';
    const STATUS_VERIFIED = 'verified';

    public static $types = [
        'real_estate_license' => 'Real Estate License',
        'driving_license' => 'Driving License',
    ];

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = [
        'file',
    ];

    protected static function boot()
    {
        parent::boot();

        // Delete attached file from storage
        static::deleted(function ($resource) {
            Storage::delete($resource->file);
        });
    }

    public function getDisplayNameAttribute()
    {
        return \Illuminate\Support\Arr::get(static::$types, $this->name);
    }

    public function download($headers = [])
    {
        abort_unless(Storage::exists($this->file), 404);

        $fileParts = explode('.', $this->file);
        $documentName = $this->id.'-'.$this->name.'.'.last($fileParts);

        return Storage::download($this->file, $documentName, $headers);
    }

    /**
     * Upload the given file.
     *
     * @param  \Illuminate\Http\UploadedFile  $file
     *
     * @return void
     */
    public function setFileAttribute(\Illuminate\Http\UploadedFile $file)
    {
        $this->attributes['file'] = Storage::putFile(Uploader::dirPath(self::STORAGE_DIR), $file, 'private');
    }
}
