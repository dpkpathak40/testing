<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobAssessment extends Model
{
    use Traits\BelongsToUser, Traits\BelongsToJob;
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'rating' => 'integer',
    ];
}
