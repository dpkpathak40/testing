<?php

namespace App\Models\Traits;

use App\Models\Job;

trait BelongsToJob
{
    /**
     * This resource belong to one job.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
