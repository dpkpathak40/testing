<?php

namespace App\Models\Traits;

use App\Models\Job;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait JobScopes
{
    /**
     * Scope a query to only include received jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $receiver
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReceivedBy(Builder $query, User $receiver)
    {
        return $query
            ->createdBy($receiver->getKey(), '!=')
            ->whereDoesntHave('assignee')
            ->has('applicants', '<', config('project.job.max_number_of_applicants'))
            ->whereDoesntHave('applicants', function (Builder $query) use ($receiver) {
                $query->where('user_id', $receiver->getKey());
            })
            ->whereDoesntHave('omitters', function (Builder $query) use ($receiver) {
                $query->where('user_id', $receiver->getKey());
            })
            ->whereIn('service_id', $receiver->services->modelKeys())
            ->whereHas('cities', function (Builder $query) use ($receiver) {
                $query->select('job_city.job_id')
                    ->whereIn('job_city.city_id', $receiver->cities->modelKeys())
                    ->groupBy('job_city.job_id')
                    // whereIn does not ensure that all of the ids matched
                    // whereIn can return results when any of ids matches
                    // but we want to this closure to return `true` only when ALL of ids matches
                    // @source https://stackoverflow.com/questions/11636061/matching-all-values-in-in-clause
                    ->havingRaw('COUNT(DISTINCT `job_city`.`city_id`) =
                    (SELECT COUNT(DISTINCT `job_city`.`city_id`) FROM `job_city` WHERE `job_city`.`job_id` = `jobs`.`id`)');
            })
            ->notExpired();
    }

    /**
     * Scope a query to only include accepted jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $receiver
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcceptedBy(Builder $query, User $receiver)
    {
        return $query->whereDoesntHave('assignee')
            ->whereHas('applicants', function (Builder $query) use ($receiver) {
                $query->where('user_id', $receiver->id);
            })
            ->notExpired();
    }

    /**
     * Scope a query to only include `to-do` jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $assignee
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToDoFor(Builder $query, User $assignee)
    {
        return $query->where('assignee_user_id', $assignee->id)
            ->where('status', '!=', Job::STATUS_COMPLETED)
            ->whereNull('cancelled_at');
    }

    /**
     * Scope a query to only include upcoming jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $assignee
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUpcomingFor(Builder $query, User $assignee)
    {
        return $query->where('assignee_user_id', $assignee->id)
            ->where('status', Job::STATUS_PENDING)
            ->whereNull('cancelled_at');
    }

    /**
     * Scope a query to only include `completed` jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $creator
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompletedFor(Builder $query, User $creator)
    {
        return $query->where('assignee_user_id', $creator->id)
            ->where('status', Job::STATUS_COMPLETED);
    }

    /**
     * Scope a query to only include sent jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $creator
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSentBy(Builder $query, User $creator)
    {
        return $query->createdBy($creator)
            ->whereDoesntHave('assignee');
    }

    /**
     * Scope a query to only include assigned jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $creator
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAssignedBy(Builder $query, User $creator)
    {
        return $query->createdBy($creator)
            ->whereHas('assignee')
            ->where('status', Job::STATUS_PENDING)
            ->whereNull('cancelled_at');
    }

    /**
     * Scope a query to only include `done` jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $creator
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToReviewBy(Builder $query, User $creator)
    {
        return $query->createdBy($creator)
            ->whereHas('assignee')
            ->where('status', Job::STATUS_DONE)
            ->whereNull('cancelled_at');
    }

    /**
     * Scope a query to only include `completed` jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $creator
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompletedBy(Builder $query, User $creator)
    {
        return $query->createdBy($creator)
            ->whereHas('assignee')
            ->where('status', Job::STATUS_COMPLETED);
    }

    /**
     * Scope a query to only include `non-expired` jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotExpired(Builder $query)
    {
        return $query->whereDate('due_date', '>=', today()->subDays(1));
    }
}
