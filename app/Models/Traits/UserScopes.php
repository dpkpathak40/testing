<?php

namespace App\Models\Traits;

use App\Models\Job;
use Illuminate\Database\Eloquent\Builder;

trait UserScopes
{
    /**
     * @param Job $job
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApplicableFor(Builder $query, Job $job)
    {
        return $query
            ->whereDoesntHave('createdJobs', function (Builder $query) use ($job) {
                $query->where('jobs.id', $job->id);
            })
            ->whereHas('services', function (Builder $query) use ($job) {
                $query->where('service_id', $job->service->id);
            })
            ->whereHas('cities', function (Builder $query) use ($job) {
                $query->select('user_city.user_id')
                    ->whereIn('user_city.city_id', $job->cities->modelKeys())
                    ->groupBy('user_city.user_id')
                    ->havingRaw('COUNT(DISTINCT `user_city`.`city_id`) = '.$job->cities->count());
            });
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $channel
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWantsNotificationOn($query, string $channel)
    {
        return $query
            ->whereHas('preference', function ($query) use ($channel) {
                $query->where($channel.'_notifications', 1);
            });
    }
}
