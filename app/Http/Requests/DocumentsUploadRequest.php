<?php

namespace App\Http\Requests;

use App\Models\Broker;
use App\Models\UserDocument;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentsUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'broker' => [
                'bail', 'required', 'integer', Rule::exists(with(new Broker())->getTable(), 'id')
                    ->where(function ($query) {
                        $query->whereNotNull('stripe_user_id');
                    }),
            ],
        ];

        foreach (UserDocument::$types as $slug => $name) {
            $rules[$slug] = 'required|file|mimes:'.config('project.media.user_document.allowed_mimes').
                '|max:'.config('project.media.user_document.max_size');
        }

        return $rules;
    }
}
