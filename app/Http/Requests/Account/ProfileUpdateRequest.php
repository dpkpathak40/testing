<?php

namespace App\Http\Requests\Account;

use App\Rules\PersonNameRule;
use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'min:3', 'max:255', new PersonNameRule()],
            'last_name' => ['nullable', 'string', 'max:255', new PersonNameRule()],
            'avatar' => 'bail|nullable|image|dimensions:min_width='.config('project.media.avatar.min_width').',min_height='.config('project.media.avatar.min_height').'|mimes:jpeg,jpg,png|max:'.config('project.media.avatar.max_size'),
            'bio' => 'nullable|max:1000',
            'experience' => 'nullable|max:1000',
            'education' => 'nullable|max:1000',
            'city' => 'nullable|max:255',
            'state' => 'nullable|min:2|max:255',
            'street_address' => 'nullable|max:255',
            'zip_code' => 'nullable|digits:5',
            'area_code' => 'nullable|required|digits:3',
            'primary_phone' => 'nullable|required|digits_between:7,10',
            'cities' => 'bail|required|array',
            'cities.*' => 'bail|integer|distinct|exists:cities,id',
            'services' => 'bail|required|array',
            'services.*' => 'bail|integer|distinct|exists:services,id',
        ];
    }
}
