<?php

namespace App\Http\Requests\Admin\Admin;

class UpdateRequest extends CreateRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['email'] .= ','.$this->route('admin')->getKey();

        return $rules;
    }
}
