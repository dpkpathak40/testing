<?php

namespace App\Http\Requests\Admin\Account;

use Illuminate\Foundation\Http\FormRequest;

class PasswordUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth('admin')->check();
    }

    public function rules()
    {
        return [
            'current_password' => ['required', 'string', 'current_password'],
            'password' => 'bail|required|string|min:8|different:current_password|confirmed',
        ];
    }
}
