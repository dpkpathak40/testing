<?php

namespace App\Http\Requests\Admin\Service;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'description' => 'nullable|min:3|max:255',
            'amount' => 'required|numeric|min:1|max:9999',
            'requires_number_of_homes' => 'nullable',
            'active' => 'nullable',
            'image' => 'bail|nullable|image|dimensions:min_width='.config('project.media.service.min_width').',min_height='.config('project.media.service.min_height').'|mimes:jpeg,jpg,png|max:'.config('project.media.service.max_size'),
        ];
    }
}
