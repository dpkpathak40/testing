<?php

namespace App\Http\Requests\Admin;

use App\Models\UserDocument;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentsStatusUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documents_status' => [
                'required',
                Rule::in([
                    UserDocument::STATUS_REJECTED,
                    UserDocument::STATUS_VERIFIED,
                ]),
            ],
            'reject_reason' => [
                'nullable',
                'min:10',
                'max:1000',
                Rule::requiredIf(function () {
                    return $this->input('documents_status') === UserDocument::STATUS_REJECTED;
                }),
            ],
        ];
    }
}
