<?php

namespace App\Http\Requests\Admin\Broker;

use App\Models\Broker;
use App\Rules\PersonNameRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'min:3', 'max:255'],
            'last_name' => ['nullable', 'string', 'min:1', 'max:255'],
            'email' => 'bail|required|string|email:strict|max:255|unique:'.with(new Broker())->getTable().',email',
            'area_code' => 'required|digits:3',
            'phone' => 'required|digits_between:7,10',
            'w9_document' => 'required|file|mimes:'.config('project.media.broker_document.allowed_mimes').
                '|max:'.config('project.media.broker_document.max_size'),
            'owner_name' => ['nullable', 'string', 'min:3', 'max:255', new PersonNameRule()],
            'company_name' => ['nullable', 'string', 'min:3', 'max:255'],
        ];
    }
}
