<?php

namespace App\Http\Requests\Admin\Broker;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Date;
use Illuminate\Validation\Validator;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $startDate = Date::parse($this->input('start_date'))->format('Y-m-d');
        $endDate = Date::parse($this->input('start_date'))->addDays(6)->format('Y-m-d');

        return [
            'start_date' => ['required', 'date_format:"Y-m-d"', 'before_or_equal:'.$endDate],
            'end_date' => ['required', 'date_format:"Y-m-d"', 'after_or_equal:'.$startDate],
        ];
    }
}
