<?php

namespace App\Http\Requests\Admin\Broker;

class UpdateRequest extends CreateRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['email'] = $rules['email'].','.$this->route('broker')->getKey();
        $w9DocumentRequired = is_null($this->route('broker')->w9_document) ? 'required|' : '';

        $rules['w9_document'] = $w9DocumentRequired.'file|mimes:'.config('project.media.broker_document.allowed_mimes').
            '|max:'.config('project.media.broker_document.max_size');

        return $rules;
    }
}
