<?php

namespace App\Http\Requests\Admin\Broker;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class DeleteRequest extends FormRequest
{
    /**
     * @var string
     */
    protected $errorBag = 'delete';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     *
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $broker = $this->route('broker');

            $usageCount = $broker->users()->count();
            if ($usageCount) {
                $validator->errors()->add(
                    'broker',
                    'Could not delete the broker because it is associated with '.$usageCount.' '.Str::plural(
                        'realtor',
                        $usageCount
                    ).'.'
                );
            }
        });
    }
}
