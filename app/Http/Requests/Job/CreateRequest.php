<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cities' => 'bail|required|array',
            'cities.*' => 'bail|integer|distinct|exists:cities,id',

            'description' => 'nullable|max:1000',
            'due_date' => ['required', 'date_format:"m/d/Y"', 'after:'.now()->subDays(1)->toDateString()],
            'incentive_amount' => 'integer|min:5|max:999',
            'number_of_homes' => [
                'nullable',
                'integer',
                'min:1',
                'max:'.config('project.job.max_number_of_homes'),
                Rule::requiredIf(function () {
                    return $this->route('service')->requires_number_of_homes;
                }),
            ],
        ];
    }
}
