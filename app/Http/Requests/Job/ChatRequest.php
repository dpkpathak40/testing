<?php

namespace App\Http\Requests\Job;

use Illuminate\Foundation\Http\FormRequest;

class ChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required_without:attachment|nullable|string|max:255',
            'attachment' => 'nullable|file|mimes:'.config('project.media.chat_document.allowed_mimes').
                '|max:'.config('project.media.chat_document.max_size'),
        ];
    }
}
