<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutBlockedUser
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() &&
            Auth::guard($guard)->user()->is_blocked) {
            Auth::guard($guard)->logout();
            $request->session()->invalidate();

            $message = 'You no longer have access to your account.';
            alert()->error($message);
            throw new AuthenticationException($message);
        }

        return $next($request);
    }
}
