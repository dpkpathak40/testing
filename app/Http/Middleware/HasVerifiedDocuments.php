<?php

namespace App\Http\Middleware;

use App\Models\UserDocument;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HasVerifiedDocuments
{
    public function handle(Request $request, Closure $next)
    {
        if (! Auth::user()->documentsStatusIs(UserDocument::STATUS_VERIFIED)) {
            $message = 'Your documents are not verified yet.';

            if ($request->expectsJson()) {
                throw new \Illuminate\Auth\Access\AuthorizationException($message);
            }

            alert()->error($message);

            return redirect()->route('documents.create');
        }

        return $next($request);
    }
}
