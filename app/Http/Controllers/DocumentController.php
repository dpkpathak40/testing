<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentsUploadRequest;
use App\Mail\Admin\DocumentsUploadedMail as DocumentsUploadedForAdmin;
use App\Models\Broker;
use App\Models\UserDocument;
use App\Notifications\DocumentsUploaded;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DocumentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brokers = Broker::whereNotNull('stripe_user_id')->get();
        $documentTypes = UserDocument::$types;

        return view('documents.index', compact('brokers', 'documentTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentsUploadRequest  $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(DocumentsUploadRequest $request)
    {
        DB::beginTransaction();

        $user = $request->user();

        // We don't keep history
        $user->documents()->delete();

        foreach (UserDocument::$types as $slug => $label) {
            $userDocument = new UserDocument([
                'file' => $request->file($slug),
                'name' => $slug,
            ]);
            $userDocument->user()->associate($user);
            $userDocument->save();
        }

        $user->broker()->associate($request->input('broker'));
        $user->setDocumentsStatusTo(UserDocument::STATUS_PENDING);
        $user->save();

        DB::commit();

        // Notify admin and user
        Mail::to(config('project.admin_email_address'))->send(new DocumentsUploadedForAdmin($user));
        $user->notify(new DocumentsUploaded());

        alert()->success('Your documents uploaded successfully.');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  UserDocument  $userDocument
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function show(UserDocument $userDocument)
    {
        return $userDocument->download();
    }
}
