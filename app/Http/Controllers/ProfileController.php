<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->loadMissing([
            'profile', 'address', 'services', 'cities', 'broker',
        ]);
        $user->append('rating');

        $accomplishments = Job::with('service')
            ->selectRaw('count(*) as total, service_id')
            ->groupBy('service_id')
            ->where('assignee_user_id', $user->getKey())
            ->where('status', Job::STATUS_COMPLETED)
            ->orderBy('total', 'desc')
            ->get();

        return view('profile', compact('user', 'accomplishments'));
    }
}
