<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BandwidthWebhookController extends Controller
{
    public function __invoke(Request $request)
    {
        // We are not processing the payload for now
        return response()->json(null, 202);
    }
}
