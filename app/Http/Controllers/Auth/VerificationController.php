<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\EmailVerificationMail;
use App\Models\User;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class VerificationController extends Controller
{
    use RedirectsUsers;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResendForm()
    {
        return view('auth.verification.resend');
    }

    /**
     * Check the incoming verification request.
     *
     * @param  Request  $request
     * @param  User  $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request, User $user)
    {
        if (! $request->hasValidSignature()) {
            alert()->error('Invalid verification link. Please request a new one.');

            return redirect($this->redirectPath());
        }

        if ($user->has_verified_email) {
            alert()->info('Your email address is already verified.');
        } else {
            $user->setEmailAsVerified();
            $user->save();
            alert()->success('Your email address verified successfully.');
        }

        return redirect($this->redirectPath());
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function resend(Request $request)
    {
        $this->validate($request, [
            'email' => [
                'bail', 'required', 'string', 'email', Rule::exists('users')->where(function ($query) {
                    $query->whereNull('email_verified_at');
                }),
            ],
        ], [
            'email.exists' => 'We can\'t find such user in our records.',
        ]);

        $user = User::where('email', $request->get('email'))->first();

        Mail::to($user)->send(new EmailVerificationMail($user));
        alert()->success('Verification email resent successfully.');

        return redirect($this->redirectPath());
    }

    /**
     * Where to redirect users after successful operation.
     *
     * @return string
     */
    private function redirectTo()
    {
        return route('login');
    }
}
