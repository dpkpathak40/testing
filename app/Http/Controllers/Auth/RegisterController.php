<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\ActiveCampaign\SyncAgentJob;
use App\Mail\EmailVerificationMail;
use App\Models\User;
use App\Models\UserProfile;
use App\Rules\PersonNameRule;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'min:3', 'max:255', new PersonNameRule()],
            'last_name' => ['nullable', 'string', 'max:255', new PersonNameRule()],
            'email' => ['required', 'string', 'email:strict', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'terms_and_condition' => ['accepted'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     *
     * @return \App\Models\User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        DB::beginTransaction();

        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $profile = new UserProfile(\Illuminate\Support\Arr::only($data, ['first_name', 'last_name']));
        $profile->user()->associate($user);
        $profile->save();
        // Make sure that the default preference persist
        $user->preference->save();

        DB::commit();

        $this->dispatch(new SyncAgentJob($user));

        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     *
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        Mail::to($user)->send(new EmailVerificationMail($user));

        alert()->success('We have sent you an email verification link to your email address.');

        return redirect($this->redirectPath());
    }

    /**
     * Where to redirect users after registration.
     *
     * @return string
     */
    private function redirectTo()
    {
        return route('login');
    }
}
