<?php

namespace App\Http\Controllers\Admin\Job;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\Refund;
use App\Notifications\JobCancelled;
use App\Notifications\RefundOnJobCancelled;
use App\Support\Money;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class ActionController extends Controller
{
    public function cancel(Request $request, Job $job)
    {
        $this->validate($request, [
            'cancellation_reason' => ['required', 'string', 'min:3', 'max:1000'],
        ]);

        DB::beginTransaction();

        $refund = $this->processRefund($job);

        $job->setStatusTo(Job::STATUS_CANCELLED);
        $job->fill([
            'cancelled_at' => Date::now(),
            'cancellation_reason' => $request->input('cancellation_reason'),
        ]);
        $job->save();

        DB::commit();

        $job->assignee->notify(new JobCancelled($job));
        $job->creator->notify(new RefundOnJobCancelled($job, $refund));

        alert()->success('Job was cancelled.');

        return redirect()->route('admin.jobs.index');
    }

    protected function processRefund(Job $job): Refund
    {
        $charge = $job->charge;

        $response = \Stripe\Refund::create([
            'charge' => $charge->transaction_id,
            'reason' => 'requested_by_customer',
            'metadata' => [
                'jobId' => $job->getKey(),
            ],
        ]);

        $refund = new Refund();
        $refund->charge()->associate($charge);
        $refund->fill([
            'transaction_id' => $response->id,
            'total_amount' => Money::centsToDollars($response->amount),
        ]);
        $refund->save();

        return $refund;
    }
}
