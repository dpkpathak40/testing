<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DocumentsStatusUpdateRequest;
use App\Models\City;
use App\Models\Job;
use App\Models\User;
use App\Models\UserDocument;
use App\Notifications\DocumentsRejected;
use App\Notifications\DocumentsVerified;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = User::with(['profile', 'broker']);

        if ($request->filled('search')) {
            $users->where('email', 'like', '%'.$request->input('search').'%');

            $users->orWhereHas('profile', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('search').'%')
                    ->orWhere('last_name', 'like', '%'.$request->input('search').'%');
            });
        }

        if ($request->filled('email_status')) {
            if ($request->input('email_status') === 'confirmed') {
                $users->whereNotNull('email_verified_at');
            }
            if ($request->input('email_status') === 'not_confirmed') {
                $users->whereNull('email_verified_at');
            }
        }

        if ($request->filled('documents_status')) {
            $users->where('documents_status', $request->input('documents_status'));
        }

        if ($request->filled('city_id')) {
            $users->whereHas('cities', function ($query) use ($request) {
                $query->where('cities.id', $request->input('city_id'));
            });
        }

        $users = $users->latest()
            ->paginate($request->input('per_page', 10));

        $cities = City::orderBy('name')->get();

        return view('admin.users.index', compact('users', 'cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->loadMissing(['broker', 'profile', 'documents']);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DocumentsStatusUpdateRequest $request
     * @param  User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentsStatusUpdateRequest $request, User $user)
    {
        $user->setDocumentsStatusTo($request->input('documents_status'));
        $user->save();

        if ($request->input('documents_status') === UserDocument::STATUS_REJECTED) {
            $user->notify(new DocumentsRejected($request->input('reject_reason')));
        }

        if ($request->input('documents_status') === UserDocument::STATUS_VERIFIED) {
            $user->notify(new DocumentsVerified());
        }

        alert()->success('User updated successfully.');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $user->loadMissing([
            'profile', 'address', 'services', 'cities', 'broker',
        ]);
        $user->append('rating');

        $accomplishments = Job::with('service')
            ->selectRaw('count(*) as total, service_id')
            ->groupBy('service_id')
            ->where('assignee_user_id', $user->getKey())
            ->where('status', Job::STATUS_COMPLETED)
            ->orderBy('total', 'desc')
            ->get();

        return view('admin.users.show', compact('user', 'accomplishments'));
    }

    /**
     * Download user document.
     *
     * @param  int $user
     * @param  int $document
     *
     * @return mixed
     */
    public function downloadDocument($user, $document)
    {
        $userDocument = UserDocument::where('user_id', $user)->findOrFail($document);

        return $userDocument->download();
    }
}
