<?php

namespace App\Http\Controllers\Admin\Broker;

use App\Http\Controllers\Controller;
use App\Mail\StripeConnectInvite;
use App\Models\Broker;
use Illuminate\Support\Facades\Mail;

class BrokerActionController extends Controller
{
    /**
     * Send Stripe Connect email to broker.
     *
     * @param  Broker  $broker
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendStripeInvite(Broker $broker)
    {
        Mail::to($broker)->send(new StripeConnectInvite($broker));

        alert()->success('Stripe connect invitation email sent to broker.');

        return back();
    }
}
