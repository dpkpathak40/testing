<?php

namespace App\Http\Controllers\Admin\Broker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Broker\CreateRequest;
use App\Http\Requests\Admin\Broker\DeleteRequest;
use App\Http\Requests\Admin\Broker\ReportRequest;
use App\Http\Requests\Admin\Broker\UpdateRequest;
use App\Jobs\ActiveCampaign\SyncBrokerJob;
use App\Mail\Admin\BrokerTransactionsReportMail;
use App\Models\Broker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Mail;

class BrokerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $brokers = Broker::withCount('users');

        if ($request->filled('search')) {
            $brokers->where('first_name', 'like', '%'.$request->input('search').'%')
                ->orWhere('last_name', 'like', '%'.$request->input('search').'%')
                ->orWhere('email', 'like', '%'.$request->input('search').'%');
        }

        $brokers = $brokers->latest()
            ->paginate($request->input('per_page', 10));

        return view('admin.brokers.index', compact('brokers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brokers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $broker = new Broker();
        $broker->fill($request->only([
            'email', 'first_name', 'last_name', 'area_code', 'phone', 'w9_document', 'owner_name', 'company_name',
        ]));
        $broker->save();

        $this->dispatch(new SyncBrokerJob($broker));

        alert()->success('Broker created successfully.');

        return redirect()->route('admin.brokers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Broker  $broker
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Broker $broker)
    {
        return view('admin.brokers.edit', compact('broker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Broker  $broker
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Broker $broker)
    {
        $broker->fill($request->only([
            'email', 'first_name', 'last_name', 'area_code', 'phone', 'w9_document', 'owner_name', 'company_name',
        ]));
        $broker->update();

        $this->dispatch(new SyncBrokerJob($broker));

        alert()->success('Broker updated successfully.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteRequest  $request
     * @param  Broker  $broker
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(DeleteRequest $request, Broker $broker)
    {
        $broker->delete();

        alert()->success('Broker deleted successfully.');

        return redirect()->route('admin.brokers.index');
    }

    /**
     * Download the broker's document.
     *
     * @param  Broker  $broker
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadW9Document(Broker $broker)
    {
        return $broker->downloadW9Document();
    }

    /**
     * Generate broker report.
     *
     * @param  ReportRequest  $request
     * @param  Broker  $broker
     * @return \Illuminate\Http\RedirectResponse
     */
    public function report(ReportRequest $request, Broker $broker)
    {
        $startDate = Date::create($request->get('start_date'))->startOfDay();
        $endDate = Date::create($request->get('end_date'))->endOfDay();

        Mail::to($broker)->send(new BrokerTransactionsReportMail($broker, $startDate, $endDate));
        alert()->success('Report email sent successfully.');

        return back();
    }
}
