<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\CreateRequest;
use App\Http\Requests\Admin\Service\DeleteRequest;
use App\Http\Requests\Admin\Service\UpdateRequest;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $services = Service::query();

        if ($request->filled('search')) {
            $services->where('name', 'like', '%'.$request->input('search').'%')
                ->orWhere('description', 'like', '%'.$request->input('search').'%');
        }

        $services = $services->latest()
            ->paginate($request->input('per_page', 10));

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $service = new Service();
        $service->fill($request->only([
            'name', 'description', 'amount', 'image', 'requires_number_of_homes', 'active',
        ]));
        $service->save();

        alert()->success('Service created successfully.');

        return redirect()->route('admin.services.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  \App\Models\Service  $service
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Service $service)
    {
        $service->fill($request->only([
            'name', 'description', 'amount', 'image',
        ]));
        $service->fill([
            'requires_number_of_homes' => $request->filled('requires_number_of_homes'),
            'active' => $request->filled('active'),
        ]);
        $service->save();

        alert()->success('Service updated successfully.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteRequest  $request
     * @param  Service  $service
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(DeleteRequest $request, Service $service)
    {
        $service->delete();

        alert()->success('Service deleted successfully.');

        return redirect()->route('admin.services.index');
    }
}
