<?php

namespace App\Http\Controllers\Admin\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Support\Facades\Password;

class ActionController extends Controller
{
    /**
     * Send password reset email to user.
     *
     * @param  \App\Models\Admin  $admin
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendPasswordResetEmail(Admin $admin)
    {
        Password::broker('admins')->sendResetLink([
            'email' => $admin->getEmailForPasswordReset(),
        ]);

        alert()->success('Password reset email was sent successfully.');

        return back();
    }

    /**
     * Toggle user's account locked status.
     *
     * @param  Admin  $admin
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleBlockedStatus(Admin $admin)
    {
        $admin->setBlockedStatusTo($admin->is_blocked ? null : now());
        $admin->save();

        alert()->success('User status was changed successfully.');

        return back();
    }
}
