<?php

namespace App\Http\Controllers\Admin\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\CreateRequest;
use App\Http\Requests\Admin\Admin\UpdateRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $admins = Admin::query()
            ->where('id', '!=', $request->user('admin')->getKey())
            ->when($request->filled('search'), function ($query) use ($request) {
                $query->where('email', 'like', '%'.$request->input('search').'%');
            });

        $admins = $admins->latest()->paginate($request->input('per_page', 10));

        return view('admin.admins.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $admin = new Admin();
        $admin->fill($request->only([
            'email',
        ]));
        $admin->password = bcrypt(\Illuminate\Support\Str::random(30));
        $admin->save();

        Password::broker('admins')->sendResetLink([
            'email' => $admin->getEmailForPasswordReset(),
        ]);

        alert()->success('Admin user created successfully.');

        return redirect()->route('admin.admins.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Admin  $admin
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.admins.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Admin  $admin
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Admin $admin)
    {
        $admin->fill($request->only([
            'email',
        ]));

        // Reset password for new email address
        if ($admin->isDirty('email')) {
            $admin->setAttribute('password', Hash::make(Str::random(30)));
            $admin->setRememberToken(Str::random(60));
        }
        $admin->update();

        if ($admin->wasChanged('email')) {
            Password::broker('admins')->sendResetLink([
                'email' => $admin->getEmailForPasswordReset(),
            ]);
        }

        alert()->success('Admin user updated successfully.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Admin  $admin
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();

        alert()->success('Admin user deleted successfully.');

        return redirect()->route('admin.admins.index');
    }
}
