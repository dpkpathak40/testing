<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Display a listing of the jobs.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $cities = City::all();

        $jobs = Job::query()
            ->with(['service', 'creator.profile', 'assignee.profile', 'charge', 'transfer'])
            ->orderByDesc('updated_at');

        if ($request->filled('search')) {
            $jobs->whereHas('service', function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->input('search').'%');
            });

            $jobs->orWhereHas('creator.profile', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('search').'%')
                    ->orWhere('last_name', 'like', '%'.$request->input('search').'%');
            });

            $jobs->orWhereHas('assignee.profile', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('search').'%')
                    ->orWhere('last_name', 'like', '%'.$request->input('search').'%');
            });
        }

        if ($request->filled('job_status')) {
            $jobs->where('status', $request->input('job_status'));
        }

        if ($request->filled('city_id')) {
            $jobs->whereHas('cities', function ($query) use ($request) {
                $query->where('cities.id', $request->input('city_id'));
            });
        }

        $jobs = $jobs->latest()
            ->paginate($request->input('per_page', 10));

        return view('admin.jobs.index', compact('jobs', 'cities'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job $job
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Job $job)
    {
        $job->loadMissing(['creator.profile', 'cities']);

        if ($job->assignee_user_id) {
            $job->loadMissing(['assignee.profile']);
        } else {
            $job->loadMissing(['applicants.profile', 'applicants.assessments']);
        }

        return view('admin.jobs.show', compact('job'));
    }
}
