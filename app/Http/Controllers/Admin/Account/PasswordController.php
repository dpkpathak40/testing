<?php

namespace App\Http\Controllers\Admin\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Account\PasswordUpdateRequest;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('admin.account.password', [
            'user' => Auth::guard('admin')->user(),
        ]);
    }

    /**
     * Update the user password in storage.
     *
     * @param  PasswordUpdateRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordUpdateRequest $request)
    {
        $user = Auth::guard('admin')->user();
        $user->password = bcrypt($request->input('password'));
        $user->setRememberToken(\Illuminate\Support\Str::random(60));
        $user->save();

        alert()->success('Your password was updated successfully.');

        return back();
    }
}
