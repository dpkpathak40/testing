<?php

namespace App\Http\Controllers\Admin\Account;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ActionController extends Controller
{
    /**
     * Send password reset email to user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendPasswordResetEmail()
    {
        Password::broker('admins')->sendResetLink([
            'email' => Auth::guard('admin')->user()->getEmailForPasswordReset(),
        ]);

        alert()->success('Password reset email was sent successfully.');

        return back();
    }
}
