<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Charge;
use App\Models\Job;
use App\Models\User;

class DashboardController extends Controller
{
    public function __invoke()
    {
        $counts = new \stdClass();

        $counts->users = User::count();
        $counts->jobs = Job::count();
        $counts->charged = Charge::sum('total_amount');

        return view('admin.dashboard', compact('counts'));
    }
}
