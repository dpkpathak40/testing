<?php

namespace App\Http\Controllers;

use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Get all notifications.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $notifications = Auth::user()->notifications()
            ->orderBy('read_at')
            ->paginate(50);

        $notifications->transform(function ($item) {
            $item['created_at_tz'] = $item->created_at->toIso8601String();

            return $item;
        });

        return response()->json($notifications);
    }

    /**
     * Mark all notification as read.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAllAsRead()
    {
        $result = Auth::user()->unreadNotifications()
            ->update(['read_at' => now()]);

        return response()->json($result);
    }

    /**
     * Mark all notification as unread.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAllAsUnread()
    {
        $result = Auth::user()->readNotifications()
            ->update(['read_at' => null]);

        return response()->json($result);
    }

    /**
     * Mark specific notification as read.
     *
     * @param  DatabaseNotification  $notification
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsRead(DatabaseNotification $notification)
    {
        $result = Auth::user()->unreadNotifications()
            ->where('id', $notification->getKey())
            ->update(['read_at' => now()]);

        return response()->json($result);
    }

    /**
     * Mark specific notification as unread.
     *
     * @param  DatabaseNotification  $notification
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsUnread(DatabaseNotification $notification)
    {
        $result = Auth::user()->readNotifications()
            ->where('id', $notification->getKey())
            ->update(['read_at' => null]);

        return response()->json($result);
    }

    /**
     * Delete all notifications.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll()
    {
        Auth::user()->notifications()->delete();

        return response()->json();
    }
}
