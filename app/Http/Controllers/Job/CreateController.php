<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\CreateRequest;
use App\Jobs\JobCreated;
use App\Jobs\JobIncentiveUpdated;
use App\Models\City;
use App\Models\Job;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreateController extends Controller
{
    /**
     * Show list of available services.
     *
     * @return \Illuminate\Http\Response
     */
    public function services()
    {
        $services = Service::active()->orderBy('name')->get();

        return view('jobs.services', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Service $service
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Service $service)
    {
        $cities = City::orderBy('name')->get();

        return view('jobs.create', compact('service', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Service $service
     * @param CreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(CreateRequest $request, Service $service)
    {
        DB::beginTransaction();

        $job = new Job();
        $job->fill($request->only('description', 'due_date', 'number_of_homes'));

        $chargeableAmount = $service->chargeableAmount($request->input('number_of_homes'));
        $job->fill([
            'chargeable_amount' => $chargeableAmount,
            'transferable_amount' => (config('project.transferable_percentage') / 100) * $chargeableAmount,
        ]);
        $incentiveAmount = $request->input('incentive_amount');
        $job->fill([
            'chargeable_incentive_amount' => $incentiveAmount + (config('project.incentive_percentage') / 100) * $incentiveAmount,
            'transferable_incentive_amount' => $incentiveAmount,
        ]);

        $job->service()->associate($service);
        $job->creator()->associate($request->user());
        $job->save();

        $job->cities()->attach($request->input('cities'));

        DB::commit();

        $this->dispatch(new JobCreated($job));

        alert()->success('Job created successfully.');

        return response()->json($job);
    }

    /**
     * Calculate job amount based on number of homes.
     *
     * @param \Illuminate\Http\Request $request
     * @param Service $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAmount(Request $request, Service $service)
    {
        $this->validate($request, [
            'number_of_homes' => 'required|integer|min:1|max:'.config('project.job.max_number_of_homes'),
        ]);
        $amount = $service->chargeableAmount($request->input('number_of_homes'));

        return response()->json(compact('amount'));
    }

    public function updateIncentive(Request $request, Job $job)
    {
        $this->validate($request, [
            'incentive_amount' => 'integer|min:5|max:999',
        ]);

        $incentiveAmount = $request->input('incentive_amount');
        $job->fill([
            'chargeable_incentive_amount' => $incentiveAmount + (config('project.incentive_percentage') / 100) * $incentiveAmount,
            'transferable_incentive_amount' => $incentiveAmount,
        ]);
        $job->save();

        $this->dispatch(new JobIncentiveUpdated($job));

        alert()->success('Job was updated successfully');

        return response()->noContent();
    }
}
