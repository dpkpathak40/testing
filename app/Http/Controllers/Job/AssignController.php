<?php

namespace App\Http\Controllers\Job;

use Ankurk91\StripeExceptions\ApiException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Job\AssignRequest;
use App\Models\Charge;
use App\Models\Job;
use App\Models\User;
use App\Notifications\JobAssigned;
use App\Notifications\JobNotAssigned;
use App\Support\Money;
use Illuminate\Support\Facades\DB;

class AssignController extends Controller
{
    /**
     * Show assignment form.
     *
     * @param Job $job
     * @param User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAssignForm(Job $job, User $user)
    {
        $user->loadMissing(['profile']);

        return view('jobs.assign', compact('job', 'user'));
    }

    /**
     * Handle incoming request.
     *
     * @param AssignRequest $request
     * @param Job $job
     * @param User $user Assignee
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function assign(AssignRequest $request, Job $job, User $user)
    {
        $authUser = $request->user();
        $total_amount = ($job->chargeable_incentive_amount) ? ($job->chargeable_amount + $job->chargeable_incentive_amount) : $job->chargeable_amount;

        try {
            // Create a Customer if not exists
            if (! $authUser->stripe_customer_id) {
                $customer = \Stripe\Customer::create([
                    'email' => $authUser->email,
                    'description' => $authUser->profile->full_name,
                    'metadata' => [
                        'userId' => $authUser->getKey(),
                    ],
                ]);
                $authUser->fill([
                    'stripe_customer_id' => $customer->id,
                ]);
                $authUser->save();
            }

            // todo prevent duplicate cards
            // Attach the card to customer, without modifying default
            if ($request->input('save_card', false)) {
                \Stripe\Customer::createSource($authUser->stripe_customer_id, [
                    'source' => $request->input('source'),
                ]);
            }

            // Charge customer with the recent source
            $response = \Stripe\Charge::create([
                'source' => $request->input('source'),
                'customer' => $authUser->stripe_customer_id,
                'amount' => Money::dollarsToCents($total_amount),
                'currency' => 'usd',
                'description' => 'Job assigned.',
                'metadata' => [
                    'jobId' => $job->getKey(),
                    'assigneeId' => $user->getKey(),
                ],
            ]);
        } catch (\Throwable $e) {
            throw new ApiException($e);
        }

        DB::beginTransaction();

        $charge = new Charge();
        $charge->fill([
            'total_amount' => $total_amount,
            'transaction_id' => $response->id,
        ]);
        $charge->job()->associate($job);
        $charge->save();

        $job->assignee()->associate($user);
        $job->save();

        DB::commit();

        $user->notify(new JobAssigned($job));

        $this->dispatch(new \App\Jobs\JobNotAssigned($job, $user));

        alert()->success('Job assigned to realtor '.e($user->profile->full_name).' successfully.');

        return response()->json($job);
    }
}
