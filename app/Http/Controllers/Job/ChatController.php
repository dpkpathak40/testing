<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\ChatRequest;
use App\Models\Job;
use App\Models\JobChat;
use Illuminate\Support\Facades\Date;

class ChatController extends Controller
{
    /**
     * Get chats for a job.
     *
     * @param  Job  $job
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Job $job)
    {
        $chats = $job->chat()->orderByDesc('created_at')->paginate(10);

        $this->markReadBeforeSend($chats);

        $chats->load(['user', 'user.profile']);

        return response()->json($chats);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ChatRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(ChatRequest $request, Job $job)
    {
        $chat = JobChat::create(
            [
                'job_id' => $job->getKey(),
                'sender_id' => $request->user()->getKey(),
                'message' => $request->input('message'),
                'attachment' => $request->file('attachment'),
            ]
        );

        if ($job->creator->is($request->user())) {
            $job->assignee->notify(new \App\Notifications\JobChatReceived($job, $job->creator));
        } else {
            $job->creator->notify(new \App\Notifications\JobChatReceived($job, $job->assignee));
        }

        $chat->load(['user.profile']);

        return response()->json($chat);
    }

    /**
     * @param  Job  $job
     *
     * @param  JobChat  $jobChat
     *
     * @return mixed
     */
    public function downloadAttachment(Job $job, JobChat $jobChat)
    {
        return $jobChat->downloadAttachmentFile();
    }

    public function markReadBeforeSend($chats)
    {
        $unread = array_filter($chats->items(), function ($item) {
            return is_null($item['read_at']);
        });

        if (! count($unread)) {
            return false;
        }

        return JobChat::query()
            ->where('sender_id', '<>', auth()->user()->getKey())
            ->whereIn('id', array_column($unread, 'id'))
            ->update(['read_at' => Date::now()]);
    }
}
