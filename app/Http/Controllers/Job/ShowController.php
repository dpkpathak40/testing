<?php

namespace App\Http\Controllers\Job;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\JobChat;

class ShowController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        $job->loadMissing(['service', 'cities', 'creator', 'chat', 'chat.user'])
            ->loadCount([
                'chat' => function ($query) {
                    return $query->where('sender_id', '<>', auth()->user()->getKey())
                        ->whereNull('read_at');
                },
            ]);
        $applicants = [];

        if ($job->assignee_user_id) {
            $job->loadMissing(['assignee', 'assignee.profile', 'assignee.address']);
        } else {
            $job->loadMissing(['applicants', 'applicants.cities', 'applicants.assessments']);
            $job->applicants->each(function ($user) {
                $user->append('rating');
                $user->makeHidden('assessments');
            });
            $applicants = $job->applicants;
            unset($job->applicants);
        }

        return view('jobs.show', compact('job', 'applicants'));
    }
}
