<?php

namespace App\Http\Controllers\Job;

use Ankurk91\StripeExceptions\ApiException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Job\AcceptRequest;
use App\Http\Requests\Job\CompletedRequest;
use App\Http\Requests\Job\DeleteRequest;
use App\Http\Requests\Job\DoneRequest;
use App\Http\Requests\Job\IgnoreRequest;
use App\Models\Job;
use App\Models\Transfer;
use App\Models\UserJobAssessment;
use App\Notifications\JobAccepted;
use App\Notifications\JobCompletedForAssignee;
use App\Notifications\JobCompletedForCreator;
use App\Notifications\JobDone;
use App\Support\Money;
use Illuminate\Support\Facades\DB;

class ActionController extends Controller
{
    /**
     * Handle incoming request.
     *
     * @param  DeleteRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function delete(DeleteRequest $request, Job $job)
    {
        $job->delete();

        return response()->noContent();
    }

    /**
     * Handle incoming request.
     *
     * @param  AcceptRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     */
    public function accept(AcceptRequest $request, Job $job)
    {
        $job->applicants()->attach($request->user()->getKey());

        $job->creator->notify(new JobAccepted($job, $request->user()));

        return response()->json($job);
    }

    /**
     * Handle incoming request.
     *
     * @param  IgnoreRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     */
    public function ignore(IgnoreRequest $request, Job $job)
    {
        $job->omitters()->attach($request->user()->getKey());

        return response()->json($job);
    }

    /**
     * Handle incoming request.
     *
     * @param  DoneRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     */
    public function markAsDone(DoneRequest $request, Job $job)
    {
        $job->setStatusTo(Job::STATUS_DONE);
        $job->save();

        $job->creator->notify(new JobDone($job));

        return response()->json($job);
    }

    /**
     * Handle incoming request.
     *
     * @param  CompletedRequest  $request
     * @param  Job  $job
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function markAsCompleted(CompletedRequest $request, Job $job)
    {
        DB::beginTransaction();

        $broker = $job->assignee->broker;
        $totalAmount = ($job->transferable_incentive_amount) ?
            ($job->transferable_amount + $job->transferable_incentive_amount)
            : $job->transferable_amount;

        try {
            $response = \Stripe\Transfer::create([
                'source_transaction' => $job->charge->transaction_id,
                'destination' => $broker->stripe_user_id,
                'amount' => Money::dollarsToCents($totalAmount),
                'currency' => 'usd',
                'description' => 'Job completed',
                'metadata' => [
                    'jobId' => $job->getKey(),
                    'assigneeId' => $job->assignee->getKey(),
                    'brokerId' => $broker->getKey(),
                ],
            ]);
        } catch (\Throwable $e) {
            throw new ApiException($e);
        }

        $transfer = new Transfer();
        $transfer->fill([
            'total_amount' => $totalAmount,
            'transaction_id' => $response->id,
        ]);
        $transfer->job()->associate($job);
        $transfer->save();

        $job->setStatusTo(Job::STATUS_COMPLETED);
        $job->save();

        $assessment = new UserJobAssessment();
        $assessment->fill($request->only('rating'));
        $assessment->user()->associate($job->assignee);
        $assessment->job()->associate($job);
        $assessment->save();

        DB::commit();

        $job->creator->notify(new JobCompletedForCreator($job));
        $job->assignee->notify(new JobCompletedForAssignee($job));

        return response()->json($job);
    }
}
