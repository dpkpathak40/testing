<?php

namespace App\Http\Controllers;

use App\Models\Job;

class ReceivedJobController extends Controller
{
    /**
     * Show listing of received jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function received()
    {
        $receivedJobs = Job::with(['creator', 'service', 'cities'])
            ->receivedBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('receivedJobs.received', compact('receivedJobs'));
    }

    /**
     * Show listing of accepted jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function accepted()
    {
        $acceptedJobs = Job::with(['creator', 'service', 'cities'])
            ->acceptedBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('receivedJobs.accepted', compact('acceptedJobs'));
    }

    /**
     * Show listing of to-do jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toDo()
    {
        $toDoJobs = Job::with(['service', 'cities', 'creator.address', 'assignee', 'chat', 'chat.user'])
            ->withCount([
                'chat' => function ($query) {
                    return $query->where('sender_id', '<>', auth()->user()->getKey())->whereNull('read_at');
                },
            ])
            ->toDoFor(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('receivedJobs.toDo', compact('toDoJobs'));
    }

    /**
     * Show listing of completed jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function completed()
    {
        $completedJobs = Job::with(['creator', 'service', 'cities'])
            ->completedFor(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('receivedJobs.completed', compact('completedJobs'));
    }
}
