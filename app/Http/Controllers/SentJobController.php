<?php

namespace App\Http\Controllers;

use App\Models\Job;

class SentJobController extends Controller
{
    /**
     * Show listing of sent jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sent()
    {
        $sentJobs = Job::with(['service', 'cities'])
            ->withCount(['applicants'])
            ->sentBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('sentJobs.sent', compact('sentJobs'));
    }

    /**
     * Show listing of assigned jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assigned()
    {
        $assignedJobs = Job::with(['cities', 'service', 'assignee'])
            ->assignedBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('sentJobs.assigned', compact('assignedJobs'));
    }

    /**
     * Show listing of for-review jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function forReview()
    {
        $forReviewJobs = Job::with(['service', 'cities', 'assignee'])
            ->toReviewBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('sentJobs.forReview', compact('forReviewJobs'));
    }

    /**
     * Show listing of completed jobs.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function completed()
    {
        $completedJobs = Job::with(['service', 'cities', 'assignee'])
            ->completedBy(auth()->user())
            ->latest()
            ->simplePaginate();

        return view('sentJobs.completed', compact('completedJobs'));
    }
}
