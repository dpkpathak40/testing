<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\User;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $user = auth()->user();
        $user->loadMissing(['cities', 'services']);

        $receivedJobs = Job::with(['creator', 'service', 'cities'])
            ->receivedBy($user)
            ->latest()->limit(2)->get();

        $sentJobs = Job::with(['service', 'cities'])
            ->withCount('applicants')
            ->sentBy($user)
            ->latest()->limit(2)->get();

        $upcomingJobs = Job::with(['service', 'cities', 'creator.address'])
            ->upcomingFor($user)
            ->latest('due_date')->limit(1)->get();

        $counts = $this->counts($user);

        return view('dashboard', compact('receivedJobs', 'sentJobs', 'upcomingJobs', 'counts'));
    }

    protected function counts(User $user)
    {
        $counts = new \stdClass();

        $counts->sent_jobs = Job::sentBy($user)->count();
        $counts->assigned_jobs = Job::assignedBy($user)->count();

        $counts->received_jobs = Job::receivedBy($user)->count();
        $counts->accepted_jobs = Job::acceptedBy($user)->count();

        return $counts;
    }
}
