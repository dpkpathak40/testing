<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\ProfileUpdateRequest;
use App\Jobs\ActiveCampaign\SyncAgentJob;
use App\Models\City;
use App\Models\Service;
use App\Models\UserAddress;
use App\Models\UserPreference;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        $user->loadMissing([
            'profile', 'address', 'services', 'cities', 'preference',
        ]);

        $services = Service::orderBy('name')->get();
        $cities = City::orderBy('name')->get();

        return view('account.profile', compact('user', 'cities', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProfileUpdateRequest  $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(ProfileUpdateRequest $request)
    {
        $profile = UserProfile::updateOrCreate(
            ['user_id' => $request->user()->getKey()],
            $request->validated()
        );

        UserAddress::updateOrCreate(
            ['user_id' => $request->user()->getKey()],
            $request->validated()
        );

        $request->user()->cities()->sync($request->input('cities'));
        $request->user()->services()->sync($request->input('services'));

        UserPreference::updateOrCreate(
            ['user_id' => $request->user()->getKey()],
            [
                'email_notifications' => $request->boolean('email_notifications'),
                'sms_notifications' => $request->boolean('sms_notifications'),
            ]
        );

        $this->dispatch(new SyncAgentJob($request->user()));

        return response()->json($profile);
    }
}
