<?php

namespace App\Http\Controllers;

use App\Models\Broker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Stripe\StripeClient;

class StripeConnectController extends Controller
{
    protected StripeClient $stripe;

    public function __construct(StripeClient $stripe)
    {
        $this->stripe = $stripe;
    }

    public function callback(Request $request)
    {
        // When user denied the connect request
        if ($request->filled('error')) {
            return $this->redirectWith('error', 'Stripe sent an error. Please try again.');
        }

        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'state' => 'required|integer',
        ]);

        // When returned url does not have required query parameters
        if ($validator->fails()) {
            return $this->redirectWith('error', 'Stripe Connect redirect url found malformed.');
        }

        try {
            $response = $this->stripe->oauth->token([
                'grant_type' => 'authorization_code',
                'code' => $request->input('code'),
            ]);
        } catch (\Throwable $exception) {
            throw new \Ankurk91\StripeExceptions\OAuthException($exception, $this->statusRoute());
        }

        $account = $this->stripe->accounts->retrieve($response->stripe_user_id);
        if ($account->country !== 'US') {
            return $this->redirectWith('error', 'Your Stripe account country must be United States.');
        }

        $broker = Broker::query()
            ->whereNull('stripe_user_id')
            ->findOrFail($request->input('state'));

        $broker->fill([
            'stripe_user_id' => $response->stripe_user_id,
        ]);
        $broker->save();

        return $this->redirectWith('status', 'Stripe Connect was successful.');
    }

    protected function statusRoute()
    {
        return route('stripe.status');
    }

    protected function redirectWith(string $type, string $message)
    {
        return redirect($this->statusRoute())->with($type, $message);
    }

    public function status()
    {
        return view('stripeConnectStatus');
    }
}
