<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class JobCancelled extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;

    public function __construct(Job $job)
    {
        $this->job = $job->withoutRelations();
    }

    public function via($notifiable)
    {
        return ['mail', 'database', BandwidthChannel::class];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Job cancelled')
            ->greeting('Hi '.optional($notifiable->profile)->first_name.',')
            ->line(sprintf(
                'The %s job was cancelled by admin and is no longer in your todo list.',
                $this->job->display_name
            ))
            ->line('If you have any query, please contact support.');
    }

    public function toArray($notifiable)
    {
        return [
            'job_id' => $this->job->getKey(),
            'message' => 'Job cancelled.',
        ];
    }

    public function toBandwidth($notifiable)
    {
        return $this->job->display_name.' job cancelled by admin.';
    }
}
