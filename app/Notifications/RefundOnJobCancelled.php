<?php

namespace App\Notifications;

use App\Models\Job;
use App\Models\Refund;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class RefundOnJobCancelled extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;
    public Refund $refund;

    public function __construct(Job $job, Refund $refund)
    {
        $this->job = $job->withoutRelations();
        $this->refund = $refund;
    }

    public function via($notifiable)
    {
        return ['mail', 'database', BandwidthChannel::class];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Refund received')
            ->greeting('Hi '.optional($notifiable->profile)->first_name.',')
            ->line(sprintf(
                'The %s job was cancelled by admin and %s has been refunded back to you.',
                $this->job->display_name,
                money($this->refund->total_amount)
            ))
            ->line('If you have any query, please contact support.');
    }

    public function toArray($notifiable)
    {
        return [
            'job_id' => $this->job->getKey(),
            'message' => 'Refund received.',
        ];
    }

    public function toBandwidth($notifiable)
    {
        return money($this->refund->total_amount).' refund received for '.$this->job->display_name.' job';
    }
}
