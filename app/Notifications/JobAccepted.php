<?php

namespace App\Notifications;

use App\Models\Job;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class JobAccepted extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;

    public User $applicant;

    public function __construct(Job $job, User $applicant)
    {
        $this->job = $job->withoutRelations();
        $this->applicant = $applicant->withoutRelations();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Job Accepted')
            ->markdown('emails.job.accepted', [
                'job' => $this->job,
                'user' => $notifiable,
                'applicant' => $this->applicant,
                'actionUrl' => route('jobs.show', $this->job),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'job_id' => $this->job->getKey(),
            'applicant_id' => $this->applicant->getKey(),
            'message' => $this->applicant->profile->first_name.' accepted the '.$this->job->display_name.' job.',
        ];
    }

    /**
     * Get the text representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return string
     */
    public function toBandwidth($notifiable)
    {
        return "You're off the hook. Your job has been accepted.".route('jobs.show', $this->job);
    }
}
