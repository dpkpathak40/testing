<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class JobPosted extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;

    public function __construct(Job $job)
    {
        $this->job = $job->withoutRelations();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Job Posted')
            ->markdown('emails.job.posted', [
                'job' => $this->job,
                'user' => $notifiable,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'job_id' => $this->job->id,
            'message' => 'New job posted.',
        ];
    }

    /**
     * Get the text representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return string
     */
    public function toBandwidth($notifiable)
    {
        return "Good call, you've requested a task through OktoHub. We will take it from here.";
    }
}
