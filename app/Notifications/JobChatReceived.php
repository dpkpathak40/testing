<?php

namespace App\Notifications;

use App\Models\Job;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class JobChatReceived extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;

    public User $sender;

    public function __construct(Job $job, User $sender)
    {
        $this->job = $job->withoutRelations();
        $this->sender = $sender->withoutRelations();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Job Chat Notification')
            ->markdown('emails.job.chat', [
                'job' => $this->job,
                'user' => $notifiable,
                'sender' => $this->sender,
                'actionUrl' => ($this->job->creator->is($notifiable)) ? route(
                    'jobs.show',
                    $this->job
                ) : route('receivedJobs.toDo'),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }

    public function toBandwidth($notifiable)
    {
        return 'You have a new chat notification from '.$this->sender->profile->full_name.' on '.$this->job->service->name.' which has a due date on '.$this->job->formatted_due_date.'.'.($this->job->creator->is($notifiable)) ? route(
            'jobs.show',
            $this->job
        ) : route('receivedJobs.toDo');
    }
}
