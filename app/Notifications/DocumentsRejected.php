<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class DocumentsRejected extends Notification implements ShouldQueue
{
    use Queueable;

    public string $reason;

    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Documents Rejected')
            ->markdown('emails.documents.rejected', [
                'reason' => $this->reason,
                'user' => $notifiable,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Documents rejected.',
        ];
    }

    /**
     * Get the text representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return string
     */
    public function toBandwidth($notifiable)
    {
        return 'Unfortunately, your document(s) could not be verified. Turn that frown upside down because you can just go to your
    account and resubmit the document. Please see message below for clarification.';
    }
}
