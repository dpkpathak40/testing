<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class JobReceived extends Notification implements ShouldQueue
{
    use Queueable;

    public Job $job;

    public function __construct(Job $job)
    {
        $this->job = $job->withoutRelations();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Job Received')
            ->markdown('emails.job.received', [
                'job' => $this->job,
                'user' => $notifiable,
                'actionUrl' => route('receivedJobs.received'),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'job_id' => $this->job->id,
            'message' => $this->job->display_name.' job is available for you.',
        ];
    }

    /**
     * Get the text representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return string
     */
    public function toBandwidth($notifiable)
    {
        if ($this->job->transferable_incentive_amount) {
            return 'Ding, Ding, we have a job! Please click on the link to review the details. There is an extra incentive for this job available.'.route('receivedJobs.received');
        }

        return 'Ding, Ding, we have a job! Please click on the link to review the details.'.route('receivedJobs.received');
    }
}
