<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Bandwidth\BandwidthChannel;

class DocumentsVerified extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', BandwidthChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Documents Verified')
            ->markdown('emails.documents.verified', [
                'user' => $notifiable,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Documents verified',
        ];
    }

    /**
     * Get the text representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return string
     */
    public function toBandwidth($notifiable)
    {
        return 'Whoopee! Your documents have been verified. Please complete your account including that sweet bio pic. Glamour shots
    are encouraged.';
    }
}
