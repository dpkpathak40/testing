<?php

namespace App\Mail\Admin;

use App\Models\Broker;
use App\Models\Job;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class BrokerTransactionsReportMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public Broker $broker;
    public Carbon $startDate;
    public Carbon $endDate;

    public function __construct(Broker $broker, Carbon $startDate, Carbon $endDate)
    {
        $this->broker = $broker->withoutRelations();
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function build()
    {
        return $this
            ->subject('Transactions Report')
            ->markdown('emails.brokerReport')
            ->attachData($this->generatePdf(), $this->getFileName(), [
                'mime' => 'application/pdf',
            ]);
    }

    protected function getFileName()
    {
        return Str::slug('transaction-report-'.Date::now()->format('d-M-Y')).'.pdf';
    }

    protected function generatePdf()
    {
        $users = $this->getApplicableUsers();

        return PDF::loadView('admin.brokers.reportPdf', [
            'broker' => $this->broker,
            'users' => $users,
            'totalAmount' => $this->getTotalAmount($users),
        ])
            ->setPaper('a4', 'landscape')
            ->output();
    }

    protected function getApplicableUsers()
    {
        $dateFilterCallback = function ($query) {
            /* @var Builder $query */
            $query->where('status', Job::STATUS_COMPLETED)
                ->where('updated_at', '>=', $this->startDate)
                ->where('updated_at', '<=', $this->endDate);
        };

        return $this->broker
            ->users()
            ->whereHas('assignedJobs', $dateFilterCallback)
            ->with([
                'assignedJobs' => $dateFilterCallback,
                'assignedJobs.service',
                'assignedJobs.transfer',
            ])
            ->latest()
            ->get();
    }

    public function getTotalAmount(Collection $users)
    {
        return $users->sum(function (User $user) {
            return $user->assignedJobs->sum(function (Job $job) {
                return optional($job->transfer)->total_amount;
            });
        });
    }
}
