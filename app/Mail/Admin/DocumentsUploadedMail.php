<?php

namespace App\Mail\Admin;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DocumentsUploadedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    public $realtor;

    /**
     * @var string
     */
    public $actionUrl;

    /**
     * Create a new message instance.
     *
     * @param  User  $realtor
     *
     * @return void
     */
    public function __construct(User $realtor)
    {
        $this->realtor = $realtor->withoutRelations();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->actionUrl = route('admin.users.edit', $this->realtor);

        return $this
            ->subject('Documents Uploaded')
            ->markdown('emails.documents.admin.uploaded');
    }
}
