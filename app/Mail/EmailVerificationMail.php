<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class EmailVerificationMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    const LINK_EXPIRE_IN_HOURS = 24;

    public User $user;

    public string $actionUrl;

    public function __construct(User $user)
    {
        $this->user = $user->withoutRelations();
    }

    public function build()
    {
        $this->actionUrl = $this->getActionUrl($this->user);

        return $this
            ->subject('Email Verification')
            ->markdown('emails.emailVerification')
            ->with([
                'linkExpireInHours' => self::LINK_EXPIRE_IN_HOURS,
            ]);
    }

    protected function getActionUrl(User $user): string
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            now()->addHours(self::LINK_EXPIRE_IN_HOURS),
            [
                'user' => $user,
            ]
        );
    }
}
