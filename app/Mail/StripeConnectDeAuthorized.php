<?php

namespace App\Mail;

use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StripeConnectDeAuthorized extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public Broker $broker;

    public string $actionUrl;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker->withoutRelations();
    }

    public function build()
    {
        $this->actionUrl = route('admin.brokers.edit', $this->broker);

        return $this
            ->subject('Stripe Account DeAuthorized')
            ->markdown('emails.stripe.connectDeAuthorized')
            ->with([
                'realtorsCount' => $this->broker->users()->count(),
            ]);
    }
}
