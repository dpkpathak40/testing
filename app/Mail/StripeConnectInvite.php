<?php

namespace App\Mail;

use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Stripe\OAuth as StripeOAuth;

class StripeConnectInvite extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public Broker $broker;

    public string $actionUrl;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker->withoutRelations();
    }

    public function build()
    {
        $this->actionUrl = $this->getActionUrl($this->broker);

        return $this
            ->markdown('emails.stripe.connectInvite')
            ->subject('Stripe Connect');
    }

    protected function getActionUrl(Broker $broker): string
    {
        return StripeOAuth::authorizeUrl([
            'client_id' => config('services.stripe.connect.client_id'),
            'scope' => 'read_write',
            'response_type' => 'code',
            // Stripe will send back the `state` parameter value to callback url
            'state' => $broker->getKey(),
            'redirect_uri' => route('stripe.callback'),
            'suggested_capabilities' => ['transfers'],
            // Auto fill stripe connect form with this data
            // https://stripe.com/docs/connect/oauth-reference
            'stripe_user' => [
                'email' => $broker->email,
                'country' => 'US',
                'first_name' => $broker->first_name,
                'last_name' => $broker->last_name,
            ],
        ]);
    }
}
