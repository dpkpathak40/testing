<?php

namespace App\Jobs\Webhooks;

use App\Mail\StripeConnectDeAuthorized;
use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Spatie\WebhookClient\Models\WebhookCall;

class AccountApplicationDeauthorized implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public WebhookCall $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $objectId = data_get($this->webhookCall->payload, 'data.object.id');

        if (is_null($objectId)) {
            return;
        }

        $broker = Broker::where('stripe_user_id', $objectId)->first();

        if (is_null($broker)) {
            return;
        }

        $broker->fill(['stripe_user_id' => null]);
        $broker->save();

        // Notify admin
        Mail::to(config('project.admin_email_address'))->send(new StripeConnectDeAuthorized($broker));
    }
}
