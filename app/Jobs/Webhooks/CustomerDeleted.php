<?php

namespace App\Jobs\Webhooks;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;

class CustomerDeleted implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public WebhookCall $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $objectId = data_get($this->webhookCall->payload, 'data.object.id');

        if (is_null($objectId)) {
            return;
        }

        $user = User::where('stripe_customer_id', $objectId)->first();

        if (is_null($user)) {
            return;
        }

        $user->fill(['stripe_customer_id' => null]);
        $user->save();
    }
}
