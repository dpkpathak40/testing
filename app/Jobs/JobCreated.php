<?php

namespace App\Jobs;

use App\Models\Job;
use App\Models\User;
use App\Models\UserDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class JobCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Job $jobModel;

    public function __construct(Job $job)
    {
        $this->jobModel = $job->withoutRelations();
    }

    public function handle()
    {
        $users = User::query()
            ->where('documents_status', UserDocument::STATUS_VERIFIED)
            ->applicableFor($this->jobModel)
            ->whereHas('preference', function ($query) {
                $query->where('sms_notifications', 1)
                    ->orWhere('email_notifications', 1);
            })
            ->latest();

        // Notify the applicable users
        $users->each(function (User $user) {
            $user->notify(new \App\Notifications\JobReceived($this->jobModel));
        });

        // Notify the job creator
        $this->jobModel->creator->notify(new \App\Notifications\JobPosted($this->jobModel));
    }
}
