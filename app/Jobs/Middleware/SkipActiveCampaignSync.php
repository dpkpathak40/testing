<?php

namespace App\Jobs\Middleware;

use Illuminate\Support\Facades\Log;

class SkipActiveCampaignSync
{
    public function handle($job, $next)
    {
        if (! config('services.active_campaign.enabled')) {
            Log::info('Skip Active Campaign Sync for - '.get_class($job));

            return false;
        }

        return $next($job);
    }
}
