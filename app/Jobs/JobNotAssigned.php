<?php

namespace App\Jobs;

use App\Models\Job;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class JobNotAssigned implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Job $jobModel;

    public User $assignee;

    public function __construct(Job $job, User $assignee)
    {
        $this->jobModel = $job->withoutRelations();
        $this->assignee = $assignee->withoutRelations();
    }

    public function handle()
    {
        $applicants = $this->jobModel
            ->applicants()
            ->where('user_id', '<>', $this->assignee->getKey())
            ->latest();

        // Notify the users those who applied but could not get the job,
        // since it has been assigned to one of them
        $applicants->each(function (User $user) {
            $user->notify(new \App\Notifications\JobNotAssigned($this->jobModel));
        });
    }
}
