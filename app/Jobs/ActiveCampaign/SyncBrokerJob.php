<?php

namespace App\Jobs\ActiveCampaign;

use App\Jobs\Middleware\SkipActiveCampaignSync;
use App\Models\Broker;
use App\Services\ActiveCampaign\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SyncBrokerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Broker $broker;

    public Client $client;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker->withoutRelations();
        $this->client = (new Client())->withTokenAuth();
    }

    public function middleware()
    {
        return [new SkipActiveCampaignSync];
    }

    public function handle()
    {
        Log::debug("Sync broker to active campaign - {$this->broker->getKey()}");

        $response = $this->client->post('contact/sync', [
            'contact' => [
                'email' => $this->broker->email,
                'firstName' => $this->broker->first_name,
                'lastName' => $this->broker->last_name,
                'phone' => $this->broker->full_phone_number,
            ],
        ]);

        $contactId = $response->getBody()->json()->contact->id;

        if ($contactId !== $this->broker->active_campaign_contact_id) {
            $this->subscribeToList($contactId);
            $this->saveReferenceId($contactId);
        }
    }

    public function saveReferenceId($contactId)
    {
        $this->broker->fill([
            'active_campaign_contact_id' => $contactId,
        ]);
        $this->broker->save();
    }

    public function subscribeToList($contactId)
    {
        return $this->client->post('contactLists', [
            'contactList' => [
                'list' => config('services.active_campaign.lists.broker'),
                'contact' => $contactId,
                'status' => 1,
            ],
        ]);
    }
}
