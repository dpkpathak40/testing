<?php

namespace App\Jobs\ActiveCampaign;

use App\Jobs\Middleware\SkipActiveCampaignSync;
use App\Models\User;
use App\Services\ActiveCampaign\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SyncAgentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $agent;

    public Client $client;

    public function __construct(User $user)
    {
        $this->agent = $user->withoutRelations();

        $this->client = (new Client())->withTokenAuth();
    }

    public function middleware()
    {
        return [new SkipActiveCampaignSync];
    }

    public function handle()
    {
        Log::debug("Sync agent to active campaign - {$this->agent->getKey()}");

        $response = $this->client->post('contact/sync', [
            'contact' => [
                'email' => $this->agent->email,
                'firstName' => $this->agent->profile->first_name,
                'lastName' => $this->agent->profile->last_name,
                'phone' => optional($this->agent->address)->full_phone_number,
            ],
        ]);

        $contactId = $response->getBody()->json()->contact->id;

        if ($contactId !== $this->agent->active_campaign_contact_id) {
            $this->subscribeToList($contactId);
            $this->addTag($contactId);
            $this->saveReferenceId($contactId);
        }
    }

    public function addTag($contactId)
    {
        // The mete field should be created on CRM
        $metaFieldId = 3;

        return $this->client->post("fieldValues/{$metaFieldId}", [
            'fieldValue' => [
                'contact' => $contactId,
                'field' => $metaFieldId,
                'value' => 'yes',
            ],
        ]);
    }

    public function saveReferenceId($contactId)
    {
        $this->agent->fill([
            'active_campaign_contact_id' => $contactId,
        ]);
        $this->agent->save();
    }

    public function subscribeToList($contactId)
    {
        return $this->client->post('contactLists', [
            'contactList' => [
                'list' => config('services.active_campaign.lists.agent'),
                'contact' => $contactId,
                'status' => 1,
            ],
        ]);
    }
}
