<?php

namespace App\Jobs;

use App\Models\Job;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class JobIncentiveUpdated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Job $jobModel;

    public function __construct(Job $job)
    {
        $this->jobModel = $job->withoutRelations();
    }

    public function handle()
    {
        $users = User::query()
            ->whereHas('appliedJobs', function ($query) {
                $query->where('job_id', $this->jobModel->getKey());
            })
            ->whereHas('preference', function ($query) {
                $query->where('sms_notifications', 1)
                    ->orWhere('email_notifications', 1);
            })
            ->latest();

        // Notify the job receivers
        $users->each(function (User $user) {
            $user->notify(new \App\Notifications\JobIncentiveUpdated($this->jobModel));
        });
    }
}
