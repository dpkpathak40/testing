<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PersonNameRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[A-Za-z \'-]+$/', (string) $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute may only contain letters, hyphens and apostrophes.';
    }
}
